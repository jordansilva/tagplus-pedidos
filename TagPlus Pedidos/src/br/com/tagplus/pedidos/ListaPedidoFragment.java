package br.com.tagplus.pedidos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PedidoService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Pedido;
import br.com.tagplus.pedidos.helper.Constants;
import br.com.tagplus.pedidos.ui.SessionHelper;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterPedidoList;
import br.com.tagplus.pedidos.ui.adapter.AdapterPedidoList.AdapterPedidoListener;
import br.com.tagplus.pedidos.ui.adapter.SectionAdapterPedido;

import com.jordansilva.android.prettyui.empty.EmptyView;
import com.mobsandgeeks.adapters.Sectionizer;

@EFragment(R.layout.fragment_list)
public class ListaPedidoFragment extends BaseFragment implements BusinessListener, OnItemClickListener, OnQueryTextListener,
		OnActionExpandListener, AdapterPedidoListener {

	@Bean
	SessionHelper mSession;

	@ViewById(R.id.list)
	ListView listView;

	@ViewById
	EmptyView emptyView;

	@Bean
	PedidoService service;

	SectionAdapterPedido mAdapter;
	Menu mMenu;
	ArrayList<Pedido> mPedidos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.lista_pedido, menu);

		mMenu = menu;

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		searchItem.setOnActionExpandListener(this);

		SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setQueryHint(getString(R.string.buscar_pedido));
		searchView.setOnQueryTextListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			adicionarNovo();
			return true;
		case R.id.action_ordernar_data:
			ordernarData();
			return true;
		case R.id.action_ordernar_cliente:
			ordernarCliente();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void adicionarNovo() {
		Intent intent = PedidoActivity_.intent(getActivity()).get();
		startActivityForResult(intent, 1);
	}

	@Override
	protected void load() {
		super.load();

		configurarUi();
		buscarDados();
	}

	@Background
	protected void buscarDados() {
		service.listarPedidos(getUsuarioLogado().getCode(), this);
	}

	@Override
	@UiThread
	public void onPreInit() {
		if (isAdded())
			UIHelper.showLoading(getActivity());
	}

	@SuppressWarnings("unchecked")
	@Override
	@UiThread
	public void onPostExecute(RequestResult result) {
		if (isAdded()) {
			UIHelper.hideLoading(getActivity());
			if (!result.isError()) {
				mPedidos = (ArrayList<Pedido>) result.getData();
				carregarLista();
			}
		}
	}

	@UiThread
	protected void carregarLista() {

		if (isAdded()) {
			if (mSession.getConfiguracao().getTipoOrdenacaoPedido() == Constants.OrdenacaoPedido.POR_CLIENTE)
				ordernarCliente();
			else
				ordernarData();

			listView.setEmptyView(emptyView);
		}
	}

	protected void ordernarData() {

		Collections.sort(mPedidos, new Comparator<Pedido>() {

			@Override
			public int compare(Pedido lhs, Pedido rhs) {
				return rhs.getDataPedido().compareTo(lhs.getDataPedido());
			}
		});
		AdapterPedidoList adapter = new AdapterPedidoList(getActivity(), mPedidos, this);

		mAdapter = new SectionAdapterPedido(getActivity(), adapter, new Sectionizer<Pedido>() {

			@Override
			public String getSectionTitleForItem(Pedido instance) {
				return instance.getDataPedido().toString("MMMM yyyy");
			}
		});

		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);
	}

	protected void ordernarCliente() {

		Collections.sort(mPedidos, new Comparator<Pedido>() {

			@Override
			public int compare(Pedido lhs, Pedido rhs) {
				String lhsNome = "";
				String rhsNome = "";
				if (lhs.getCliente() != null)
					lhsNome = lhs.getCliente().getNome();
				if (rhs.getCliente() != null)
					rhsNome = rhs.getCliente().getNome();
				return lhsNome.compareTo(rhsNome);
			}
		});

		Collections.sort(mPedidos, new Comparator<Pedido>() {

			@Override
			public int compare(Pedido lhs, Pedido rhs) {
				return rhs.getDataPedido().compareTo(lhs.getDataPedido());
			}
		});

		AdapterPedidoList adapter = new AdapterPedidoList(getActivity(), mPedidos, this);

		mAdapter = new SectionAdapterPedido(getActivity(), adapter, new Sectionizer<Pedido>() {

			@Override
			public String getSectionTitleForItem(Pedido instance) {
				if (instance.getCliente() == null)
					return "";
				else
					return instance.getCliente().getNome();

			}
		});

		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);
	}

	protected void configurarUi() {
		emptyView.setMessageText(R.string.emptyview_sem_pedidos);
		emptyView.setButtonText(R.string.action_adicionar_novo_pedido);
		emptyView.setListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarNovo();
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1 && resultCode == Activity.RESULT_OK)
			buscarDados();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object obj = parent.getItemAtPosition(position);
		if (obj != null) {
			alterarPedido((Pedido) obj);
		}
	}

	private void alterarPedido(Pedido pedido) {
		Intent intent = PedidoActivity_.intent(getActivity()).codigoPedido(pedido.getCode()).get();
		startActivityForResult(intent, 1);
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(true);
			return true;
		}

		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(false);
			return true;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		search(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		search(query);
		return false;
	}

	private void search(String query) {
		if (mAdapter != null)
			mAdapter.getFilter().filter(query);
	}

	@Override
	public void cancelarPedido(Pedido pedido) {
		pedido.setCodigoStatus(Pedido.CANCELADO);
		service.salvar(getUsuarioLogado().getCode(), pedido, pedidoAlteradoListener);
	}

	@Override
	public void editarPedido(Pedido pedido) {
		alterarPedido(pedido);
	}

	@Override
	public void fecharPedido(Pedido pedido) {
		pedido.setCodigoStatus(Pedido.CONFIRMADO);
		service.salvar(getUsuarioLogado().getCode(), pedido, pedidoAlteradoListener);
	}

	private BusinessListener pedidoAlteradoListener = new BusinessListener() {

		@Override
		public void onPreInit() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPostExecute(RequestResult result) {
			mAdapter.notifyDataSetChanged();
		}
	};

}

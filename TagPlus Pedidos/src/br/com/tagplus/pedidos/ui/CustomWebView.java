package br.com.tagplus.pedidos.ui;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import br.com.tagplus.pedidos.helper.KeyValuePair;

public class CustomWebView extends WebView {

	private InterceptPostListener listener;

	public CustomWebView(Context context) {
		super(context);
	}

	public CustomWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public void setIntercepListener(InterceptPostListener listener) {
		this.listener = listener;
	}

	@Override
	public void postUrl(String url, byte[] postData) {
		if (listener != null) {
			getPostData(postData);
		}
		super.postUrl(url, postData);
	}

	private void getPostData(byte[] postData) {
		if (postData != null && postData.length > 0) {
			String result = EncodingUtils.getString(postData, "utf-8");
		}
	}

	public interface InterceptPostListener {
		void post(KeyValuePair strings);
	}
}

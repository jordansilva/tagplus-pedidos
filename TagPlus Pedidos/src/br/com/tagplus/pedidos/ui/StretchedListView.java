package br.com.tagplus.pedidos.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

public class StretchedListView extends LinearLayout {

	private static final String TAG = StretchedListView.class.getSimpleName();
	private final DataSetObserver dataSetObserver;
	private ListAdapter adapter;
	private StretchedListViewListener listener;
	private View emptyView;

	public StretchedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(LinearLayout.VERTICAL);
		this.dataSetObserver = new DataSetObserver() {
			@Override
			public void onChanged() {
				syncDataFromAdapter();
				super.onChanged();
			}

			@Override
			public void onInvalidated() {
				syncDataFromAdapter();
				super.onInvalidated();
			}
		};
	}

	public void setAdapter(ListAdapter adapter) {

		if (adapter != null) {
			this.adapter = adapter;
			this.adapter.registerDataSetObserver(dataSetObserver);
		}

		if (this.adapter != null && adapter == null) {
			this.adapter.unregisterDataSetObserver(dataSetObserver);
			this.adapter = null;
		}

		syncDataFromAdapter();
	}

	public void setListener(StretchedListViewListener listener) {
		this.listener = listener;
	}

	public ListAdapter getAdapter() {
		return adapter;
	}

	private void syncDataFromAdapter() {
		Log.d(TAG, "syncDataFromAdapter() called");
		removeAllViews();

		if (adapter != null) {
			int count = adapter.getCount();

			if (emptyView != null) {
				if (count > 0)
					emptyView.setVisibility(View.GONE);
				else
					emptyView.setVisibility(View.VISIBLE);
			}

			for (int i = 0; i < count; i++) {
				View view = adapter.getView(i, null, this);
				final int position = i;
				view.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (listener != null)
							listener.onItemClick(adapter, position);
					}
				});

				view.setOnLongClickListener(new OnLongClickListener() {

					@Override
					public boolean onLongClick(View v) {
						if (listener != null) {
							listener.onItemLongClick(adapter, position);
							return true;
						}

						return false;
					}
				});
				addView(view);
			}
		} else if (emptyView != null)
			emptyView.setVisibility(View.VISIBLE);

	}

	public void setEmptyView(View v) {
		this.emptyView = v;
	}

	public int getCount() {
		return adapter != null ? adapter.getCount() : 0;
	}

	public interface StretchedListViewListener {
		void onItemClick(ListAdapter adapter, int position);

		void onItemLongClick(ListAdapter adapter, int position);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}

}

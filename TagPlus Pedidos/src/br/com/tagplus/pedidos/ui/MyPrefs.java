package br.com.tagplus.pedidos.ui;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface MyPrefs {

	@DefaultBoolean(true)
	boolean isFirstTime();

	@DefaultString("")
	String codigoUltimoUsuario();
	
	@DefaultLong(0)
	long sincronizacaoFornecedor();
	
	@DefaultLong(0)
	long sincronizacaoCliente();
	
	@DefaultLong(0)
	long sincronizacaoProduto();
	
	@DefaultLong(0)
	long sincronizacaoPedido();

}

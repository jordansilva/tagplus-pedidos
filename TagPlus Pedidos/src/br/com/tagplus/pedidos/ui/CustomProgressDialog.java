package br.com.tagplus.pedidos.ui;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import br.com.tagplus.pedidos.R;

/**
 * @author Jordan Silva ( @jordansilva )
 * @filename CustomProgressDialog.java
 * @since 20/01/2014 20:35:10
 */
public class CustomProgressDialog extends Dialog {

	private ProgressWheel wheel;
	private Activity mActivity;

	public CustomProgressDialog(Activity context) {
		super(context, R.style.TransparentProgressDialog);
		init(context);
	}

	public CustomProgressDialog(Activity context, boolean cancelable, OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	public CustomProgressDialog(Activity context, int theme) {
		super(context, theme);
	}

	protected void init(Activity context) {
		mActivity = context;

		WindowManager.LayoutParams p = getWindow().getAttributes();
		p.gravity = Gravity.CENTER;
		getWindow().setAttributes(p);
		setTitle(null);
		setCancelable(false);
		setOnCancelListener(null);

		int scale = returnDip(context, 100);
		wheel = new ProgressWheel(context);
		configureWheelUI(context, scale);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(scale, scale);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		addContentView(wheel, params);
	}

	private void configureWheelUI(Context context, int scale) {
		String textColor = "#222222";
		// String circleColor = "#2E9121";
		String rimColor = "#40000000";
		String barColor = "#222222";
		int textSize = 14;
		int rimWidth = 1;
		int barLength = 30;
		int barWidth = 4;

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(scale, scale);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);

		wheel.setLayoutParams(params);
		wheel.setTextColor(Color.parseColor(textColor));
		wheel.setTextSize(returnSP(context, textSize));
		wheel.setRimColor(Color.parseColor(rimColor));
		// wheel.setCircleColor(Color.parseColor(circleColor));
		wheel.setRimWidth(returnDip(context, rimWidth));
		wheel.setBarLength(returnDip(context, barLength));
		wheel.setBarColor(Color.parseColor(barColor));
		wheel.setBarWidth(returnDip(context, barWidth));
		wheel.setSpinSpeed(returnDip(context, 30));
		wheel.setDelayMillis(75);

		// wheel.setCircleRadius(returnDip(context, 50));

		// if (!StringHelper.isNullOrEmpty(message))
		// dialog.setText(message);

		// int[] pixels = new int[] { 0xFF2E9121, 0xFF2E9121, 0xFF2E9121,
		// 0xFF2E9121, 0xFF2E9121, 0xFF2E9121, 0xFFFFFFFF,
		// 0xFFFFFFFF };
		// Bitmap bm = Bitmap.createBitmap(pixels, 8, 1,
		// Bitmap.Config.ARGB_8888);
		// Shader shader = new BitmapShader(bm, Shader.TileMode.REPEAT,
		// Shader.TileMode.REPEAT);
		// wheel.setRimShader(shader);
	}

	private static int returnDip(Context context, int pixel) {
		return Float.valueOf(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, pixel, context.getResources().getDisplayMetrics()))
				.intValue();
	}

	private static int returnSP(Context context, int pixel) {
		return Float.valueOf(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, pixel, context.getResources().getDisplayMetrics()))
				.intValue();
	}

	@Override
	public void show() {
		if (!mActivity.isFinishing()) {
			super.show();
			wheel.spin();
		}
	}

	@Override
	public void dismiss() {
		if (!mActivity.isFinishing()) {
			super.dismiss();
			wheel.stopSpinning();
		}
	}

}

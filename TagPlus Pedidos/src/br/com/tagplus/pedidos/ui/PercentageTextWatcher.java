package br.com.tagplus.pedidos.ui;

import java.lang.ref.WeakReference;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import br.com.tagplus.pedidos.helper.StringHelper;

public class PercentageTextWatcher implements TextWatcher {

	private final WeakReference<EditText> editTextWeakReference;

	public PercentageTextWatcher(EditText editText) {
		editTextWeakReference = new WeakReference<EditText>(editText);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable editable) {
		synchronized (this) {
			EditText editText = editTextWeakReference.get();
			if (editText == null)
				return;

			int selection = editText.getSelectionStart();
			String s = editable.toString();
			editText.removeTextChangedListener(this);
			String cleanString = s.toString().replaceAll("[%]", "");
			if (cleanString.length() != 0) {
				if (cleanString.indexOf(".") == cleanString.length() || cleanString.indexOf(",") == cleanString.length())
					cleanString = cleanString.replaceAll("[.,]", "");

				String formatted = String.valueOf(cleanString) + "%";
				int maxLength = StringHelper.getMaxLength(editText);
				if (maxLength > 0 && maxLength >= formatted.length())
					editText.setText(formatted);

				editText.setSelection(selection);
			} else
				editText.setText("");
			
			editText.addTextChangedListener(this);
		}
	}
}
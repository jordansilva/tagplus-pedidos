package br.com.tagplus.pedidos.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class NavMenuItem {
	private int code;
	private String name;
	private Drawable image;
	private String section;

	public NavMenuItem(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public NavMenuItem(int code, String name, Context context, int image) {
		this.code = code;
		this.name = name;
		this.image = context.getResources().getDrawable(image);
	}

	public NavMenuItem(int code, String name, Drawable drawable) {
		this.code = code;
		this.name = name;
		this.image = drawable;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Drawable getImage() {
		return image;
	}

	public void setImage(Drawable image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

}

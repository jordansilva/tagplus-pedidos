package br.com.tagplus.pedidos.ui.adapter;

public interface IFilterable {

	boolean filter(CharSequence text);
}

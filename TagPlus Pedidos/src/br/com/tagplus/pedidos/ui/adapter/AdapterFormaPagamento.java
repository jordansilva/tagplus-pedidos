package br.com.tagplus.pedidos.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.FormaPagamento;

import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterFormaPagamento extends InstantAdapter<FormaPagamento> {

	private List<FormaPagamento> mList;
	private boolean isEnabled = true;

	public AdapterFormaPagamento(Context context, List<FormaPagamento> list) {
		super(context, R.layout.list_pedido_forma_pagamento_layout, FormaPagamento.class, list);
		this.mList = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		final FormaPagamento p = mList.get(position);

		ImageButton imgButton = (ImageButton) v.findViewById(R.id.btnPopup);
		imgButton.setOnClickListener(new OnClickListener() {

			@SuppressLint("ViewHolder")
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(getContext(), v);
				MenuInflater inflater = popup.getMenuInflater();
				inflater.inflate(R.menu.pedido_produto_item, popup.getMenu());

				popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.action_delete:
							removerItem(p);
							return true;
						}
						return false;
					}
				});
				popup.show();

			}
		});

		imgButton.setVisibility(isEnabled ? View.VISIBLE : View.INVISIBLE);

		return v;
	}

	private void removerItem(FormaPagamento item) {
		mList.remove(item);
		notifyDataSetChanged();
	}

	public List<FormaPagamento> getItems() {
		return mList;
	}

	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
		notifyDataSetChanged();
	}

}

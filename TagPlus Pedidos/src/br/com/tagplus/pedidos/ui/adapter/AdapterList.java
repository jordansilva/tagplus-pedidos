package br.com.tagplus.pedidos.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.Fornecedor;

import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterList<T> extends InstantAdapter<T> implements Filterable {

	private int layoutResourceId;
	private List<T> list;

	public AdapterList(Context context, int layoutResourceId, Class<?> dataType, List<T> list) {
		super(context, layoutResourceId, dataType, list);
		this.layoutResourceId = layoutResourceId;
		this.list = new ArrayList<T>(list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		if (layoutResourceId == R.layout.list_layout) {

			T obj = list.get(position);

			TextView email = (TextView) v.findViewById(R.id.txtEmail);
			if (email.getText() == null || email.getText().toString().isEmpty())
				email.setVisibility(View.GONE);
			else
				email.setVisibility(View.VISIBLE);

			TextView telefone = (TextView) v.findViewById(R.id.txtTelefone);
			if (telefone.getText() == null || telefone.getText().toString().isEmpty())
				telefone.setVisibility(View.GONE);
			else
				telefone.setVisibility(View.VISIBLE);

			TextView localizacao = (TextView) v.findViewById(R.id.txtLocalizacao);
			if (localizacao.getText() == null || localizacao.getText().toString().isEmpty())
				localizacao.setVisibility(View.GONE);
			else
				localizacao.setVisibility(View.VISIBLE);

			TextView tipoPessoa = (TextView) v.findViewById(R.id.txtTipoPessoa);
			boolean isPessoaFisica = true;
			if (obj instanceof Fornecedor)
				isPessoaFisica = ((Fornecedor) obj).isPessoaFisica();
			if (obj instanceof Cliente)
				isPessoaFisica = ((Cliente) obj).isPessoaFisica();

			if (isPessoaFisica)
				tipoPessoa.setText(R.string.pessoa_fisica);
			else
				tipoPessoa.setText(R.string.pessoa_juridica);
		}
		return v;
	}

	public List<T> getItems() {
		return list;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				if (results.count == 0) {
					clear();
					notifyDataSetInvalidated();
				} else {
					clear();
					addAll((List<T>) results.values);
					notifyDataSetChanged();
				}
			}

			@SuppressWarnings("unchecked")
			@Override
			protected FilterResults performFiltering(CharSequence filterText) {

				FilterResults results = new FilterResults();

				List<IFilterable> items = (List<IFilterable>) list;

				if (filterText != null && !filterText.toString().isEmpty()) {

					List<T> filteredItems = new ArrayList<T>();

					for (IFilterable i : items) {
						if (i.filter(filterText))
							filteredItems.add((T) i);
					}

					results.count = filteredItems.size();
					results.values = filteredItems;
				} else {
					results.count = items.size();
					results.values = items;

				}
				return results;
			}
		};
	}

}

package br.com.tagplus.pedidos.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.ui.NavMenuItem;

import com.mobsandgeeks.adapters.Sectionizer;
import com.mobsandgeeks.adapters.SimpleSectionAdapter;

public class SectionAdapterMenu extends SimpleSectionAdapter<NavMenuItem> {

	private int itemSelected = -1;

	public SectionAdapterMenu(Context context, AdapterMenu listAdapter) {
		super(context, listAdapter, R.layout.section_header, R.id.section_text, new Sectionizer<NavMenuItem>() {

			@Override
			public String getSectionTitleForItem(NavMenuItem instance) {
				return instance.getSection();
			}
		});
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = super.getView(position, convertView, parent);
		if (position == itemSelected)
			v.setBackgroundResource(R.color.menu_list_selected);
		else
			v.setBackgroundColor(Color.WHITE);

		return v;
	}

	public void setSelectedItem(int position) {
		this.itemSelected = position;
	}

}

package br.com.tagplus.pedidos.ui.adapter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.AutoCompleteFloatLabel;
import br.com.tagplus.pedidos.ui.MoneyTextWatcher;
import br.com.tagplus.pedidos.ui.listener.AdapterPedidoProdutoListener;

import com.jordansilva.android.prettyui.ExtendedEditText;
import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterPedidoProduto extends InstantAdapter<PedidoProduto> {

	private List<PedidoProduto> mList;
	private AdapterPedidoProdutoListener mListener;
	private boolean isEnabled = true;

	public AdapterPedidoProduto(Context context, List<PedidoProduto> list, AdapterPedidoProdutoListener listener) {
		super(context, R.layout.list_pedido_produto_layout, PedidoProduto.class, list);
		this.mList = list;
		this.mListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		final PedidoProduto p = mList.get(position);

		final ExtendedEditText txtQuantidade = (ExtendedEditText) v.findViewById(R.id.txtQuantidade);
		txtQuantidade.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() > 0)
					alterarProduto(p, Integer.valueOf(s.toString()), txtQuantidade);
				else
					alterarProduto(p, 0, txtQuantidade);

			}
		});
		// ImageButton imgMenos = (ImageButton) v.findViewById(R.id.imgMenos);
		// imgMenos.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// String qtd = txtQuantidade.getEditableText().toString();
		// int quantidade = 0;
		// if (!StringHelper.isNullOrEmpty(qtd))
		// quantidade = Integer.valueOf(qtd);
		//
		// quantidade--;
		// alterarProduto(p, quantidade, txtQuantidade);
		// }
		// });
		//
		// ImageButton imgMais = (ImageButton) v.findViewById(R.id.imgMais);
		// imgMais.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// String qtd = txtQuantidade.getEditableText().toString();
		// int quantidade = 0;
		// if (!StringHelper.isNullOrEmpty(qtd))
		// quantidade = Integer.valueOf(qtd);
		//
		// quantidade++;
		// alterarProduto(p, quantidade, txtQuantidade);
		// }
		// });

		final AutoCompleteFloatLabel txtValor = (AutoCompleteFloatLabel) v.findViewById(R.id.txtValor);
		txtValor.getAutoCompleteTextView().setTypeface(Typeface.DEFAULT);
		TextWatcher watcher = new MoneyTextWatcher(txtValor.getAutoCompleteTextView());
		txtValor.getAutoCompleteTextView().addTextChangedListener(watcher);
		txtValor.getAutoCompleteTextView().addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

				try {
					if (!StringHelper.isNullOrEmpty(s.toString())) {
						NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
						Number money = moneyFormat.parse(s.toString());

						p.setValorUnidade(BigDecimal.valueOf(money.doubleValue()));
						if (mListener != null)
							mListener.notifyPedidoDataSetChanged();
					}
				} catch (ArithmeticException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
		});

		ImageButton imgButton = (ImageButton) v.findViewById(R.id.btnPopup);
		imgButton.setOnClickListener(new OnClickListener() {

			@SuppressLint("ViewHolder")
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(getContext(), v);
				MenuInflater inflater = popup.getMenuInflater();
				inflater.inflate(R.menu.pedido_produto_item, popup.getMenu());

				popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.action_delete:
							removerItem(p);
							return true;
						}
						return false;
					}
				});
				popup.show();

			}
		});

		TextView txtNome = (TextView) v.findViewById(R.id.txtNome);

		txtNome.setEnabled(isEnabled);
		txtQuantidade.setEnabled(isEnabled);
		txtValor.setEnabled(isEnabled);
		imgButton.setVisibility(isEnabled ? View.VISIBLE : View.INVISIBLE);

		return v;
	}

	private void alterarProduto(PedidoProduto produto, int quantidade, ExtendedEditText editText) {

		if (quantidade < 0)
			quantidade = 0;

		if (String.valueOf(quantidade).length() > editText.getMaxLength()) {
			StringBuilder builder = new StringBuilder();
			while (builder.length() < editText.getMaxLength()) {
				builder.append('9');
			}

			quantidade = Integer.valueOf(builder.toString());
		}

		if (quantidade >= 0 && produto.getQuantidade() != quantidade) {
			produto.setQuantidade(quantidade);
			editText.setText(String.valueOf(quantidade));
			editText.setSelection(editText.getText().length());

			if (mListener != null)
				mListener.notifyPedidoDataSetChanged();
		}
	}

	private void removerItem(PedidoProduto item) {
		mList.remove(item);
		notifyDataSetChanged();
		if (mListener != null)
			mListener.notifyPedidoDataSetChanged();
	}

	public List<PedidoProduto> getItems() {
		return mList;
	}

	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
		notifyDataSetChanged();
	}

}

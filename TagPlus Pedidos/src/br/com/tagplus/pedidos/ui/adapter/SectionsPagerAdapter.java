package br.com.tagplus.pedidos.ui.adapter;

import java.util.Collection;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

	@SuppressWarnings("rawtypes")
	LinkedHashMap<String, Class> fragments;
	FragmentManager fm;
	Activity context;

	@SuppressWarnings("rawtypes")
	public SectionsPagerAdapter(Activity context, FragmentManager fm) {
		super(fm);
		this.context = context;
		this.fm = fm;
		fragments = new LinkedHashMap<String, Class>();
	}

	@SuppressWarnings("rawtypes")
	public void addFragment(String title, Class fragment) {
		fragments.put(title, fragment);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Fragment getItem(int position) {
		
		Collection<Class> list = fragments.values();
		if (list != null && list.size() > 0)
		{
			String name = ((Class) list.toArray()[position]).getName();
			return Fragment.instantiate(context, name);
		}
		else
			return null;
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

//	public void removeAllfragments() {
//		if (fragments != null) {
//			for (Fragment fragment : fragments.values()) {
//				fm.beginTransaction().remove(fragment).commit();
//			}
//
//			fragments.clear();
//			notifyDataSetChanged();
//		}
//	}

	@Override
	public CharSequence getPageTitle(int position) {
		return (String) fragments.keySet().toArray()[position];
	}
}

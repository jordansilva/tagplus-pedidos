package br.com.tagplus.pedidos.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Fornecedor;

import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterProdutoFornecedor extends InstantAdapter<Fornecedor> {

	private List<Fornecedor> list;
	private AdapterFornecedorListener listener;

	public AdapterProdutoFornecedor(Context context, List<Fornecedor> list, AdapterFornecedorListener listener) {
		super(context, R.layout.list_fornecedor_layout, Fornecedor.class, list);
		this.list = list;
		this.listener = listener;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		final Fornecedor f = list.get(position);

		ImageButton button = (ImageButton) v.findViewById(R.id.imgButton);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (listener != null)
					listener.onDelete(position, f);
			}
		});

		return v;
	}

	public List<Fornecedor> getItems() {
		return list;
	}

	public interface AdapterFornecedorListener {
		void onDelete(int position, Fornecedor fornecedor);
	}

}

package br.com.tagplus.pedidos.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.domain.Perfil;

public class TwoLineAdapter extends ArrayAdapter<Perfil> {

	private List<Perfil> objects;

	public TwoLineAdapter(Context context, List<Perfil> objects) {
		super(context, android.R.layout.simple_dropdown_item_1line, objects);
		this.objects = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		if (rowView == null) {
			rowView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(android.R.id.text1);
			rowView.setTag(viewHolder);
		}

		// View view = super.getView(position, convertView, parent);
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.text.setText(objects.get(position).getNome());

		// TextView text2 = (TextView) view.findViewById(android.R.id.text2);
		// text2.setText(objects.get(position).getUsuarioSistema());
		return rowView;
	}

	static class ViewHolder {
		public TextView text;
		public TextView text2;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// View view = super.getDropDownView(position, convertView, parent);
		Perfil p = objects.get(position);
		View view = null;
		if (p.isVinculado()) {
			view = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
			TextView text2 = (TextView) view.findViewById(android.R.id.text2);
			text2.setText(p.getUsuarioSistema());
		} else

			view = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_dropdown_item_1line, parent, false);

		TextView text1 = (TextView) view.findViewById(android.R.id.text1);
		text1.setText(p.getNome());

		return view;
	}
}
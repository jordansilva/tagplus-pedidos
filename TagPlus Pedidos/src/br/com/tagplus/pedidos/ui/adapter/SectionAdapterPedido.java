/*
 * Copyright (c) 2012 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.tagplus.pedidos.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.Filter;
import android.widget.Filterable;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Pedido;

import com.mobsandgeeks.adapters.Sectionizer;
import com.mobsandgeeks.adapters.SimpleSectionAdapter;

public class SectionAdapterPedido extends SimpleSectionAdapter<Pedido> implements Filterable {

	public SectionAdapterPedido(Context context, AdapterPedidoList listAdapter, Sectionizer<Pedido> sectionizer) {
		super(context, listAdapter, R.layout.section_pedido_header, R.id.section_text, sectionizer);
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				if (results.count == 0) {
					((AdapterPedidoList) mListAdapter).clearItems();
					findSections();
					notifyDataSetInvalidated();
				} else {
					((AdapterPedidoList) mListAdapter).setItems((List<Pedido>) results.values);
					findSections();
					notifyDataSetChanged();
				}
			}

			@Override
			protected FilterResults performFiltering(CharSequence filterText) {

				FilterResults results = new FilterResults();

				List<Pedido> items = ((AdapterPedidoList) mListAdapter).getItemsFull();

				if (filterText != null && !filterText.toString().isEmpty()) {

					List<Pedido> filteredItems = new ArrayList<Pedido>();

					for (Pedido i : items) {
						if (i.filter(filterText))
							filteredItems.add((Pedido) i);
					}

					results.count = filteredItems.size();
					results.values = filteredItems;
				} else {
					results.count = items.size();
					results.values = items;

				}
				return results;
			}
		};
	}

}

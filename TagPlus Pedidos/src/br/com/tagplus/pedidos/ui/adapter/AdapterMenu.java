package br.com.tagplus.pedidos.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.ui.NavMenuItem;

public class AdapterMenu extends BaseAdapter {

	private Context context;
	private ArrayList<NavMenuItem> items;

	public AdapterMenu(Context context, ArrayList<NavMenuItem> data) {
		this.context = context;
		this.items = data;
	}

	public int getCount() {
		return items.size();
	}

	public NavMenuItem getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return items.get(position).getCode();
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.list_menu, parent, false);

			viewHolder = new ViewHolder();
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
			viewHolder.text = (TextView) convertView.findViewById(R.id.text);

			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		NavMenuItem objectItem = items.get(position);
		if (objectItem != null) {

			if (objectItem.getImage() != null) {
				viewHolder.icon.setImageDrawable(objectItem.getImage());
				viewHolder.icon.setVisibility(View.VISIBLE);
			}

			viewHolder.text.setText(objectItem.getName());
		}

		return convertView;
	}

	private class ViewHolder {
		ImageView icon;
		TextView text;
	}

}

package br.com.tagplus.pedidos.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Atributo;

import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterAtributo extends InstantAdapter<Atributo> {

	private List<Atributo> list;
	private AdapterAtributoListener listener;

	public AdapterAtributo(Context context, List<Atributo> list, AdapterAtributoListener listener) {
		super(context, R.layout.list_atributo_layout, Atributo.class, list);
		this.list = list;
		this.listener = listener;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		final Atributo a = list.get(position);

		ImageButton button = (ImageButton) v.findViewById(R.id.imgButton);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (listener != null)
					listener.onDelete(position, a);
			}
		});

		return v;
	}

	public List<Atributo> getItems() {
		return list;
	}

	public interface AdapterAtributoListener {
		void onDelete(int position, Atributo a);
	}

}

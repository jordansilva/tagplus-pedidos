package br.com.tagplus.pedidos.ui.adapter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.crashlytics.android.Crashlytics;
import com.mobsandgeeks.adapters.InstantAdapter;
import com.squareup.picasso.Picasso;

public class AdapterProdutoGrid extends InstantAdapter<Produto> implements Filterable {

	private List<Produto> list;

	public AdapterProdutoGrid(Context context, List<Produto> list) {
		super(context, R.layout.grid_layout, Produto.class, list);
		this.list = list;
		this.list = new ArrayList<Produto>(list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		Produto p = getItem(position);

		TextView codigo = (TextView) v.findViewById(R.id.txtCodigo);
		if (codigo.getText() == null || codigo.getText().toString().isEmpty())
			codigo.setVisibility(View.GONE);

		ImageView imageProduto = (ImageView) v.findViewById(R.id.imgProduto);
		carregarImagem(imageProduto, p);

		FrameLayout frameProduto = (FrameLayout) v.findViewById(R.id.frameImage);
		if (!StringHelper.isNullOrEmpty(p.getImage()) || !StringHelper.isNullOrEmpty(p.getImageExterna())) {
			frameProduto.setBackgroundResource(android.R.color.transparent);
			imageProduto.setScaleType(ScaleType.CENTER_CROP);
		} else {
			frameProduto.setBackgroundResource(R.color.tagplus_background_purple);
			imageProduto.setScaleType(ScaleType.CENTER);
		}

		return v;
	}

	protected void carregarImagem(ImageView imageView, Produto produto) {

		loadBitmap(produto, imageView);

		// try {
		// if (!StringHelper.isNullOrEmpty(produto.getImage())) {
		// File file = getContext().getFileStreamPath(produto.getImage());
		// if (file.exists()) {
		//
		// // Picasso.with(getContext()).load(file).into(imageView);
		// // Bitmap b = FileHelper.decodeBitmap(file);
		// // imageView.setImageBitmap(b);
		// }
		// } else if (!StringHelper.isNullOrEmpty(produto.getImageExterna())) {
		// loadBitmap(Uri.parse(produto.getImageExterna()), imageView);
		// Picasso.with(getContext()).load(Uri.parse(produto.getImageExterna())).into(imageView);
		// } else {
		// loadBitmap(R.drawable.images, imageView);
		// imageView.setImageResource(R.drawable.images);
		// imageView.setScaleType(ScaleType.CENTER);
		// }
		//
		// } catch (Exception e) {
		// Crashlytics.logException(e);
		// }
	}

	public List<Produto> getItems() {
		return list;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				notifyDataSetChanged();
				clear();
				if (results.count > 0)
					addAll((List<Produto>) results.values);

				notifyDataSetInvalidated();
			}

			@Override
			protected FilterResults performFiltering(CharSequence filterText) {

				FilterResults results = new FilterResults();

				List<Produto> items = (List<Produto>) list;

				if (filterText != null && !filterText.toString().isEmpty()) {

					List<Produto> filteredItems = new ArrayList<Produto>();

					for (Produto i : items) {
						if (i.filter(filterText))
							filteredItems.add((Produto) i);
					}

					results.count = filteredItems.size();
					results.values = filteredItems;
				} else {
					results.count = items.size();
					results.values = items;

				}
				return results;
			}
		};
	}

	/**
	 * BITMAP
	 * 
	 * @return
	 */

	class BitmapWorkerTask extends AsyncTask<Produto, Void, Bitmap> {
		private final WeakReference<ImageView> imageViewReference;
		private Produto data = null;

		public BitmapWorkerTask(ImageView imageView) {
			// Use a WeakReference to ensure the ImageView can be garbage
			// collected
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		// Decode image in background.
		@Override
		protected Bitmap doInBackground(Produto... params) {
			data = params[0];

			try {

				if (!StringHelper.isNullOrEmpty(data.getImage())) {
					File file = getContext().getFileStreamPath(data.getImage());
					if (file.exists())
						return Picasso.with(getContext()).load(file).get();
				} else if (!StringHelper.isNullOrEmpty(data.getImageExterna()))
					return Picasso.with(getContext()).load(Uri.parse(data.getImageExterna())).get();
				else
					return Picasso.with(getContext()).load(R.drawable.images).get();
			} catch (Exception ex) {
				Crashlytics.logException(ex);
			} catch (OutOfMemoryError e) {
				Crashlytics.logException(e);
			}

			return null;
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (isCancelled()) {
				bitmap = null;
			}

			if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
				final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
				if (this == bitmapWorkerTask && imageView != null) {
					imageView.setImageBitmap(bitmap);
				}
			}
		}
	}

	static class AsyncDrawable extends BitmapDrawable {
		private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

		public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
			super(res, bitmap);
			bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
		}

		public BitmapWorkerTask getBitmapWorkerTask() {
			return bitmapWorkerTaskReference.get();
		}
	}

	public static boolean cancelPotentialWork(Object data, ImageView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

		if (bitmapWorkerTask != null) {
			final Object bitmapData = bitmapWorkerTask.data;
			// If bitmapData is not yet set or it differs from the new data
			if (bitmapData == null || bitmapData != data) {
				// Cancel previous task
				bitmapWorkerTask.cancel(true);
			} else {
				// The same work is already in progress
				return false;
			}
		}
		// No task associated with the ImageView, or an existing task was
		// cancelled
		return true;
	}

	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();
			if (drawable instanceof AsyncDrawable) {
				final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
				return asyncDrawable.getBitmapWorkerTask();
			}
		}
		return null;
	}

	public void loadBitmap(Produto produto, ImageView imageView) {
		if (cancelPotentialWork(produto, imageView)) {
			Bitmap b = null;
			final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
			final AsyncDrawable asyncDrawable = new AsyncDrawable(getContext().getResources(), b, task);
			imageView.setImageDrawable(asyncDrawable);
			task.execute(produto);
		}
	}

}

package br.com.tagplus.pedidos.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Pedido;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.mobsandgeeks.adapters.InstantAdapter;

public class AdapterPedidoList extends InstantAdapter<Pedido> implements Filterable {

	private List<Pedido> list;
	private AdapterPedidoListener mListener;

	public AdapterPedidoList(Context context, List<Pedido> list, AdapterPedidoListener listener) {
		super(context, R.layout.list_pedido, Pedido.class, list);
		this.list = new ArrayList<Pedido>(list);
		this.mListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = super.getView(position, convertView, parent);

		final Pedido pedido = getItem(position);

		LinearLayout lnlData = (LinearLayout) v.findViewById(R.id.lnlData);
		TextView txtStatus = (TextView) v.findViewById(R.id.txtStatus);
		TextView txtFormaPagamento = (TextView) v.findViewById(R.id.txtFormaPagamento);

		if (StringHelper.isNullOrEmpty(pedido.getFormaPagamentoTexto()))
			txtFormaPagamento.setVisibility(View.GONE);

		int drawable = 0;

		if (pedido.getCodigoStatus() == Pedido.ABERTO) {
			lnlData.setBackgroundResource(android.R.color.holo_blue_light);
			drawable = R.drawable.ic_blue_small;
			txtStatus.setText(R.string.pedido_aberto);
		} else if (pedido.getCodigoStatus() == Pedido.CANCELADO) {
			lnlData.setBackgroundResource(android.R.color.holo_red_light);
			drawable = R.drawable.ic_red_small;
			txtStatus.setText(R.string.pedido_cancelado);
		} else if (pedido.getCodigoStatus() == Pedido.CONFIRMADO) {
			lnlData.setBackgroundResource(android.R.color.holo_green_light);
			drawable = R.drawable.ic_green_small;
			txtStatus.setText(R.string.pedido_confirmado);
		}

		txtStatus.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(drawable), null, null, null);

		ImageButton imgButton = (ImageButton) v.findViewById(R.id.btnPopup);
		imgButton.setOnClickListener(new OnClickListener() {

			@SuppressLint("ViewHolder")
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(getContext(), v);
				MenuInflater inflater = popup.getMenuInflater();

				if (pedido.getCodigoStatus() == Pedido.ABERTO)
					inflater.inflate(R.menu.pedido_item, popup.getMenu());
				else
					inflater.inflate(R.menu.pedido_fechado_item, popup.getMenu());

				popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						if (mListener != null) {
							switch (item.getItemId()) {
							case R.id.action_cancelar_pedido:
								mListener.cancelarPedido(pedido);
								return true;
							case R.id.action_visualizar_pedido:
							case R.id.action_editar_pedido:
								mListener.editarPedido(pedido);
								return true;
							case R.id.action_fechar_pedido:
								mListener.fecharPedido(pedido);
								return true;
							}
						}
						return true;
					}
				});
				popup.show();

			}
		});

		return v;
	}

	public List<Pedido> getItems() {
		return list;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				notifyDataSetChanged();
				clear();
				if (results.count > 0)
					addAll((List<Pedido>) results.values);

				notifyDataSetInvalidated();
			}

			@Override
			protected FilterResults performFiltering(CharSequence filterText) {

				FilterResults results = new FilterResults();

				List<Pedido> items = (List<Pedido>) list;

				if (filterText != null && !filterText.toString().isEmpty()) {

					List<Pedido> filteredItems = new ArrayList<Pedido>();

					for (Pedido i : items) {
						if (i.filter(filterText))
							filteredItems.add((Pedido) i);
					}

					results.count = filteredItems.size();
					results.values = filteredItems;
				} else {
					results.count = items.size();
					results.values = items;

				}
				return results;
			}
		};
	}

	public void clearItems() {
		clear();
	}

	public List<Pedido> getItemsFull() {
		return list;
	}

	public void setItems(List<Pedido> values) {
		clear();
		addAll(values);
	}

	public interface AdapterPedidoListener {
		void cancelarPedido(Pedido pedido);

		void editarPedido(Pedido pedido);

		void fecharPedido(Pedido pedido);
	}

}

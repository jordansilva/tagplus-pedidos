package br.com.tagplus.pedidos.ui.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * @author Jordan Silva ( @jordansilva )
 * @filename AdapterTextWatcher.java
 * @since 07/02/2013 19:13:55
 */
public class AdapterTextWatcher implements TextWatcher
{

	private final EditText view;
	private final TextWatcherListener listener;

	public interface TextWatcherListener
	{

		void onTextChanged(EditText view, String text);

	}

	public AdapterTextWatcher(EditText editText, TextWatcherListener listener)
	{
		this.view = editText;
		this.listener = listener;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		listener.onTextChanged(view, s.toString());
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
		// pass
	}

	@Override
	public void afterTextChanged(Editable s)
	{
		// pass
	}

}
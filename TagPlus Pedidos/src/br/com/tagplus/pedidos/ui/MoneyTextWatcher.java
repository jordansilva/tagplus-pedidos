package br.com.tagplus.pedidos.ui;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.NumberFormat;

import com.crashlytics.android.Crashlytics;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import br.com.tagplus.pedidos.helper.StringHelper;

public class MoneyTextWatcher implements TextWatcher {

	private final WeakReference<EditText> editTextWeakReference;

	public MoneyTextWatcher(EditText editText) {
		editTextWeakReference = new WeakReference<EditText>(editText);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable editable) {
		synchronized (this) {
			EditText editText = editTextWeakReference.get();
			if (editText == null)
				return;
			try {
				String s = editable.toString();
				editText.removeTextChangedListener(this);
				String cleanString = s.toString().replaceAll("[R$,.]", "");
				if (cleanString.length() == 0)
					cleanString = "0";
				BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100),
						BigDecimal.ROUND_FLOOR);
				String formatted = NumberFormat.getCurrencyInstance().format(parsed);
				int maxLength = StringHelper.getMaxLength(editText);
				if (maxLength > 0 && maxLength > formatted.length())
					editText.setText(formatted);

				editText.setSelection(editText.getText().length());
				editText.addTextChangedListener(this);
			} catch (Exception ex) {
				Crashlytics.logException(ex);
				editText.setText("");
			}
		}
	}
}
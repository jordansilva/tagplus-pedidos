package br.com.tagplus.pedidos.ui;

import java.lang.ref.WeakReference;
import java.util.UUID;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import br.com.tagplus.pedidos.ui.listener.AdapterPedidoProdutoListener;

public class QuantidadeTextWatcher implements TextWatcher {

	private final WeakReference<EditText> editTextWeakReference;
	private final AdapterPedidoProdutoListener listener;

	public QuantidadeTextWatcher(EditText editText, AdapterPedidoProdutoListener listener) {
		editTextWeakReference = new WeakReference<EditText>(editText);
		this.listener = listener;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable editable) {
		synchronized (this) {
			EditText editText = editTextWeakReference.get();
			if (editText == null)
				return;

			if (editable.length() > 0)
				listener.adicionarQuantidade((UUID) editText.getTag(), Integer.valueOf(editable.toString()));
			else
				listener.adicionarQuantidade((UUID) editText.getTag(), 0);
		}
	}
}
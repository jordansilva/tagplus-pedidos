package br.com.tagplus.pedidos.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class DialogListFragment extends DialogFragment {

	public final static String TAG = "DialogListFragment";
	private String title;
	private String[] list;
	private ListClickListener listener;

	public DialogListFragment() {
		setRetainInstance(true);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getList() {
		return list;
	}

	public void setList(String[] list) {
		this.list = list;
	}

	public ListClickListener getListener() {
		return listener;
	}

	public void setListener(ListClickListener listener) {
		this.listener = listener;
	}

	public interface ListClickListener {
		void onClick(int which);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity()).setTitle(title).setItems(list, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (listener != null)
					listener.onClick(which);

			}
		}).create();
	}
}

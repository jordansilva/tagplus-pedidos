package br.com.tagplus.pedidos.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import br.com.tagplus.pedidos.R;

public class DateDialogFragment extends DialogFragment implements DialogInterface.OnClickListener, OnDateSetListener {

	public static final String DAY = "DAY";
	public static final String MONTH = "MONTH";
	public static final String YEAR = "YEAR";
	public static final String TAG = DateDialogFragment.class.getSimpleName();

	public boolean mIsSubmitted = false;
	public OnDateSetListener mListener;

	public static DateDialogFragment newInstance() {
		return new DateDialogFragment();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		Bundle b = getArguments();
		int d = b.getInt(DAY);
		int m = b.getInt(MONTH);
		int y = b.getInt(YEAR);

		DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, y, m, d);
		dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.action_fechar), this);
		return dialog;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DatePickerDialog.BUTTON_POSITIVE:
			mIsSubmitted = true;
			break;
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		if (mIsSubmitted)
			mListener.onDateSet(view, year, monthOfYear, dayOfMonth);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnDateSetListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement OnDateSetListener.");
		}
	}

}

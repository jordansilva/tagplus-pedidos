package br.com.tagplus.pedidos.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class DialogDeleteFragment extends DialogFragment {

	public final static String TAG = "DialogDeleteFragment";
	private int titleId;
	private String message;
	private int yesButton = android.R.string.yes;
	private int noButton = android.R.string.no;
	private YesNoListener listener;

	public DialogDeleteFragment() {
		setRetainInstance(true);
	}

	public DialogDeleteFragment(int titleId, int messageId, YesNoListener listener) {
		this.titleId = titleId;
		this.message = getString(messageId);
		this.listener = listener;
	}
	
	public DialogDeleteFragment(int titleId, int messageId, int yesButton, int noButton, YesNoListener listener) {
		this.titleId = titleId;
		this.message = getString(messageId);
		this.yesButton = yesButton;
		this.noButton = noButton;
		this.listener = listener;
	}
	
	public DialogDeleteFragment(int titleId, String message, int yesButton, int noButton, YesNoListener listener) {
		this.titleId = titleId;
		this.message = message;
		this.yesButton = yesButton;
		this.noButton = noButton;
		this.listener = listener;
	}

	public int getTitleId() {
		return titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public YesNoListener getListener() {
		return listener;
	}

	public void setListener(YesNoListener listener) {
		this.listener = listener;
	}

	public interface YesNoListener {
		void onYes();

		void onNo();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity()).setTitle(titleId).setMessage(message)
				.setPositiveButton(yesButton, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (listener != null)
							listener.onYes();
					}
				}).setNegativeButton(noButton, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (listener != null)
							listener.onNo();
					}
				}).create();
	}
}

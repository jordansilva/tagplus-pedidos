package br.com.tagplus.pedidos.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterTextWatcher;
import br.com.tagplus.pedidos.ui.adapter.AdapterTextWatcher.TextWatcherListener;

/**
 * @author Jordan Silva ( @jordansilva )
 * @filename ClearableEditText.java
 * @since 07/02/2013 19:03:40
 */
public class ClearableEditText extends EditText implements OnTouchListener, OnFocusChangeListener, TextWatcherListener
{

	private boolean showClearButton = true;
	private Drawable xD;
	private ClearableEditTextListener listener;

	public interface ClearableEditTextListener
	{
		void didClearEditText();

		void onTextChanged(EditText view, String text);
	}

	public void setClearDrawable(int drawableResId)
	{
		xD = getResources().getDrawable(drawableResId);
		xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
	}

	public void setListener(ClearableEditTextListener listener)
	{
		this.listener = listener;
	}

	public ClearableEditText(Context context)
	{
		super(context);
		init();
	}

	public ClearableEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public ClearableEditText(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}

	private void init()
	{
		setClearDrawable(android.R.drawable.presence_offline);
		super.setOnTouchListener(this);
		super.setOnFocusChangeListener(this);
		addTextChangedListener(new AdapterTextWatcher(this, this));
	}

	@Override
	public void setOnTouchListener(OnTouchListener l)
	{
		this.l = l;
	}

	@Override
	public void setOnFocusChangeListener(OnFocusChangeListener f)
	{
		this.f = f;
	}

	private OnTouchListener l;
	private OnFocusChangeListener f;

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (getCompoundDrawables()[2] != null)
		{
			if (event.getAction() == MotionEvent.ACTION_UP)
			{
				boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - xD.getIntrinsicWidth());
				if (tappedX)
				{
					setText("");
					if (listener != null)
					{
						listener.didClearEditText();
						return true;
					}
					return false;
				}
			}
		}
		if (l != null)
		{
			return l.onTouch(v, event);
		}
		return false;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		if (hasFocus)
		{
			setClearShown(!StringHelper.isNullOrEmpty(getText().toString()));
		}
		else
		{
			setClearShown(false);
		}
		if (f != null)
		{
			f.onFocusChange(v, hasFocus);
		}
	}

	@Override
	public void onTextChanged(EditText view, String text)
	{
		setClearShown(!StringHelper.isNullOrEmpty(text));
		if (listener != null)
			listener.onTextChanged(view, text);
	}

	public void showClearButton(boolean showButton)
	{
		showClearButton = showButton;
	}
	
	private void setClearShown(boolean shown)
	{
		Drawable x = shown && showClearButton ? xD : null;
		setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
	}
}
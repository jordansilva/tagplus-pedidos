package br.com.tagplus.pedidos.ui;

import java.util.HashMap;

public class Session {

	private static HashMap<String, Object> session;
	private static Session instance = null;

	protected Session() {
		session = new HashMap<String, Object>();
	}

	static Session getInstance() {
		if (instance == null)
			instance = new Session();
		return instance;
	}

	@SuppressWarnings("static-access")
	public static void add(String key, Object value) {
		getInstance().session.put(key, value);
	}

	@SuppressWarnings("static-access")
	public static Object get(String key) {
		return getInstance().session.get(key);
	}

	@SuppressWarnings("static-access")
	public static boolean isset(String key) {
		return getInstance().session.containsKey(key);
	}

	@SuppressWarnings("static-access")
	public static void remove(String key) {
		getInstance().session.remove(key);
	}
}

package br.com.tagplus.pedidos.ui.listener;


public interface UpdateFragmentListener {

	void update();
	boolean validar();
}
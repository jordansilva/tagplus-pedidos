package br.com.tagplus.pedidos.ui.listener;

import java.util.Collection;

import br.com.tagplus.pedidos.business.domain.Atributo;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Produto;

public interface ProdutoListener {

	Produto getProduto();

	void updateFornecedores(Collection<Fornecedor> fornecedores);

	void updateAtributos(Collection<Atributo> atributos);

	void updateDetalhes(Produto detalhes);

	void salvarProduto();
}
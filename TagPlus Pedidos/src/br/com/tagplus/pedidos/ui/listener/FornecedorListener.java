package br.com.tagplus.pedidos.ui.listener;

import br.com.tagplus.pedidos.business.domain.Fornecedor;

public interface FornecedorListener {
	Fornecedor getFornecedor();
	
	void updateDetalhes(Fornecedor fornecedor);

	void salvarFornecedor();
}

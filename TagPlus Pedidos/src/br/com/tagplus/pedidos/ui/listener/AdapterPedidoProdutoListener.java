package br.com.tagplus.pedidos.ui.listener;

import java.util.UUID;

public interface AdapterPedidoProdutoListener {
	void notifyPedidoDataSetChanged();
	void adicionarQuantidade(UUID id, int quantidade);
}

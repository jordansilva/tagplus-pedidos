package br.com.tagplus.pedidos.ui.listener;

import br.com.tagplus.pedidos.business.domain.Cliente;

public interface ClienteListener {
	Cliente getCliente();

	void updateDetalhes(Cliente cliente);

	void salvarCliente();
}

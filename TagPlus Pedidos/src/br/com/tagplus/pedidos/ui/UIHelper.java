/**
 * 
 */
package br.com.tagplus.pedidos.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

/**
 * @author Jordan Silva ( @jordansilva )
 * @since 08/01/2014
 */
public class UIHelper {
	private static CustomProgressDialog mDialog;
	private static Activity mActivity;

	public static void showLoading(final Activity activity) {
//		if (activity != null && !activity.isChangingConfigurations())
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mDialog != null && mActivity != activity) {
						mDialog.dismiss();
						mDialog = null;
					}

					if (mDialog == null) {
						mDialog = new CustomProgressDialog(activity);
						mActivity = activity;
					}

					if (!mDialog.isShowing())
						mDialog.show();
				}
			});
	}

	public static void hideLoading(final Activity activity) {
//		if (activity != null && !activity.isChangingConfigurations())
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mDialog != null) {
						mDialog.dismiss();
					}
				}
			});
	}

	/**
	 * To check device is tablet or not.
	 * 
	 * @param context
	 * @return true if device is tablet
	 */
	public static boolean isTablet(Context context) {
		boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
		boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
		return (xlarge || large);
	}

	public static boolean isLandscape(Context context) {
		return context.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT;
	}
}

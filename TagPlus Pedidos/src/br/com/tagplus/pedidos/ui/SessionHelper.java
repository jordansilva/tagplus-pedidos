package br.com.tagplus.pedidos.ui;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import com.crashlytics.android.Crashlytics;

import android.content.Context;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Configuracao;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.helper.Constants;

@EBean(scope = Scope.Singleton)
public class SessionHelper {

	@RootContext
	Context context;

	@Pref
	MyPrefs_ prefs;

	public SessionHelper() {
		super();
	}

	public Perfil getUsuario() {
		if (Session.isset(Constants.Session.PERFIL))
			return (Perfil) Session.get(Constants.Session.PERFIL);
		else {
			if (logar())
				return (Perfil) Session.get(Constants.Session.PERFIL);
			else
				return null;
		}
	}

	private boolean logar() {
		String codigoUltimoUsuario = getPrefs().codigoUltimoUsuario().get();
		if (codigoUltimoUsuario != null && !codigoUltimoUsuario.isEmpty()) {
			new PerfilService(context).logar(codigoUltimoUsuario, new BusinessListener() {

				@Override
				public void onPreInit() {
				}

				@Override
				public void onPostExecute(RequestResult result) {
					if (!result.isError() && result.getData() != null) {
						Perfil perfil = (Perfil) result.getData();

						getPrefs().edit().isFirstTime().put(false).apply();
						getPrefs().edit().codigoUltimoUsuario().put(perfil.getNome()).apply();

						addUsuario(perfil);

						Crashlytics.setUserIdentifier(perfil.getCode().toString());
						Crashlytics.setUserName(perfil.getNome());
					}
				}
			});
			return true;
		} else
			return false;

	}

	public Configuracao getConfiguracao() {
		if (Session.isset(Constants.Session.PERFIL))
			return ((Perfil) Session.get(Constants.Session.PERFIL)).getConfiguracao();
		else
			return null;
	}

	public void addUsuario(Perfil perfil) {
		Session.add(Constants.Session.PERFIL, perfil);
	}

	public MyPrefs_ getPrefs() {
		return prefs;
	}

	public void limparSessao() {
		Session.remove(Constants.Session.PERFIL);
		prefs.edit().codigoUltimoUsuario().put(null).apply();
	}

}

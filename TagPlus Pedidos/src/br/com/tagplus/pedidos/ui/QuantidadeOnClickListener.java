package br.com.tagplus.pedidos.ui;

import java.util.UUID;

import android.view.View;
import android.view.View.OnClickListener;
import br.com.tagplus.pedidos.ui.listener.AdapterPedidoProdutoListener;

public class QuantidadeOnClickListener implements OnClickListener {

	private final AdapterPedidoProdutoListener listener;
	private final UUID id;
	private int initialValue;
	private final boolean sum;

	public QuantidadeOnClickListener(UUID id, int initialValue, boolean sum, AdapterPedidoProdutoListener listener) {
		this.id = id;
		this.listener = listener;
		this.initialValue = initialValue;
		this.sum = sum;
	}

	@Override
	public void onClick(View v) {
		if (sum)
			initialValue++;
		else
			initialValue--;

		listener.adicionarQuantidade(id, initialValue);
	}
}
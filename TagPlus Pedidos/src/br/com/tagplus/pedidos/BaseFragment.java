package br.com.tagplus.pedidos;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import br.com.tagplus.pedidos.business.domain.Perfil;

@EFragment
public abstract class BaseFragment extends Fragment {

	protected BaseActivity activity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@AfterViews
	protected void load() {
		configureActionBar();
	}

	protected void configureActionBar() {
		if (activity != null)
			activity.configureActionBar();
	}

	@UiThread
	protected void showToast(int texto) {
		Toast.makeText(getActivity(), texto, Toast.LENGTH_SHORT).show();
	}

	public Perfil getUsuarioLogado() {
		return activity.getUsuarioLogado();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = (BaseActivity) activity;
	}

	public void reconfigureMenu() {
		if (this.activity instanceof DrawerActivity)
			((DrawerActivity) activity).reloadMenu();
	}

	public void moverParaTelaPedidos() {
		if (this.activity instanceof DrawerActivity)
			((DrawerActivity) activity).moverParaTelaPedidos();
	}

	public void hideSoftKeyboard(View v) {
		if (activity != null && v != null && isAdded()) {
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromInputMethod(v.getWindowToken(), 0);
		}
	}

	public void showSoftKeyboard(View v) {
		if (activity != null && v != null && isAdded()) {
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

}

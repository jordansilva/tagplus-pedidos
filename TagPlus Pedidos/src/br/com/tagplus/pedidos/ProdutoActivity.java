package br.com.tagplus.pedidos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.OrientationEventListener;
import android.view.WindowManager;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ProdutoService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Atributo;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.SectionsPagerAdapter;
import br.com.tagplus.pedidos.ui.listener.ProdutoListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

@EActivity(R.layout.activity_viewpager)
public class ProdutoActivity extends BaseActivity implements ActionBar.TabListener, ProdutoListener {

	@ViewById(R.id.pager)
	ViewPager mViewPager;

	@Bean
	ProdutoService service;

	SectionsPagerAdapter mSectionsPagerAdapter;

	@Extra
	UUID mCodigoProduto;

	Produto mProduto;
	OrientationEventListener listener;
	int mOrientation = -1;
	Fragment currentFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void load() {
		super.load();

		if (mCodigoProduto == null) {
			mProduto = new Produto();
			configureUi();
		} else {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			service.getProduto(mCodigoProduto, new BusinessListener() {

				@Override
				public void onPreInit() {
					UIHelper.showLoading(ProdutoActivity.this);
				}

				@Override
				public void onPostExecute(RequestResult result) {
					UIHelper.hideLoading(ProdutoActivity.this);
					if (result.isError()) {
						finish();
						showToast(R.string.erro_generico);
					} else {
						mProduto = (Produto) result.getData();
						configureUi();
					}
				}
			});
		}
	}

	protected void configureUi() {
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		if (mProduto.getDescricao() != null && !mProduto.getDescricao().isEmpty())
			actionBar.setTitle(mProduto.getDescricao());

		mSectionsPagerAdapter = new SectionsPagerAdapter(this, getFragmentManager());
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_produto), ProdutoDetalheFragment_.class);
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_atributos), ProdutoAtributoFragment_.class);
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_fornecedores), ProdutoFornecedoresFragment_.class);

		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(3);
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			Tab tab = actionBar.newTab();
			tab.setText(mSectionsPagerAdapter.getPageTitle(i));
			tab.setTabListener(this);
			actionBar.addTab(tab);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.produto, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		UpdateFragmentListener page = (UpdateFragmentListener) getFragmentManager().findFragmentByTag(
				"android:switcher:" + R.id.pager + ":" + tab.getPosition());
		page.update();
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	@UiThread
	public void salvarProduto() {
		// Validar dados aqui!
		if (mProduto.validar()) {
			service.salvar(getUsuarioLogado().getCode(), mProduto, new BusinessListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					salvarProduto(result);
				}
			});
		} else
			mViewPager.setCurrentItem(0, true);
	}

	@Override
	public Produto getProduto() {
		return mProduto;
	}

	@Override
	public void updateDetalhes(Produto detalhes) {
		Collection<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
		Collection<Atributo> atributos = new ArrayList<Atributo>();

		if (mProduto != null) {
			fornecedores = mProduto.getFornecedores();
			atributos = mProduto.getAtributos();
		}

		mProduto = detalhes;
		updateAtributos(atributos);
		updateFornecedores(fornecedores);
	}

	@Override
	public void updateAtributos(Collection<Atributo> atributos) {
		if (mProduto == null)
			mProduto = new Produto();

		mProduto.setAtributos(atributos);
	}

	@Override
	public void updateFornecedores(Collection<Fornecedor> fornecedores) {
		if (mProduto == null)
			mProduto = new Produto();

		mProduto.setFornecedores(fornecedores);
	}

	@UiThread
	protected void salvarProduto(RequestResult result) {
		if (!result.isError()) {
			if (mProduto.getCode() == null)
				showToast(R.string.produto_criado);
			else
				showToast(R.string.produto_salvo);

			setResult(RESULT_OK);
			finish();
		}
	}

}

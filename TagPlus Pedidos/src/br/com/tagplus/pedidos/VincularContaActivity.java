package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.view.View;
import android.widget.Button;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.SincronizarService;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.SessionHelper;
import br.com.tagplus.pedidos.ui.UIHelper;

import com.iangclifton.android.floatlabel.FloatLabel;

@EActivity(R.layout.activity_vincular_conta)
public class VincularContaActivity extends BaseActivity implements BusinessListener {

	@Bean
	SessionHelper mSession;

	@Bean
	SincronizarService mService;

	@Bean
	PerfilService mPerfilService;

	@ViewById
	FloatLabel txtSistema;

	@ViewById
	FloatLabel txtUsuario;

	@ViewById
	FloatLabel txtSenha;

	@ViewById
	Button btnVincular;

	String mSistema;
	String mUsuario;
	String mSenha;

	@Override
	protected void load() {
		super.load();

		if (getUsuarioLogado() != null)
			carregarDados();
	}

	protected void carregarDados() {
		Perfil p = getUsuarioLogado();
		if (!StringHelper.isNullOrEmpty(p.getSistema())) {
			txtSistema.setText(p.getSistema());
			txtSistema.setEnabled(false);
		}
		if (!StringHelper.isNullOrEmpty(p.getUsuario())) {
			txtUsuario.setText(p.getUsuario());
		}
	}

	protected boolean validarCampos() {

		boolean isValid = true;

		if (StringHelper.isNullOrEmpty(txtSistema.getTextValue())) {
			isValid = false;
			txtSistema.setError(getString(R.string.campo_obrigatorio));
		}

		if (StringHelper.isNullOrEmpty(txtUsuario.getTextValue())) {
			isValid = false;
			txtUsuario.setError(getString(R.string.campo_obrigatorio));
		}

		if (StringHelper.isNullOrEmpty(txtSenha.getTextValue())) {
			isValid = false;
			txtSenha.setError(getString(R.string.campo_obrigatorio));
		}

		return isValid;
	}

	@Click(R.id.btnVincular)
	protected void onClickVincularConta(View v) {
		if (validarCampos()) {

			mSistema = txtSistema.getTextValue();
			mUsuario = txtUsuario.getTextValue();
			mSenha = txtSenha.getTextValue();

			vincular(mSistema, mUsuario, mSenha);
		}
	}

	@Background
	protected void vincular(String sistema, String usuario, String senha) {
		mService.vincularConta(sistema, usuario, senha, getUsuarioLogado(), this);
	}

	@Override
	public void onPreInit() {
		UIHelper.showLoading(this);
	}

	@Override
	public void onPostExecute(RequestResult result) {
		UIHelper.hideLoading(this);
		if (!result.isError()) {
			mSession.addUsuario((Perfil) result.getData());
			setResult(RESULT_OK);
			finish();
		} else {
			int message = R.string.erro_generico;
			switch (result.getCode()) {
			case 404:
				message = R.string.error_not_found;
				break;
			case 401:
				message = R.string.error_unauthorized;
				break;
			}

			showToast(message);
		}

	}

}

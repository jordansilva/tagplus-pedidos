package br.com.tagplus.pedidos;

import java.util.List;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.Detalhe;
import br.com.tagplus.pedidos.ui.StretchedListView;
import br.com.tagplus.pedidos.ui.listener.ClienteListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

import com.jordansilva.android.prettyui.ExtendedTextView;
import com.mobsandgeeks.adapters.InstantAdapter;

@EFragment(R.layout.fragment_geral_detalhe)
public class ClienteAtributoFragment extends BaseFragment implements UpdateFragmentListener {

	@ViewById(R.id.emptyview_no_items)
	ExtendedTextView text;

	@ViewById(R.id.lnlGeralMessage)
	LinearLayout lnlGeral;

	@ViewById
	TextView txtDetalhes;

	@ViewById
	StretchedListView listView;

	@ViewById(R.id.emptyview_divisor)
	View view;

	@ViewById(R.id.emptyview_add_new_item)
	Button button;

	Cliente cliente;
	ClienteListener listener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();
		cliente = listener.getCliente();
		configureUi();
	}

	protected void configureUi() {
		if (getUsuarioLogado().isVinculado()) {

			button.setVisibility(View.GONE);
			view.setVisibility(View.GONE);

			if (cliente != null && cliente.getDetalhes() != null && cliente.getDetalhes().size() > 0) {
				lnlGeral.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
				carregarDetalhes();
			} else
				text.setText(R.string.emptyview_no_detalhes);
		}
	}

	protected void carregarDetalhes() {
		List<Detalhe> detalhes = (List<Detalhe>) cliente.getDetalhes();
		if (detalhes != null && detalhes.size() > 0) {
			InstantAdapter<Detalhe> adapter = new InstantAdapter<Detalhe>(getActivity(), R.layout.list_descricao_layout, Detalhe.class,
					detalhes);
			listView.setAdapter(adapter);

			if (detalhes.size() > 0)
				txtDetalhes.setVisibility(View.VISIBLE);
		}
	}

	@Click(R.id.emptyview_add_new_item)
	protected void onClickVincularConta() {
		Intent intent = new Intent(getActivity(), ContaTagPlusActivity_.class);
		startActivityForResult(intent, 999);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (ClienteListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement ClienteListener.");
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 999 && resultCode == Activity.RESULT_OK) {
			configureUi();
			reconfigureMenu();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void update() {
	}
	
	@Override
	public boolean validar() {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;

		int id = item.getItemId();
		if (id == R.id.action_save) {
			try {
				update();
				listener.salvarCliente();
				result = false;
			} catch (Exception ex) {
				result = true;
			}
		}

		return result;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		update();
	}

}

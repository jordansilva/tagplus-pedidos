package br.com.tagplus.pedidos;

import java.util.ArrayList;
import java.util.UUID;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ProdutoService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterProdutoGrid;

import com.iangclifton.android.floatlabel.FloatLabel;
import com.jordansilva.android.prettyui.RelativeLayoutAnimation;
import com.jordansilva.android.prettyui.empty.EmptyView;

@EFragment(R.layout.fragment_grid)
public class ListaProdutoGradeFragment extends BaseFragment implements BusinessListener, OnItemClickListener, OnQueryTextListener,
		OnActionExpandListener {

	@ViewById(R.id.grid)
	GridView gridView;

	@ViewById
	RelativeLayoutAnimation buscaAvancada;

	@ViewById
	FloatLabel txtFornecedor;

	@ViewById
	FloatLabel txtCategoria;

	@ViewById
	FloatLabel txtAtributo;

	@ViewById
	TextView txtQuantidadeProdutos;

	@ViewById
	LinearLayout headerResultado;

	@ViewById
	EmptyView emptyView;

	@Bean
	ProdutoService service;

	Menu mMenu;
	AdapterProdutoGrid mAdapter;
	private SearchView mSearchView;
	boolean isFiltrado = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();
		listarProdutos();
		emptyView.setButtonText(R.string.action_adicionar_novo_produto);
		emptyView.setListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarNovo();
			}
		});
	}

	@Background
	protected void listarProdutos() {
		service.listarProdutos(getUsuarioLogado().getCode(), this);
	}

	@Override
	@UiThread
	public void onPreInit() {
		if (isAdded())
			UIHelper.showLoading(getActivity());
	}

	@Override
	@UiThread
	public void onPostExecute(RequestResult result) {
		if (isAdded()) {
			UIHelper.hideLoading(getActivity());
			carregarLista(result);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.lista_produto, menu);

		mMenu = menu;

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MenuItem searchItem = menu.findItem(R.id.action_search);
		searchItem.setOnActionExpandListener(this);

		mSearchView = (SearchView) searchItem.getActionView();
		mSearchView.setOnQueryTextListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			adicionarNovo();
			return true;
		case R.id.action_busca_avancada:
			exibirBuscaAvancada();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void adicionarNovo() {
		Intent intent = ProdutoActivity_.intent(getActivity()).get();
		startActivityForResult(intent, 1);
	}

	@UiThread
	protected void carregarLista(RequestResult result) {
		if (!result.isError()) {

			@SuppressWarnings("unchecked")
			ArrayList<Produto> list = (ArrayList<Produto>) result.getData();

			if (isAdded()) {
				if (buscaAvancada.getVisibility() == View.VISIBLE)
					exibirBuscaAvancada();

				mAdapter = new AdapterProdutoGrid(getActivity(), list);
				gridView.setAdapter(mAdapter);
				gridView.setOnItemClickListener(this);
				gridView.setEmptyView(emptyView);
				verificarHeader();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK)
			listarProdutos();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object obj = parent.getItemAtPosition(position);
		if (obj != null) {
			UUID code = ((Produto) obj).getCode();
			Intent intent = ProdutoActivity_.intent(getActivity()).mCodigoProduto(code).get();
			startActivityForResult(intent, 1);
		}
	}

	protected void verificarHeader() {
		if (isFiltrado) {
			headerResultado.setVisibility(View.VISIBLE);
			txtQuantidadeProdutos.setText(String.valueOf(mAdapter.getCount()));
		} else
			headerResultado.setVisibility(View.GONE);
	}

	protected void exibirBuscaAvancada() {

		final int height = (buscaAvancada.getHeight() + buscaAvancada.getPaddingTop());

		if (buscaAvancada.getVisibility() == View.GONE)
			buscaAvancada.setVisibility(View.INVISIBLE);

		if (buscaAvancada.getVisibility() == View.INVISIBLE) {

			if (mAdapter == null || mAdapter.getCount() == 0)
				emptyView.setVisibility(View.INVISIBLE);

			buscaAvancada.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buscaAvancada.getLayoutParams();
					params.topMargin += height;
					buscaAvancada.setVisibility(View.VISIBLE);
					buscaAvancada.setLayoutParams(params);
				}
			});

			AnimationSet animationSet = new AnimationSet(true);

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, height);
			translate.setDuration(300);
			animationSet.addAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(0, 1);
			alpha.setDuration(300);
			animationSet.addAnimation(alpha);

			buscaAvancada.startAnimation(animationSet);
		}

		if (buscaAvancada.getVisibility() == View.VISIBLE) {

			if (mAdapter == null || mAdapter.getCount() == 0)
				emptyView.setVisibility(View.VISIBLE);

			buscaAvancada.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buscaAvancada.getLayoutParams();
					params.topMargin -= height;
					buscaAvancada.setVisibility(View.INVISIBLE);
					buscaAvancada.setLayoutParams(params);
				}
			});

			AnimationSet animationSet = new AnimationSet(true);

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, -height);
			translate.setDuration(300);
			animationSet.addAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(1, 0);
			alpha.setDuration(300);
			animationSet.addAnimation(alpha);

			buscaAvancada.startAnimation(animationSet);
		}
	}

	@Click(R.id.btnPesquisar)
	protected void onClickPesquisarAvancado() {

		isFiltrado = true;
		service.listarProdutos(getUsuarioLogado().getCode(), txtFornecedor.getTextValue(), txtAtributo.getTextValue(),
				txtCategoria.getTextValue(), this);

		hideSoftKeyboard(txtFornecedor);
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(true);
			mMenu.setGroupVisible(R.id.groupAvancado, false);
			if (buscaAvancada.getVisibility() == View.VISIBLE)
				exibirBuscaAvancada();
			return true;
		}

		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(false);
			mMenu.setGroupVisible(R.id.groupAvancado, true);

			return true;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		search(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		search(query);
		return false;
	}

	private void search(String query) {
		if (mAdapter != null) {
			mAdapter.getFilter().filter(query);
			isFiltrado = true;
			verificarHeader();
		}
	}

	@Click({ R.id.btnLimpar, R.id.btnCancelar })
	protected void onClickLimparFiltro(View v) {

		mSearchView.setQuery("", false);
		txtFornecedor.getEditText().setText("");
		txtAtributo.getEditText().setText("");
		txtCategoria.getEditText().setText("");

		isFiltrado = false;
		listarProdutos();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

}

package br.com.tagplus.pedidos;

import java.util.UUID;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.WindowManager;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ClienteService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.SectionsPagerAdapter;
import br.com.tagplus.pedidos.ui.listener.ClienteListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

@EActivity(R.layout.activity_viewpager)
public class ClienteActivity extends BaseActivity implements ActionBar.TabListener, ClienteListener {

	@ViewById(R.id.pager)
	ViewPager mViewPager;

	@Bean
	ClienteService service;

	SectionsPagerAdapter mSectionsPagerAdapter;

	@Extra
	UUID codigoCliente;

	@Extra
	Cliente mCliente;

	int currPage = 0;

	@Override
	protected void load() {
		super.load();

		if (codigoCliente == null && mCliente == null) {
			mCliente = new Cliente();
			configureUi();
		} else if (codigoCliente != null && mCliente == null) {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			service.getCliente(codigoCliente, new BusinessListener() {

				@Override
				public void onPreInit() {
					UIHelper.showLoading(ClienteActivity.this);
				}

				@Override
				public void onPostExecute(RequestResult result) {
					UIHelper.hideLoading(ClienteActivity.this);
					if (result.isError()) {
						finish();
						showToast(R.string.erro_generico);
					} else {
						mCliente = (Cliente) result.getData();
						configureUi();
					}
				}
			});
		} else {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			configureUi();
		}
	}

	protected void configureUi() {
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		if (mCliente != null && mCliente.getNome() != null && !mCliente.getNome().isEmpty())
			actionBar.setTitle(mCliente.getNome());

		mSectionsPagerAdapter = new SectionsPagerAdapter(this, getFragmentManager());
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_cliente), ClienteDetalheFragment_.class);
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_detalhes), ClienteAtributoFragment_.class);

		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			Tab tab = actionBar.newTab();
			tab.setText(mSectionsPagerAdapter.getPageTitle(i));
			tab.setTabListener(this);
			actionBar.addTab(tab);
		}

		mViewPager.setCurrentItem(currPage);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cliente, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		UpdateFragmentListener page = (UpdateFragmentListener) getFragmentManager().findFragmentByTag(
				"android:switcher:" + R.id.pager + ":" + tab.getPosition());
		page.update();
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public Cliente getCliente() {
		return mCliente;
	}

	@Override
	public void updateDetalhes(Cliente cliente) {
		if (cliente != null)
			mCliente = cliente;
		else
			mCliente = new Cliente();
	}

	@Override
	@UiThread
	public void salvarCliente() {
		if (mCliente.validar()) {
			service.salvar(getUsuarioLogado().getCode(), mCliente, new BusinessListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					salvarCliente(result);
				}
			});
		} else {
			mViewPager.setCurrentItem(0, true);
			UpdateFragmentListener page = (UpdateFragmentListener) getFragmentManager().findFragmentByTag(
					"android:switcher:" + R.id.pager + ":0");
			page.validar();
		}

	}

	@UiThread
	protected void salvarCliente(RequestResult result) {
		if (!result.isError()) {
			if (mCliente.getCode() == null)
				showToast(R.string.cliente_criado);
			else
				showToast(R.string.cliente_salvo);

			mCliente = (Cliente) result.getData();

			Intent intent = new Intent();
			intent.putExtra("data", mCliente);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			if (result.getCode() == -1)
				showToast("Oops! " + result.getMessage());
		}
	}

}

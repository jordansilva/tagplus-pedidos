package br.com.tagplus.pedidos.repository;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Cidade;
import br.com.tagplus.pedidos.business.domain.Estado;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.ProdutoFornecedor;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;

public class FornecedorRepository extends RepositoryBase<Fornecedor> {

	public FornecedorRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public List<Fornecedor> listar(UUID perfil) throws SQLException {
		
		String sql = "SELECT f.code, f.nome, f.email, f.telefone, f.isPessoaFisica, f.isPessoaJuridica, f.cidade_id, "
				+ "cid.nome, est.code, est.nome, est.sigla FROM fornecedor f " + "LEFT JOIN cidade cid ON f.cidade_id = cid.code "
				+ "LEFT JOIN estado est ON cid.estado_id = est.code " + "WHERE f.perfil_id = '" + perfil.toString() + "' and f.isAtivo = 1 "
				+ "ORDER BY f.nome COLLATE NOCASE asc";

		return getDao().queryRaw(sql, new RawRowMapper<Fornecedor>() {

			@Override
			public Fornecedor mapRow(String[] arg0, String[] arg1) throws SQLException {
				Fornecedor item = new Fornecedor();
				item.setCode(UUID.fromString(arg1[0]));
				item.setNome(arg1[1]);
				item.setEmail(arg1[2]);
				item.setTelefone(arg1[3]);
				item.setPessoaFisica((Integer.valueOf(arg1[4]) == 1));
				item.setPessoaJuridica((Integer.valueOf(arg1[5]) == 1));

				if (arg1[6] != null) {
					Cidade cid = new Cidade();
					cid.setCode(UUID.fromString(arg1[6]));
					cid.setNome(arg1[7]);

					Estado est = new Estado();
					est.setCode(UUID.fromString(arg1[8]));
					est.setNome(arg1[9]);
					est.setSigla(arg1[10]);

					cid.setEstado(est);
					item.setCidade(cid);
				}

				return item;
			}
		}).getResults();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(Fornecedor item) throws SQLException {

		if (item.getCodigoExterno() != null && !item.getCodigoExterno().isEmpty()) {
			Fornecedor f = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and()
					.eq("codigoExterno", item.getCodigoExterno()).prepare());
			if (f != null)
				item.setCode(f.getCode());
		}

		if (item.getCpf() != null && !item.getCpf().isEmpty()) {
			Fornecedor f = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and().eq("cpf", item.getCpf()).prepare());
			if (f != null && !f.getCode().equals(item.getCode()))
				throw new SQLException("Cpf já cadastrado");
		}

		if (item.getCnpj() != null && !item.getCnpj().isEmpty()) {
			Fornecedor f = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and().eq("cnpj", item.getCnpj()).prepare());
			if (f != null && !f.getCode().equals(item.getCode()))
				throw new SQLException("Cnpj já cadastrado");
		}

		if (item.getUltimaAlteracao() == null)
			item.setUltimaAlteracao(DateTime.now());
		super.save(item);

	}

	@SuppressWarnings("unchecked")
	public List<Fornecedor> listarPorData(UUID perfil, DateTime ultimaSincronizacao) throws SQLException {
		if (ultimaSincronizacao != null)
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).and().gt("ultimaAlteracao", ultimaSincronizacao).prepare());
		else
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).prepare());
	}

	@SuppressWarnings("unchecked")
	public Fornecedor getCodigoExterno(UUID perfil, String fornecedorId) throws SQLException {
		return Get(newQuery().where().eq("perfil_id", perfil).and().eq("codigoExterno", fornecedorId).prepare());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deletePorCodigoExterno(UUID perfil, String id) throws SQLException {
		Fornecedor f = getCodigoExterno(perfil, id);
		if (f != null) {
			Dao produtoFornecedor = getDao(ProdutoFornecedor.class);
			produtoFornecedor.delete(produtoFornecedor.queryBuilder().where().eq("fornecedor_id", f.getCode()).query());

			delete(f);
		}
	}
}

package br.com.tagplus.pedidos.repository;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Cidade;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.Estado;
import br.com.tagplus.pedidos.business.domain.Pedido;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;

public class ClienteRepository extends RepositoryBase<Cliente> {

	public ClienteRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> listar(UUID perfil) throws SQLException {

		String sql = "SELECT c.code, c.nome, c.email, c.telefone, c.isPessoaFisica, c.isPessoaJuridica, c.cidade_id, "
				+ "cid.nome, est.code, est.nome, est.sigla FROM cliente c " + "LEFT JOIN cidade cid ON c.cidade_id = cid.code "
				+ "LEFT JOIN estado est ON cid.estado_id = est.code " + "WHERE c.perfil_id = '" + perfil.toString()
				+ "' and c.isAtivo = 1 " + "ORDER BY c.nome COLLATE NOCASE asc";

		return getDao().queryRaw(sql, new RawRowMapper<Cliente>() {

			@Override
			public Cliente mapRow(String[] arg0, String[] arg1) throws SQLException {
				Cliente item = new Cliente();
				item.setCode(UUID.fromString(arg1[0]));
				item.setNome(arg1[1]);
				item.setEmail(arg1[2]);
				item.setTelefone(arg1[3]);
				item.setPessoaFisica((Integer.valueOf(arg1[4]) == 1));
				item.setPessoaJuridica((Integer.valueOf(arg1[5]) == 1));

				if (arg1[6] != null) {
					Cidade cid = new Cidade();
					cid.setCode(UUID.fromString(arg1[6]));
					cid.setNome(arg1[7]);

					Estado est = new Estado();
					est.setCode(UUID.fromString(arg1[8]));
					est.setNome(arg1[9]);
					est.setSigla(arg1[10]);

					cid.setEstado(est);
					item.setCidade(cid);
				}

				return item;
			}
		}).getResults();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(Cliente item) throws SQLException {
		if (item.getCodigoExterno() != null && !item.getCodigoExterno().isEmpty()) {
			Cliente c = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and()
					.eq("codigoExterno", item.getCodigoExterno()).prepare());
			if (c != null)
				item.setCode(c.getCode());
		}

		if (item.getCpf() != null && !item.getCpf().isEmpty()) {
			Cliente c = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and().eq("cpf", item.getCpf()).prepare());
			if (c != null && !c.getCode().equals(item.getCode()))
				throw new SQLException("Cpf já cadastrado");
		}

		if (item.getCnpj() != null && !item.getCnpj().isEmpty()) {
			Cliente c = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and().eq("cnpj", item.getCnpj()).prepare());
			if (c != null && !c.getCode().equals(item.getCode()))
				throw new SQLException("Cnpj já cadastrado");
		}

		if (item.getUltimaAlteracao() == null)
			item.setUltimaAlteracao(DateTime.now());

		super.save(item);
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> listarPorData(UUID perfil, DateTime ultimaSincronizacao) throws SQLException {
		if (ultimaSincronizacao != null)
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).and()
					.gt("ultimaAlteracao", ultimaSincronizacao).prepare());
		else
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).prepare());
	}

	@SuppressWarnings("unchecked")
	public Cliente getCodigoExterno(UUID perfil, String clienteId) throws SQLException {
		return Get(newQuery().where().eq("perfil_id", perfil).and().eq("codigoExterno", clienteId).prepare());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deletePorCodigoExterno(UUID perfil, String id) throws SQLException {
		Cliente cliente = getCodigoExterno(perfil, id);
		if (cliente != null) {
			cliente.setAtivo(false);

			Dao pedidoDao = getDao(Pedido.class);
			List<Pedido> items = pedidoDao.queryBuilder().where().eq("cliente_id", cliente.getCode()).query();

			if (items.size() == 0)
				delete(cliente);
			else
				save(cliente);
		}
	}
}

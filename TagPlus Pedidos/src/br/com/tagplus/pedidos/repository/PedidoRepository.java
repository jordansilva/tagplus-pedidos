package br.com.tagplus.pedidos.repository;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.FormaPagamento;
import br.com.tagplus.pedidos.business.domain.Pedido;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.DeleteBuilder;

public class PedidoRepository extends RepositoryBase<Pedido> {

	public PedidoRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public List<Pedido> listar(UUID perfil) throws SQLException {

		String sql = "SELECT p.code, p.codigoStatus, p.dataPedido, p.total, c.nome, "
				+ "(SELECT COUNT(1) FROM pedido_produto pp WHERE pp.pedido_id = p.code) as qtd, p.codigoExterno " + "FROM pedido p "
				+ "LEFT JOIN cliente c ON c.code = p.cliente_id " + "WHERE p.perfil_id = '" + perfil.toString() + "' "
				+ "ORDER BY p.dataPedido DESC, p.codigoExterno DESC, p.code DESC";

		return getDao().queryRaw(sql, new RawRowMapper<Pedido>() {

			@Override
			public Pedido mapRow(String[] arg0, String[] arg1) throws SQLException {
				Pedido p = new Pedido();
				p.setCode(UUID.fromString(arg1[0]));
				p.setCodigoStatus(Integer.valueOf(arg1[1]));
				p.setDataPedido(new DateTime(Long.valueOf(arg1[2])));

				BigDecimal total = BigDecimal.ZERO;
				if (arg1[3] != null)
					total = new BigDecimal(arg1[3]);

				p.setTotal(total);

				if (arg1[4] != null) {
					Cliente c = new Cliente();
					c.setNome(String.valueOf(arg1[4]));
					p.setCliente(c);
				}

				p.setQuantidadeProdutos(Integer.valueOf(arg1[5]));

				return p;
			}
		}).getResults();
		//
		// return List(newQuery().where().eq("perfil_id", perfil).prepare());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(Pedido item) throws SQLException {

		// Valida se existe pedido já vinculado
		if (item.getCodigoExterno() != null && !item.getCodigoExterno().isEmpty()) {
			Pedido f = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and()
					.eq("codigoExterno", item.getCodigoExterno()).prepare());
			if (f != null)
				item.setCode(f.getCode());
		}

		if (item.getUltimaAlteracao() == null)
			item.setUltimaAlteracao(DateTime.now());

		super.save(item);

		List<PedidoProduto> pedidoProdutos = (List<PedidoProduto>) item.getProdutos();
		Dao<PedidoProduto, UUID> daoPedidoProduto = getDao(PedidoProduto.class);
		DeleteBuilder<PedidoProduto, UUID> deletePedidoProduto = daoPedidoProduto.deleteBuilder();
		deletePedidoProduto.where().eq("pedido_id", item.getCode());
		daoPedidoProduto.delete(deletePedidoProduto.prepare());

		for (PedidoProduto a : pedidoProdutos) {
			a.setPedido(item);
			daoPedidoProduto.createOrUpdate(a);
		}

		List<FormaPagamento> formaPagamento = (List<FormaPagamento>) item.getFormaPagamento();
		Dao<FormaPagamento, UUID> daoFormaPagamento = getDao(FormaPagamento.class);
		DeleteBuilder<FormaPagamento, UUID> deleteFormaPagamento = daoFormaPagamento.deleteBuilder();
		deleteFormaPagamento.where().eq("pedido_id", item.getCode());
		daoFormaPagamento.delete(deleteFormaPagamento.prepare());

		for (FormaPagamento f : formaPagamento) {
			f.setPedido(item);
			daoFormaPagamento.createOrUpdate(f);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Pedido> listarPorData(UUID codigoPerfil, DateTime ultimaSincronizacao) throws SQLException {
		if (ultimaSincronizacao != null)
			return List(newQuery().where().eq("perfil_id", codigoPerfil).and().gt("ultimaAlteracao", ultimaSincronizacao).prepare());
		else
			return List(newQuery().where().eq("perfil_id", codigoPerfil).prepare());
	}

	@SuppressWarnings("unchecked")
	public Pedido getCodigoExterno(UUID perfil, String pedidoId) throws SQLException {
		return Get(newQuery().where().eq("perfil_id", perfil).and().eq("codigoExterno", pedidoId).prepare());
	}

	public void deletePorCodigoExterno(UUID perfil, String id) throws SQLException {
		Pedido pedido = getCodigoExterno(perfil, id);
		if (pedido != null) {
			pedido.setCodigoStatus(Pedido.CANCELADO);

			// delete(pedido);
			save(pedido);
		}
	}

}

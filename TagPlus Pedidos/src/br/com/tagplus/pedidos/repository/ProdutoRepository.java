package br.com.tagplus.pedidos.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Atributo;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.business.domain.ProdutoFornecedor;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class ProdutoRepository extends RepositoryBase<Produto> {

	public ProdutoRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public List<Produto> listar(UUID perfil) throws SQLException {

		return List(newQuery().selectColumns("codigoInterno", "descricao", "valorDoProduto", "image", "imageExterna")
				.orderByRaw("descricao COLLATE NOCASE").where().eq("perfil_id", perfil).and().eq("isAtivo", true).prepare());

	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(Produto item) throws SQLException {

		// Valida se existe produto já vinculado
		if (item.getCodigoInterno() != null && !item.getCodigoInterno().isEmpty()) {
			Produto f = Get(newQuery().where().eq("perfil_id", item.getPerfil().getCode()).and()
					.eq("codigoExterno", item.getCodigoExterno()).prepare());
			if (f != null)
				item.setCode(f.getCode());
		}

		if (item.getUltimaAlteracao() == null)
			item.setUltimaAlteracao(DateTime.now());

		super.save(item);

		// Save Atributos
		if (item.getAtributos() != null) {
			List<Atributo> atributos = (List<Atributo>) item.getAtributos();
			Dao<Atributo, UUID> daoAtributo = getDao(Atributo.class);
			DeleteBuilder<Atributo, UUID> deleteAtributo = daoAtributo.deleteBuilder();
			deleteAtributo.where().eq("produto_id", item.getCode());
			daoAtributo.delete(deleteAtributo.prepare());

			for (Atributo a : atributos) {
				a.setProduto(item);
				daoAtributo.createOrUpdate(a);
			}
		}

		// Save fornecedores
		if (item.getFornecedores() != null) {
			List<Fornecedor> fornecedores = (List<Fornecedor>) item.getFornecedores();
			Dao<ProdutoFornecedor, UUID> daoProdutoFornecedor = getDao(ProdutoFornecedor.class);
			DeleteBuilder<ProdutoFornecedor, UUID> deleteProdutoFornecedor = daoProdutoFornecedor.deleteBuilder();
			deleteProdutoFornecedor.where().eq("produto_id", item.getCode());
			daoProdutoFornecedor.delete(deleteProdutoFornecedor.prepare());

			Dao<Fornecedor, UUID> daoFornecedor = getDao(Fornecedor.class);

			for (Fornecedor f : fornecedores) {
				if (f.getCode() == null || StringHelper.isNullOrEmpty(f.getCode().toString())) {
					f.setPerfil(item.getPerfil());
					f.setUltimaAlteracao(DateTime.now());
					daoFornecedor.createOrUpdate(f);
				}

				daoProdutoFornecedor.createOrUpdate(new ProdutoFornecedor(item, f));
			}
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Produto> listar(UUID codigoPerfil, String fornecedor, String atributos, String categoria) throws SQLException {

		QueryBuilder<Produto, UUID> queryProduto = newQuery();
		Where<Produto, UUID> where = newQuery().where().eq("perfil_id", codigoPerfil).and().eq("isAtivo", true);

		if (!StringHelper.isNullOrEmpty(categoria))
			where = where.and().like("categoria", "%" + categoria + "%");

		if (!StringHelper.isNullOrEmpty(atributos)) {
			Dao<Atributo, UUID> daoAtributos = getDao(Atributo.class);
			QueryBuilder<Atributo, UUID> queryAtributo = daoAtributos.queryBuilder();
			queryAtributo.where().like("descricao", "%" + atributos + "%").or().like("conteudo", "%" + atributos + "%");

			queryProduto.leftJoin(queryAtributo);
		}

		if (!StringHelper.isNullOrEmpty(fornecedor)) {
			Dao<Fornecedor, UUID> daoFornecedor = getDao(Fornecedor.class);
			QueryBuilder<Fornecedor, UUID> queryFornecedor = daoFornecedor.queryBuilder();
			queryFornecedor.where().like("nome", "%" + fornecedor + "%");

			Dao<ProdutoFornecedor, UUID> daoProdutoFornecedor = getDao(ProdutoFornecedor.class);
			QueryBuilder<ProdutoFornecedor, UUID> queryProdutoFornecedor = daoProdutoFornecedor.queryBuilder();
			queryProdutoFornecedor.join(queryFornecedor);

			queryProduto.leftJoin(queryProdutoFornecedor);
		}

		queryProduto.setWhere(where);
		return (ArrayList<Produto>) queryProduto.distinct().query();

		// GenericRawResults<UUID> result = getDao().queryRaw(sql, new
		// RawRowMapper<UUID>() {
		// @Override
		// public UUID mapRow(String[] columnNames, String[] resultColumns)
		// throws SQLException {
		// return UUID.fromString(resultColumns[0]);
		// }
		// });

		// return (ArrayList<Produto>) newQuery().where().in("code",
		// result.getResults()).query();

		// + " WHERE p.categoria like '%a%'"
		// if (StringHelper.isNullOrEmpty(fornecedor))
		// + " AND f.nome like '%%' "
		// + "AND (a.descricao like '%%' OR a.conteudo like '%%')";

	}

	@SuppressWarnings("unchecked")
	public List<Produto> listarPorData(UUID perfil, DateTime ultimaSincronizacao) throws SQLException {
		if (ultimaSincronizacao != null)
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).and().gt("ultimaAlteracao", ultimaSincronizacao).prepare());
		else
			return List(newQuery().where().eq("perfil_id", perfil).and().eq("isAtivo", true).prepare());
	}

	@SuppressWarnings("unchecked")
	public Produto getCodigoInterno(UUID perfil, String produtoId) throws SQLException {
		return Get(newQuery().where().eq("perfil_id", perfil).and().eq("codigoInterno", produtoId).prepare());
	}

	@SuppressWarnings("unchecked")
	public Produto getCodigoExterno(UUID perfil, String produtoId) throws SQLException {
		return Get(newQuery().where().eq("perfil_id", perfil).and().eq("codigoExterno", produtoId).prepare());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deletePorCodigoExterno(UUID perfil, String id) throws SQLException {
		Produto p = getCodigoExterno(perfil, id);
		if (p != null) {
			p.setAtivo(false);

			Dao pedidoProduto = getDao(PedidoProduto.class);
			List<PedidoProduto> items = pedidoProduto.queryBuilder().where().eq("produto_id", p.getCode()).query();

			if (items.size() == 0)
				delete(p);
			else
				save(p);
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<Produto> listarFotosUpload(UUID perfil) throws SQLException {
		return List(newQuery().where().eq("perfil_id", perfil).and().isNotNull("codigoInterno").and().eq("isAtivo", true).and().eq("isImageSincronizada", false)
				.and().isNotNull("image").prepare());

	}

}

package br.com.tagplus.pedidos.repository;

import java.sql.SQLException;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Cidade;

public class CidadeRepository extends RepositoryBase<Cidade> {

	public CidadeRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.List<Cidade> listAll() throws SQLException {
		return List(newQuery().orderBy("nome", true).prepare());
	}
	
	@SuppressWarnings("unchecked")
	public Cidade getCidadePorCodigo(Integer codigoCidade) throws SQLException {
		return Get(newQuery().where().eq("codigoCidade", codigoCidade).prepare());
	}

}

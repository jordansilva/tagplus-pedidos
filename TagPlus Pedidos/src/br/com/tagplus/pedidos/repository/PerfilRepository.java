package br.com.tagplus.pedidos.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Configuracao;
import br.com.tagplus.pedidos.business.domain.Perfil;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;

public class PerfilRepository extends RepositoryBase<Perfil> {

	public PerfilRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(Perfil item) throws SQLException {

		List<Perfil> lista = List(newQuery().where().like("nome", item.getNome().trim()).prepare());
		if (item.getCode() == null && lista != null && lista.size() > 0)
			throw new SQLException("Already exists Profile!", "Already exists Profile!", 999);

		if (item.getConfiguracao().getCode() != null) {
			Dao<Configuracao, UUID> daoConfiguracao = getDao(Configuracao.class);
			daoConfiguracao.createOrUpdate(item.getConfiguracao());
		}

		super.save(item);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> listarNomeDosPerfis() throws SQLException {
		String sql = "SELECT DISTINCT LTRIM(RTRIM(nome)) FROM perfil";
		GenericRawResults<String> result = getDao().queryRaw(sql, new RawRowMapper<String>() {

			@Override
			public String mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				return resultColumns[0];
			}
		});
		return (ArrayList<String>) result.getResults();
	}

	@SuppressWarnings("unchecked")
	public Perfil logarPerfil(String nome) throws SQLException {
		Perfil perfil = Get(newQuery().where().eq("nome", nome).prepare());
		if (perfil == null)
			throw new SQLException();

		return perfil;
	}

}

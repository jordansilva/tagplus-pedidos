package br.com.tagplus.pedidos;

import java.util.UUID;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.WindowManager;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.FornecedorService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.SectionsPagerAdapter;
import br.com.tagplus.pedidos.ui.listener.FornecedorListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

@EActivity(R.layout.activity_viewpager)
public class FornecedorActivity extends BaseActivity implements ActionBar.TabListener, FornecedorListener {

	@ViewById(R.id.pager)
	ViewPager mViewPager;

	@Bean
	FornecedorService service;

	SectionsPagerAdapter mSectionsPagerAdapter;

	@Extra
	UUID codigoFornecedor;

	@Extra
	Fornecedor mFornecedor;

	@Override
	protected void load() {
		super.load();

		if (codigoFornecedor == null && mFornecedor == null) {
			mFornecedor = new Fornecedor();
			configureUi();
		} else if (codigoFornecedor != null && mFornecedor == null) {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			service.getFornecedor(codigoFornecedor, new BusinessListener() {

				@Override
				public void onPreInit() {
					UIHelper.showLoading(FornecedorActivity.this);
				}

				@Override
				public void onPostExecute(RequestResult result) {
					UIHelper.hideLoading(FornecedorActivity.this);
					if (result.isError()) {
						finish();
						showToast(R.string.erro_generico);
					} else {
						mFornecedor = (Fornecedor) result.getData();
						configureUi();
					}
				}
			});
		} else {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			configureUi();
		}
	}

	protected void configureUi() {
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		if (mFornecedor != null && mFornecedor.getNome() != null && !mFornecedor.getNome().isEmpty())
			actionBar.setTitle(mFornecedor.getNome());

		mSectionsPagerAdapter = new SectionsPagerAdapter(this, getFragmentManager());
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_fornecedor), FornecedorDetalheFragment_.class);
		mSectionsPagerAdapter.addFragment(getString(R.string.tab_detalhes), FornecedorAtributoFragment_.class);

		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			Tab tab = actionBar.newTab();
			tab.setText(mSectionsPagerAdapter.getPageTitle(i));
			tab.setTabListener(this);
			actionBar.addTab(tab);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cliente, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		UpdateFragmentListener page = (UpdateFragmentListener) getFragmentManager().findFragmentByTag(
				"android:switcher:" + R.id.pager + ":" + tab.getPosition());
		page.update();
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public Fornecedor getFornecedor() {
		return mFornecedor;
	}

	@Override
	public void updateDetalhes(Fornecedor fornecedor) {
		if (fornecedor != null)
			mFornecedor = fornecedor;
		else
			mFornecedor = new Fornecedor();

	}

	@Override
	@UiThread
	public void salvarFornecedor() {
		if (mFornecedor.validar()) {
			service.salvar(getUsuarioLogado().getCode(), mFornecedor, new BusinessListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					salvarFornecedor(result);
				}
			});
		} else {
			mViewPager.setCurrentItem(0, true);
			UpdateFragmentListener page = (UpdateFragmentListener) getFragmentManager().findFragmentByTag(
					"android:switcher:" + R.id.pager + ":0");
			page.validar();
		}

	}

	@UiThread
	protected void salvarFornecedor(RequestResult result) {
		if (!result.isError()) {
			if (mFornecedor.getCode() == null)
				showToast(R.string.fornecedor_criado);
			else
				showToast(R.string.fornecedor_salvo);

			mFornecedor = (Fornecedor) result.getData();
			
			Intent intent = new Intent();
			intent.putExtra("data", mFornecedor);
			setResult(RESULT_OK, intent);
			finish();
		} else {
			if (result.getCode() == -1)
				showToast("Oops! " + result.getMessage());
		}
	}
}

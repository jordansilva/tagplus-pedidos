package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.jordansilva.android.prettyui.ExtendedButton;

@EActivity(R.layout.activity_conta_tagplus)
public class ContaTagPlusActivity extends BaseActivity {

	@ViewById
	ExtendedButton btnVincular;

	@ViewById
	ExtendedButton btnCriar;

	@Override
	protected void load() {
		super.load();

		if (getUsuarioLogado() != null && !StringHelper.isNullOrEmpty(getUsuarioLogado().getSistema()))
			btnCriar.setVisibility(View.GONE);
	}

	@Click(R.id.btnVincular)
	protected void onClickVincularConta(View v) {
		Intent intent = new Intent(getApplicationContext(), VincularContaActivity_.class);
		startActivityForResult(intent, 100);
	}

	@Click(R.id.btnCriar)
	protected void onClickCriarConta(View v) {
		Intent intent = new Intent(getApplicationContext(), VincularContaNovaActivity_.class);
		startActivityForResult(intent, 100);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 100 && resultCode == RESULT_OK) {
			setResult(RESULT_OK);
			finish();
			Toast.makeText(getApplicationContext(), R.string.conta_vinculada, Toast.LENGTH_LONG).show();
		}

		super.onActivityResult(requestCode, resultCode, data);
	}
}

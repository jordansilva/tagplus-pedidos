package br.com.tagplus.pedidos;

import org.androidannotations.annotations.EApplication;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

@EApplication
public class TagPlusApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		init();
	}

	protected void init() {
		 Crashlytics.start(this);
	}

}

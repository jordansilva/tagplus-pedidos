package br.com.tagplus.pedidos;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.FornecedorService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.AutoCompleteFloatLabel;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment.YesNoListener;
import br.com.tagplus.pedidos.ui.StretchedListView;
import br.com.tagplus.pedidos.ui.StretchedListView.StretchedListViewListener;
import br.com.tagplus.pedidos.ui.adapter.AdapterProdutoFornecedor;
import br.com.tagplus.pedidos.ui.adapter.AdapterProdutoFornecedor.AdapterFornecedorListener;
import br.com.tagplus.pedidos.ui.listener.ProdutoListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

@EFragment(R.layout.fragment_produto_fornecedores)
public class ProdutoFornecedoresFragment extends BaseFragment implements AdapterFornecedorListener, UpdateFragmentListener, StretchedListViewListener {

	@ViewById
	AutoCompleteFloatLabel txtFornecedor;

	@ViewById
	Button btnAdicionar;

	@ViewById
	TextView txtFornecedores;

	@ViewById
	StretchedListView listView;

	@Bean
	FornecedorService service;

	Produto mProduto;
	ProdutoListener listener;
	AdapterProdutoFornecedor adapter;
	Fornecedor fornecedorSelected;
	ArrayList<Fornecedor> mFornecedores;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

	}

	@Override
	protected void load() {
		super.load();

		if (mProduto == null)
			mProduto = listener.getProduto();
		mFornecedores = new ArrayList<Fornecedor>();

		listarFornecedores();

		if (mProduto != null)
			carregarCampos();
	}

	@UiThread
	protected void carregarCampos() {
		mFornecedores = (ArrayList<Fornecedor>) mProduto.getFornecedores();
		// fornecedores = new ArrayList<Fornecedor>();
		adapter = new AdapterProdutoFornecedor(getActivity(), mFornecedores, this);
		listView.setAdapter(adapter);
		listView.setListener(this);

		if (mFornecedores.size() > 0)
			txtFornecedores.setVisibility(View.VISIBLE);
	}

	@Background
	protected void listarFornecedores() {
		service.listarFornecedores(getUsuarioLogado().getCode(), new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(RequestResult result) {
				carregarFornecedoresAutoComplete(result);

			}
		});
	}

	@SuppressWarnings("unchecked")
	@UiThread
	protected void carregarFornecedoresAutoComplete(RequestResult result) {
		if (!result.isError()) {
			List<Fornecedor> list = (List<Fornecedor>) result.getData();
			ArrayAdapter<Fornecedor> adapterFornecedores = new ArrayAdapter<Fornecedor>(getActivity(),
					android.R.layout.simple_dropdown_item_1line, list);
			txtFornecedor.getAutoCompleteTextView().setAdapter(adapterFornecedores);
			txtFornecedor.getAutoCompleteTextView().setThreshold(2);
			txtFornecedor.getAutoCompleteTextView().setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Object obj = parent.getItemAtPosition(position);
					if (obj != null)
						fornecedorSelected = (Fornecedor) obj;
				}
			});
		}
	}

	@Click(R.id.btnAdicionar)
	protected void adicionarFornecedor(View v) {
		if (validar()) {

			hideSoftKeyboard(v);

			String nomeFornecedor = txtFornecedor.getTextValue();
			if (fornecedorSelected == null || !nomeFornecedor.equalsIgnoreCase(fornecedorSelected.toString())) {
				fornecedorSelected = new Fornecedor();
				fornecedorSelected.setNome(nomeFornecedor);
			}

			if (!checkFornecedor(fornecedorSelected))
				mFornecedores.add(fornecedorSelected);

			txtFornecedor.getAutoCompleteTextView().getEditableText().clear();
			txtFornecedor.getAutoCompleteTextView().dismissDropDown();

			if (adapter == null)
				carregarCampos();
			else {
				adapter.notifyDataSetChanged();
				txtFornecedores.setVisibility(View.VISIBLE);
			}
		}
	}

	protected boolean checkFornecedor(Fornecedor fornecedor) {
		for (Fornecedor f : mFornecedores) {
			if (fornecedor.getCode() != null && fornecedor.getCode().equals(f.getCode()))
				return true;
		}

		return false;
	}

	@Override
	public boolean validar() {
		boolean isValid = true;

		if (StringHelper.isNullOrEmpty(txtFornecedor.getTextValue())) {
			isValid = false;
			txtFornecedor.requestFocus();
			txtFornecedor.getAutoCompleteTextView().setError(getString(R.string.campo_obrigatorio));
		}

		return isValid;
	}

	@Override
	public void onDelete(int position, final Fornecedor fornecedor) {

		DialogDeleteFragment dialog = new DialogDeleteFragment();
		dialog.setTitleId(R.string.dialog_excluir_fornecedor_title);
		dialog.setMessage(getString(R.string.dialog_excluir_fornecedor));
		dialog.setListener(new YesNoListener() {

			@Override
			public void onYes() {

				adapter.remove(fornecedor);
				mFornecedores.remove(fornecedor);
				carregarCampos();
			}

			@Override
			public void onNo() {
				// TODO Auto-generated method stub

			}
		});
		dialog.show(getFragmentManager(), DialogDeleteFragment.TAG);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (ProdutoListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement ProdutoListener.");
		}
	}

	@Override
	public void onItemClick(ListAdapter adapter, int position) {
		Fornecedor fornecedor = (Fornecedor) adapter.getItem(position);
		fornecedorSelected = fornecedor;
		Intent intent = FornecedorActivity_.intent(getActivity()).mFornecedor(fornecedor).get();
		startActivityForResult(intent, 1);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1) {
				Fornecedor f = (Fornecedor) data.getSerializableExtra("data");
				int index = mFornecedores.indexOf(fornecedorSelected);
				mFornecedores.remove(index);
				mFornecedores.add(index, f);
				adapter.notifyDataSetChanged();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onItemLongClick(ListAdapter adapter, int position) {

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		update();
	}

	@Override
	public void update() {
		listener.updateFornecedores(adapter.getItems());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;

		int id = item.getItemId();
		if (id == R.id.action_save) {
			try {
				update();
				listener.salvarProduto();
				result = false;
			} catch (Exception ex) {
				result = true;
			}
		}

		return result;
	}

}

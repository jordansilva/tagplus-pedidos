package br.com.tagplus.pedidos;

import java.util.ArrayList;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ClienteService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterList;

import com.jordansilva.android.prettyui.empty.EmptyView;

@EFragment(R.layout.fragment_list)
public class ListaClientesFragment extends BaseFragment implements BusinessListener, OnItemClickListener, OnQueryTextListener,
		OnActionExpandListener {

	@ViewById(R.id.list)
	ListView listView;

	@ViewById
	EmptyView emptyView;

	@Bean
	ClienteService service;

	AdapterList<Cliente> mAdapter;
	Menu mMenu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();
		buscarDados();

		emptyView.setButtonText(R.string.action_adicionar_novo_cliente);
		emptyView.setListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarNovo();
			}
		});
	}

	@Background
	protected void buscarDados() {
		service.listarClientes(getUsuarioLogado().getCode(), this);
	}

	@Override
	@UiThread
	public void onPreInit() {
		if (isAdded())
			UIHelper.showLoading(getActivity());
	}

	@Override
	@UiThread
	public void onPostExecute(RequestResult result) {
		if (isAdded()) {
			UIHelper.hideLoading(getActivity());
			carregarLista(result);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.lista_clientes, menu);

		mMenu = menu;

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		MenuItem searchItem = menu.findItem(R.id.action_search);
		searchItem.setOnActionExpandListener(this);

		SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setQueryHint(getString(R.string.buscar_cliente));
		searchView.setOnQueryTextListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			adicionarNovo();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void adicionarNovo() {
		Intent intent = ClienteActivity_.intent(getActivity()).get();
		startActivityForResult(intent, 1);
	}

	@UiThread
	protected void carregarLista(RequestResult result) {
		if (!result.isError()) {

			@SuppressWarnings("unchecked")
			ArrayList<Cliente> list = (ArrayList<Cliente>) result.getData();
			if (isAdded()) {
				mAdapter = new AdapterList<Cliente>(getActivity(), R.layout.list_layout, Cliente.class, list);
				listView.setAdapter(mAdapter);
				listView.setOnItemClickListener(this);
				listView.setEmptyView(emptyView);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK)
			buscarDados();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object obj = parent.getItemAtPosition(position);
		if (obj != null) {
			Cliente c = (Cliente) obj;
			Intent intent = ClienteActivity_.intent(getActivity()).codigoCliente(c.getCode()).get();
			startActivityForResult(intent, 1);
		}
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(true);
			return true;
		}

		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(false);
			return true;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		search(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		search(query);
		return false;
	}

	private void search(String query) {
		if (mAdapter != null)
			mAdapter.getFilter().filter(query);
	}

}

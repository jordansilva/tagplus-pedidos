package br.com.tagplus.pedidos.services.webservice;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class HttpBasicAuthenticatorInterceptor implements ClientHttpRequestInterceptor {

	private String authKey;

	public HttpBasicAuthenticatorInterceptor(String authKey) {
		this.authKey = authKey;
	}

	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		HttpHeaders headers = request.getHeaders();
		headers.set("AUTH_KEY", authKey);
		headers.set("Connection", "Close");
		return execution.execute(request, body);
	}
}
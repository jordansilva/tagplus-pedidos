package br.com.tagplus.pedidos.services.webservice;

import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import br.com.tagplus.pedidos.business.domain.TagPlusCliente;
import br.com.tagplus.pedidos.business.domain.TagPlusFornecedor;
import br.com.tagplus.pedidos.business.domain.TagPlusPedido;
import br.com.tagplus.pedidos.business.domain.TagPlusProduto;
import br.com.tagplus.pedidos.helper.Constants;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Rest(rootUrl = Constants.WebService2.URL, converters = { ByteArrayHttpMessageConverter.class, FormHttpMessageConverter.class,
		StringHttpMessageConverter.class, CustomHttpMessageConverter.class })
@SuppressWarnings("rawtypes")
public interface WSSincronizacao extends RestClientErrorHandling {

	// FORNECEDOR
	@Post("{sistema}/rest/fornecedores_rest/get_fornecedores")
	TagPlusFornecedor[] getFornecedores(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/fornecedores_rest/save_fornecedores")
	JsonArray saveFornecedores(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/fornecedores_rest/get_fornecedores_apagados")
	JsonArray getFornecedoresDeletados(String sistema, MultiValueMap data);

	// FIM FORNECEDOR

	// CLIENTE
	@Post("{sistema}/rest/clientes_rest/get_clientes")
	TagPlusCliente[] getClientes(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/clientes_rest/save_clientes")
	JsonArray saveClientes(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/clientes_rest/get_clientes_apagados")
	JsonArray getClientesDeletados(String sistema, MultiValueMap data);

	// FIM CLIENTE

	// PRODUTO
	@Post("{sistema}/rest/produtos_rest/get_produtos")
	TagPlusProduto[] getProdutos(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/produtos_rest/save_produtos")
	JsonArray saveProdutos(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/produtos_rest/upload_foto")
	String uploadFotos(String sistema, JsonObject data);

	@Post("{sistema}/rest/produtos_rest/get_produtos_apagados")
	JsonArray getProdutosDeletados(String sistema, MultiValueMap data);

	// FIM PRODUTO

	// Pedidos
	@Post("{sistema}/rest/pedidos_rest/get_pedidos")
	TagPlusPedido[] getPedidos(String sistema, MultiValueMap data);

	@Post("{sistema}/rest/pedidos_rest/save_pedidos")
	JsonArray savePedidos(String sistema, MultiValueMap data);
	
	@Post("{sistema}/rest/pedidos_rest/get_pedidos_apagados")
	JsonArray getPedidosDeletados(String sistema, MultiValueMap data);
	

	public RestTemplate getRestTemplate();

	public void setRestTemplate(RestTemplate restTemplate);
}

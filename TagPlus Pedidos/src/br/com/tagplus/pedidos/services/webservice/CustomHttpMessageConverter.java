package br.com.tagplus.pedidos.services.webservice;

import org.springframework.http.converter.json.GsonHttpMessageConverter;

import br.com.tagplus.pedidos.helper.GsonHelper;

import com.google.gson.Gson;

public class CustomHttpMessageConverter extends GsonHttpMessageConverter {

	protected static Gson buildGson() {
		return GsonHelper.getInstance();
	}

	public CustomHttpMessageConverter() {
		super(buildGson());
	}

}

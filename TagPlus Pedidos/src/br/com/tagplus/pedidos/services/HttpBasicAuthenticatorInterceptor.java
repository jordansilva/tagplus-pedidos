package br.com.tagplus.pedidos.services;

import retrofit.RequestInterceptor;

public class HttpBasicAuthenticatorInterceptor implements RequestInterceptor {

	private String authKey;

	public HttpBasicAuthenticatorInterceptor(String authKey) {
		this.authKey = authKey;
	}

	@Override
	public void intercept(RequestFacade requestFacade) {
		requestFacade.addHeader("AUTH_KEY", authKey);
	}

}
package br.com.tagplus.pedidos.services;

import org.androidannotations.annotations.EBean;
import org.androidannotations.api.rest.RestErrorHandler;
import org.apache.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import br.com.tagplus.pedidos.exception.InternalServerErrorException;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.google.gson.JsonObject;

@EBean
public class CustomErrorHandler implements RestErrorHandler {

	@Override
	public void onRestClientExceptionThrown(RestClientException error) {
		if (error instanceof HttpStatusCodeException) {
			HttpStatusCodeException exception = (HttpStatusCodeException) error;
			String body = null;
			switch (exception.getStatusCode().value()) {
			case HttpStatus.SC_NOT_FOUND:
				throw new InternalServerErrorException(exception.getStatusCode().value(), exception.getMessage(), exception.getResponseBodyAsString());
			case HttpStatus.SC_INTERNAL_SERVER_ERROR:
				body = exception.getResponseBodyAsString();
				if (!StringHelper.isNullOrEmpty(body)) {
					JsonObject json = GsonHelper.parseToJsonObject(body);
					int codigoErro = json.get("CodigoErro").getAsInt();
					String tipoErro = json.get("TipoErro").getAsString();
					String mensagemSistema = json.get("MensagemSistema").getAsString();
					String mensagemUsuario = json.get("MensagemUsuario").getAsString();

					throw new InternalServerErrorException(codigoErro, tipoErro, mensagemSistema, mensagemUsuario, body);
				} else
					throw exception;
			default:
				throw exception;
			}
		} else if (error.getMessage().contains("Connection timed out") || error.getMessage().contains("Network is unreachable"))
			throw new InternalServerErrorException(110, "Conexão", null);
		else
			throw error;
	}
}

package br.com.tagplus.pedidos.services;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.RestAdapter.Builder;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import android.content.Context;
import br.com.tagplus.pedidos.helper.Constants;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;

import com.squareup.okhttp.OkHttpClient;

public class ServiceProvider<T> {

	private Context mContext;
	private Class<T> mClazz;
	private Converter mConverter;

	public ServiceProvider(Context context, Class<T> clazz) {
		mContext = context;
		mClazz = clazz;
		mConverter = new GsonConverter(GsonHelper.getInstance());
	}

	public ServiceProvider(Context context, Class<T> clazz, Converter converter) {
		mContext = context;
		mClazz = clazz;
		mConverter = converter;
	}

	public T get(String sistema) {
		return get(sistema, null);
	}

	public T get(String sistema, String authKey) {
		Builder builder = new RestAdapter.Builder();
		builder = builder.setEndpoint(String.format(Constants.WebService.URL, sistema));

		if (!StringHelper.isNullOrEmpty(authKey))
			builder = builder.setRequestInterceptor(new HttpBasicAuthenticatorInterceptor(authKey));

		builder = builder.setConverter(mConverter);
		// builder = builder.setErrorHandler(new CustomErrorHandler());

		OkHttpClient httpClient = new OkHttpClient();
		httpClient.setReadTimeout(45, TimeUnit.SECONDS);
		httpClient.setWriteTimeout(30, TimeUnit.SECONDS);

		builder = builder.setClient(new ConnectivityAwareUrlClient(new OkClient(httpClient), mContext));
		RestAdapter adapter = builder.build();
		adapter.setLogLevel(LogLevel.FULL);
		return adapter.create(mClazz);
	}

}

package br.com.tagplus.pedidos.services;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import br.com.tagplus.pedidos.business.domain.TagPlusCliente;
import br.com.tagplus.pedidos.business.domain.TagPlusFornecedor;
import br.com.tagplus.pedidos.business.domain.TagPlusPedido;
import br.com.tagplus.pedidos.business.domain.TagPlusProduto;
import br.com.tagplus.pedidos.helper.Constants;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public interface RestSincronizacao {

	// Cliente
	@FormUrlEncoded
	@POST(Constants.WebService.Clientes.API + Constants.WebService.Clientes.GET)
	TagPlusCliente[] getClientes(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Clientes.API + Constants.WebService.Clientes.DELETED)
	JsonArray getClientesDeleted(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Clientes.API + Constants.WebService.Clientes.SAVE)
	JsonArray saveClientes(@Field("clientes") String data);

	// Produto
	@FormUrlEncoded
	@POST(Constants.WebService.Produtos.API + Constants.WebService.Produtos.GET)
	TagPlusProduto[] getProdutos(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Produtos.API + Constants.WebService.Produtos.DELETED)
	JsonArray getProdutosDeleted(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Produtos.API + Constants.WebService.Produtos.SAVE)
	JsonArray saveProdutos(@Field("produtos") String data);

	@POST(Constants.WebService.Produtos.API + Constants.WebService.Produtos.UPLOAD)
	String uploadFotos(@Body JsonObject item);

	// Fornecedor
	@FormUrlEncoded
	@POST(Constants.WebService.Fornecedores.API + Constants.WebService.Fornecedores.GET)
	TagPlusFornecedor[] getFornecedores(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Fornecedores.API + Constants.WebService.Fornecedores.DELETED)
	JsonArray getFornecedoresDeleted(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Fornecedores.API + Constants.WebService.Fornecedores.SAVE)
	JsonArray saveFornecedores(@Field("fornecedores") String data);

	// Pedidos
	@FormUrlEncoded
	@POST(Constants.WebService.Pedidos.API + Constants.WebService.Pedidos.GET)
	TagPlusPedido[] getPedidos(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Pedidos.API + Constants.WebService.Pedidos.DELETED)
	JsonArray getPedidosDeleted(@Field("data_alteracao") String data);

	@FormUrlEncoded
	@POST(Constants.WebService.Pedidos.API + Constants.WebService.Pedidos.SAVE)
	JsonArray savePedidos(@Field("pedidos") String data);

}

package br.com.tagplus.pedidos.services;

import br.com.tagplus.pedidos.exception.InternalServerErrorException;

public abstract class IMessage {

	private boolean isRenewToken;
	private String token;

	public IMessage() {
	}

	public IMessage(String token) {
		this.token = token;
	}

	public boolean isRenewToken() {
		return isRenewToken;
	}

	public void setRenewToken(boolean isRenewToken) {
		this.isRenewToken = isRenewToken;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	abstract Object run() throws InternalServerErrorException;

}
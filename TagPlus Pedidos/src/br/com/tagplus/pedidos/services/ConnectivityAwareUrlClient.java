package br.com.tagplus.pedidos.services;

import java.io.IOException;
import java.net.ConnectException;

import retrofit.client.Client;
import retrofit.client.Request;
import retrofit.client.Response;
import android.content.Context;
import br.com.tagplus.pedidos.helper.NetworkUtil;

import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;

public class ConnectivityAwareUrlClient implements Client {

	private Context mContext;
	private Client mWrappedClient;
	private Logger log = LoggerFactory.getLogger(ConnectivityAwareUrlClient.class);

	public ConnectivityAwareUrlClient(Client wrappedClient, Context context) {
		mWrappedClient = wrappedClient;
		mContext = context;
	}

	@Override
	public Response execute(Request request) throws IOException {
		int status = NetworkUtil.getConnectivityStatus(mContext);
		if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
			log.debug("No connectivity %s ", request);
			throw new ConnectException("No connectivity");
		}

		return mWrappedClient.execute(request);
	}
}

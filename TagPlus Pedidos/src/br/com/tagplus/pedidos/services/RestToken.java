package br.com.tagplus.pedidos.services;

import retrofit.http.Body;
import retrofit.http.POST;
import br.com.tagplus.pedidos.helper.Constants;

import com.google.gson.JsonObject;

public interface RestToken {

	@POST(Constants.WebService.Token.API + Constants.WebService.Token.BUILD)
	JsonObject vincularConta(@Body JsonObject data);

}

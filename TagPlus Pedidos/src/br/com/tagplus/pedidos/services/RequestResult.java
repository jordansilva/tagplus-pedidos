/**
 * 
 */
package br.com.tagplus.pedidos.services;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

/**
 * @filename RequestResult.java
 * @author Jordan Silva <@jordansilva>
 * @date Mar 31, 2014
 */
@SuppressWarnings("serial")
public class RequestResult implements Serializable
{
	@Expose
	private int code;
	
	@Expose
	private String message;
	
	@Expose
	private boolean isError;
	
	@Expose
	private Object data;

	public RequestResult()
	{		
	}
	
	public RequestResult(int code, String message, boolean isError)
	{
		this.code = code;
		this.message = message;
		this.isError = isError;
	}

	public RequestResult(int code, String message, boolean isError, Object data)
	{
		this.code = code;
		this.message = message;
		this.isError = isError;
		this.data = data;
	}

	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(int code)
	{
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the isError
	 */
	public boolean isError()
	{
		return isError;
	}

	/**
	 * @param isError
	 *            the isError to set
	 */
	public void setError(boolean isError)
	{
		this.isError = isError;
	}

	/**
	 * @return the object
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @param data
	 *            the object to set
	 */
	public void setData(Object data)
	{
		this.data = data;
	}
}

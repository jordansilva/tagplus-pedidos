package br.com.tagplus.pedidos.exception;

import org.springframework.web.client.RestClientException;

@SuppressWarnings("serial")
public class InternalServerErrorException extends RestClientException {

	private String body;
	private int codigoErro;
	private String tipoErro;
	private String mensagemSistema;
	private String mensagemUsuario;

	public InternalServerErrorException(int codigoErro, String message, String body) {
		super(body);
		this.codigoErro = codigoErro;
		this.mensagemUsuario = message;
		this.body = body;
	}

	public InternalServerErrorException(int codigoErro, String tipoErro, String mensagemSistema, String mensagemUsuario, String body) {
		super(body);
		this.codigoErro = codigoErro;
		this.tipoErro = tipoErro;
		this.mensagemSistema = mensagemSistema;
		this.mensagemUsuario = mensagemUsuario;
		this.body = body;
	}

	public int getCodigoErro() {
		return codigoErro;
	}

	public String getTipoErro() {
		return tipoErro;
	}

	public String getMensagemSistema() {
		return mensagemSistema;
	}

	public String getMensagemUsuario() {
		return mensagemUsuario;
	}

	public String getBody() {
		return body;
	}

}

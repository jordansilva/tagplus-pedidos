package br.com.tagplus.pedidos.exception;


@SuppressWarnings("serial")
public class RequiredFieldException extends ExceptionBase {

	private final static int CODE = 999;

	public RequiredFieldException() {
		super(CODE, "Required Field!");
	}

}

/**
 * 
 */
package br.com.tagplus.pedidos.exception;

/**
 * @author Jordan Silva ( @jordansilva )
 * @since 14/12/2012 19:08:43
 */
@SuppressWarnings("serial")
public abstract class ExceptionBase extends Exception
{
	private int mCode;
	private String mMessage;
	private String mStringResourceName;

	public ExceptionBase(int code, String message)
	{
		super(message);
		mCode = code;
		mMessage = message;		
	}

	public ExceptionBase(int code, String message, String stringResourceName)
	{
		super(message);
		mCode = code;
		mMessage = message;
		mStringResourceName = stringResourceName;
	}

	public int getCode()
	{
		return mCode;
	}

	public String getStringResourceName()
	{
		return mStringResourceName;
	}

	public void setStringResourceName(String name)
	{
		mStringResourceName = name;
	}

	@Override
	public String toString()
	{
		return mMessage;
	}

}

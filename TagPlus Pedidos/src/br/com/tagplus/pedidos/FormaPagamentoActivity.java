package br.com.tagplus.pedidos;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.time.DateTime;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import br.com.tagplus.pedidos.business.domain.FormaPagamento;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.DateDialogFragment;
import br.com.tagplus.pedidos.ui.MoneyTextWatcher;

import com.crashlytics.android.Crashlytics;
import com.iangclifton.android.floatlabel.FloatLabel;

@EActivity(R.layout.activity_forma_pagamento)
public class FormaPagamentoActivity extends BaseActivity implements OnDateSetListener {

	@ViewById
	FloatLabel txtData;

	@ViewById
	FloatLabel txtDescricao;

	@ViewById
	FloatLabel txtValor;

	@ViewById
	Button btnAdicionar;

	@Extra
	BigDecimal valorInicial;

	Money valor;
	Calendar calendar;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

	@Override
	protected void load() {
		super.load();

		calendar = Calendar.getInstance(Locale.getDefault());

		configurarUi();

		try {
			NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
			txtValor.setText(moneyFormat.format(valorInicial));
		} catch (Exception e) {
			Crashlytics.logException(e);
		}
	}

	protected void configurarUi() {

		txtData.setText(simpleDateFormat.format(calendar.getTime()));

		TextWatcher watcher = new MoneyTextWatcher(txtValor.getEditText());
		txtValor.getEditText().addTextChangedListener(watcher);
		txtValor.getEditText().addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

				try {
					if (!StringHelper.isNullOrEmpty(s.toString())) {
						NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
						Number money = moneyFormat.parse(s.toString());

						valor = (Money.of(CurrencyUnit.getInstance(Locale.getDefault()), money.doubleValue()));
					}
				} catch (ArithmeticException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
		});

		txtData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				DateDialogFragment dialog = DateDialogFragment.newInstance();

				Bundle args = new Bundle();
				args.putInt(DateDialogFragment.DAY, calendar.get(Calendar.DAY_OF_MONTH));
				args.putInt(DateDialogFragment.MONTH, calendar.get(Calendar.MONTH));
				args.putInt(DateDialogFragment.YEAR, calendar.get(Calendar.YEAR));
				dialog.setArguments(args);

				dialog.show(getFragmentManager(), DateDialogFragment.TAG);
			}
		});

	}

	protected boolean validarCampos() {

		boolean isValid = true;

		if (StringHelper.isNullOrEmpty(txtValor.getTextValue().toString()) || valor == null || valor.isZero()) {
			isValid = false;
			txtValor.setError(getString(R.string.campo_obrigatorio));
		}

		if (StringHelper.isNullOrEmpty(txtDescricao.getTextValue().toString())) {
			isValid = false;
			txtDescricao.setError(getString(R.string.campo_obrigatorio));
		}

		if (StringHelper.isNullOrEmpty(txtData.getTextValue())) {
			isValid = false;
			txtData.setError(getString(R.string.campo_obrigatorio));
		}

		return isValid;
	}

	@Click(R.id.btnAdicionar)
	protected void onClickAdicionarPagamento(View v) {
		if (validarCampos()) {

			FormaPagamento formaPagamento = new FormaPagamento();
			formaPagamento.setNome(txtDescricao.getTextValue());
			formaPagamento.setValor(valor.getAmount());
			formaPagamento.setData(new DateTime(calendar.getTimeInMillis()));

			Intent intent = new Intent();
			intent.putExtra("data", formaPagamento);
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		calendar.set(year, monthOfYear, dayOfMonth);
		txtData.setText(simpleDateFormat.format(calendar.getTime()));
	}
}

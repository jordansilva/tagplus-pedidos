package br.com.tagplus.pedidos.helper;

import java.lang.reflect.Type;
import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.ISODateTimeFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonHelper {

	private static GsonBuilder gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
	private volatile static Gson instance;

	public static Gson getInstance() {
		if (instance == null) {
			synchronized (GsonHelper.class) {
				if (instance == null) {

					gson = gson.excludeFieldsWithoutExposeAnnotation();
					gson = gson.registerTypeHierarchyAdapter(Collection.class, new CollectionAdapter());
					gson = gson.registerTypeAdapter(DateTime.class, serDate);
					gson = gson.registerTypeAdapter(DateTime.class, deserDate);
					gson = gson.registerTypeAdapter(LocalDateTime.class, serLocalDate);
					gson = gson.registerTypeAdapter(LocalDateTime.class, deserLocalDate);
					gson = gson.registerTypeAdapter(Float.class, serFloat);
					gson = gson.registerTypeAdapter(Float.class, desFloat);

					return gson.create();
				}
			}
		}

		return instance;
	}

	public static String parseToJson(Object src) {
		return getInstance().toJson(src);
	}

	public static JsonObject parseToJsonObject(Object json) {
		return new JsonParser().parse(parseToJson(json)).getAsJsonObject();
	}

	public static JsonObject parseToJsonObject(String json) {
		return new JsonParser().parse(json).getAsJsonObject();
	}

	public static JsonArray parseToJsonArray(String json) {
		return new JsonParser().parse(json).getAsJsonArray();
	}

	public static <T> T fromToJson(String src, Class<T> clazz) {
		return getInstance().fromJson(src, clazz);
	}

	public static <T> T fromToJson(String src, Type typeOfT) {
		return getInstance().fromJson(src, typeOfT);
	}

	public static String serializeObject(Object obj) {
		return getInstance().toJson(obj);
	}

	static JsonSerializer<DateTime> serDate = new JsonSerializer<DateTime>() {
		@Override
		public JsonElement serialize(DateTime src, Type typeOfSrc, JsonSerializationContext context) {
			return src == null ? null : new JsonPrimitive(src.toDateTimeISO().withZone(DateTimeZone.UTC).toString());
		}
	};

	static JsonDeserializer<DateTime> deserDate = new JsonDeserializer<DateTime>() {
		@Override
		public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			try {
				String pattern = "yyyy-MM-dd'T'HH:mm:ss";
				if (!json.getAsString().contains("'T'"))
					pattern = "yyyy-MM-dd HH:mm:ss";

				return DateTimeFormat.forPattern(pattern).parseDateTime(json.getAsString());
			} catch (Exception e) {
				return null;
			}
		}
	};

	static JsonSerializer<LocalDateTime> serLocalDate = new JsonSerializer<LocalDateTime>() {
		@Override
		public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
			return src == null ? null : new JsonPrimitive(src.toDateTime().toDateTimeISO().withZone(DateTimeZone.UTC).toString());
		}
	};

	static JsonDeserializer<LocalDateTime> deserLocalDate = new JsonDeserializer<LocalDateTime>() {
		@Override
		public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return ISODateTimeFormat.dateTimeParser().withZoneUTC().parseLocalDateTime(json.getAsString());
		}
	};

	static JsonSerializer<Float> serFloat = new JsonSerializer<Float>() {
		public JsonElement serialize(Float src, Type typeOfSrc, JsonSerializationContext context) {
			return src == null ? null : new JsonPrimitive(Float.valueOf(src * 100).intValue());
		}
	};

	static JsonDeserializer<Float> desFloat = new JsonDeserializer<Float>() {

		public Float deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : Float.valueOf(json.getAsFloat() / 100);
		}
	};

}

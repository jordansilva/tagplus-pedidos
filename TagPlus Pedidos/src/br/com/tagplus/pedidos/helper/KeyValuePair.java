package br.com.tagplus.pedidos.helper;

public class KeyValuePair {

	public Object key;
	public Object value;

	public KeyValuePair(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return value.toString();
	}

}

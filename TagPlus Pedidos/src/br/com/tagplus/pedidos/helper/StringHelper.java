package br.com.tagplus.pedidos.helper;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.InputFilter;
import android.util.Log;
import android.widget.EditText;

public class StringHelper {
	private static SimpleDateFormat format = new SimpleDateFormat("dd MMM hh:mm'hs'", Locale.getDefault());

	public static boolean isNullOrEmpty(String text) {
		return text == null || text.trim().length() == 0;
	}

	public static boolean isNullOrWhiteSpace(String value) {
		return value == null || value.trim().length() == 0;
	}

	public static HashMap<String, String> mapperORMRawRowMapper(String[] columnNames, String[] resultColumns) {
		HashMap<String, String> result = new HashMap<String, String>();

		for (int i = 0; i < columnNames.length; i++)
			result.put(columnNames[i], resultColumns[i]);

		return result;

	}

	@SuppressLint("DefaultLocale")
	public static String capitalize(String source) {
		try {
			String[] tokens = source.toLowerCase(Locale.getDefault()).split("\\s");
			String text = "";

			for (int i = 0; i < tokens.length; i++) {
				char capLetter = Character.toUpperCase(tokens[i].charAt(0));
				text += " " + capLetter + tokens[i].substring(1, tokens[i].length());
			}
			return text;
		} catch (Exception ex) {
		}

		return source;
	}

	public static String getStringResourceByName(Context context, String name) {
		int resId = context.getResources().getIdentifier(name, "string", context.getPackageName());
		return context.getString(resId);
	}

	public static String formatDate(DateTime date) {
		return format.format(date.toDate());
	}

	public static String formatDate(DateTime date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
		return sdf.format(date.toDate());
	}

	public static String formatDayOfWeek(int day) {
		switch (day) {
		case DateTimeConstants.SUNDAY:
			return "DOM";
		case DateTimeConstants.MONDAY:
			return "SEG";
		case DateTimeConstants.TUESDAY:
			return "TER";
		case DateTimeConstants.WEDNESDAY:
			return "QUA";
		case DateTimeConstants.THURSDAY:
			return "QUI";
		case DateTimeConstants.FRIDAY:
			return "SEX";
		case DateTimeConstants.SATURDAY:
			return "SAB";
		default:
			return null;
		}
	}
	
	public static int getMaxLength(EditText editText) {
		int maxLength = -1;

		for (InputFilter filter : editText.getFilters()) {
			if (filter instanceof InputFilter.LengthFilter) {
				try {
					Field maxLengthField = filter.getClass().getDeclaredField("mMax");
					maxLengthField.setAccessible(true);

					if (maxLengthField.isAccessible()) {
						maxLength = maxLengthField.getInt(filter);
					}
				} catch (IllegalAccessException e) {
					Log.w(filter.getClass().getName(), e);
				} catch (IllegalArgumentException e) {
					Log.w(filter.getClass().getName(), e);
				} catch (NoSuchFieldException e) {
					Log.w(filter.getClass().getName(), e);
				} // if an Exception is thrown, Log it and return -1
			}
		}

		return maxLength;
	}

}

package br.com.tagplus.pedidos.helper;

public class Constants {

	public static class Session {
		public static final String SHAREDPREFERENCES_ID = "spTagPlusPedidos";
		public static final String PERFIL = "session_perfil";
	}

	public static class TipoPessoa {
		public static final Integer PESSOA_FISICA = 0;
		public static final Integer PESSOA_JURIDICA = 1;
	}

	public static class OrdenacaoPedido {
		public static final Integer POR_DATA = 0;
		public static final Integer POR_CLIENTE = 1;
	}

	public static class WebService {

		public static final String NEW_ACCOUNT = "http://www.tagplus.com.br/site/checkout-mobile-app/";		
		public static final String NEW_ACCOUNT_FINAL = "checkout-mobile-app-step-2";
		public static final String URL = "http://app.tagplus.com.br/%s/rest/";

		public static final class Token {
			public static final String API = "/token_rest";
			public static final String BUILD = "/build";
		}

		public static final class Fornecedores {
			public static final String API = "/fornecedores_rest";
			public static final String GET = "/get_fornecedores";
			public static final String SAVE = "/save_fornecedores";
			public static final String DELETED = "/get_fornecedores_apagados";
		}

		public static final class Produtos {
			public static final String API = "/produtos_rest";
			public static final String GET = "/get_produtos";
			public static final String SAVE = "/save_produtos";
			public static final String UPLOAD = "/upload_fotos";
			public static final String DELETED = "/get_produtos_apagados";
		}

		public static final class Clientes {
			public static final String API = "/clientes_rest";
			public static final String GET = "/get_clientes";
			public static final String SAVE = "/save_clientes";
			public static final String DELETED = "/get_clientes_apagados";
		}

		public static final class Pedidos {
			public static final String API = "/pedidos_rest";
			public static final String GET = "/get_pedidos";
			public static final String SAVE = "/save_pedidos";
			public static final String DELETED = "/get_pedidos_apagados";
		}

	}

	public static class WebService2 {

		public static final String URL = "http://app.tagplus.com.br/";

		public static final class Token {
			public static final String API = "/token_rest";
			public static final String BUILD = "/build";
		}

		public static final class Fornecedores {
			public static final String API = "/fornecedores_rest";
			public static final String GET = "/get_fornecedores";
			public static final String SAVE = "/save_fornecedores";
		}

		public static final class Produtos {
			public static final String API = "/produtos_rest";
			public static final String GET = "/get_produtos";
			public static final String SAVE = "/save_produtos";
			public static final String UPLOAD = "/upload_fotos";
		}

		public static final class Clientes {
			public static final String API = "/clientes_rest";
			public static final String GET = "/get_clientes";
			public static final String SAVE = "/save_clientes";
		}

		public static final class Pedidos {
			public static final String API = "/pedidos_rest";
			public static final String GET = "/get_pedidos";
			public static final String SAVE = "/save_pedidos";
		}

	}
}

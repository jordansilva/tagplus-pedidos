package br.com.tagplus.pedidos.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import android.content.Context;
import dalvik.system.DexFile;

public class ReflectionHelper {

	@SuppressWarnings("rawtypes")
	public static Class[] getClasses(Context mContext, String packageName) throws ClassNotFoundException, IOException {
		DexFile df = new DexFile(mContext.getPackageCodePath());
		Enumeration<String> entries = df.entries();
		ArrayList<Class> classes = new ArrayList<Class>();
		while (entries.hasMoreElements()) {
			String entry = (String) entries.nextElement();
			if (entry.contains(packageName))
				classes.add(Class.forName(entry.replace(".class", "")));
		}

		return classes.toArray(new Class[classes.size()]);
	}
	//
	// @SuppressWarnings("rawtypes")
	// private static List<Class> findClasses(File directory, String
	// packageName) throws ClassNotFoundException
	// {
	// List<Class> classes = new ArrayList<Class>();
	// if (!directory.exists())
	// return classes;
	//
	// File[] files = directory.listFiles();
	// for (File file : files)
	// {
	// if (file.isDirectory())
	// {
	// assert !file.getName().contains(".");
	// classes.addAll(findClasses(file, packageName + "." + file.getName()));
	// }
	// else if (file.getName().endsWith(".class"))
	// classes.add(Class.forName(packageName + "." + file.getName().substring(0,
	// file.getName().length() - 6)));
	// }
	// return classes;
	// }
}

package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.jordansilva.android.prettyui.ExtendedButton;
import com.jordansilva.android.prettyui.ExtendedTextView;

@EFragment(R.layout.fragment_sincronizar)
public class SincronizarFragment extends BaseFragment {

	@ViewById(R.id.emptyview_no_items)
	ExtendedTextView text;

	@ViewById(R.id.emptyview_divisor)
	View divisor;

	@ViewById(R.id.emptyview_add_new_item)
	ExtendedButton button;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();
		configureUi();
	}

	protected void configureUi() {
		if (getUsuarioLogado().isVinculado()) {
			text.setText(R.string.detalhes_vazio2);
			button.setVisibility(View.GONE);
			divisor.setVisibility(View.GONE);
		}
	}

	@Click(R.id.emptyview_add_new_item)
	protected void onClickVincularConta() {
		Intent intent = new Intent(getActivity(), ContaTagPlusActivity_.class);
		startActivityForResult(intent, 999);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 999 && resultCode == Activity.RESULT_OK) {
			reconfigureMenu();
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}

}

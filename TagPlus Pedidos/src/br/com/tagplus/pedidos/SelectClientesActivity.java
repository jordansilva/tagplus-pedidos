package br.com.tagplus.pedidos;

import java.util.ArrayList;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ClienteService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterList;

import com.jordansilva.android.prettyui.empty.EmptyView;

@EActivity(R.layout.fragment_list)
public class SelectClientesActivity extends BaseActivity implements BusinessListener, OnItemClickListener, OnQueryTextListener,
		OnActionExpandListener {

	@ViewById(R.id.list)
	ListView listView;

	@ViewById
	EmptyView emptyView;

	@Bean
	ClienteService service;

	AdapterList<Cliente> mAdapter;
	Menu mMenu;
	MenuItem mSearchItem;
	SearchView mSearchView;
	String mQuery;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void load() {
		super.load();
		buscarDados();

		emptyView.setButtonText(R.string.action_adicionar_novo_cliente);
		emptyView.setListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarNovo();
			}
		});

	}

	@Background
	protected void buscarDados() {
		service.listarClientes(getUsuarioLogado().getCode(), this);
	}

	@Override
	@UiThread
	public void onPreInit() {
	}

	@Override
	public void onPostExecute(RequestResult result) {
		carregarLista(result);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lista_clientes, menu);

		mSearchItem = menu.findItem(R.id.action_search);
		mSearchItem.setOnActionExpandListener(this);

		mSearchView = (SearchView) mSearchItem.getActionView();
		mSearchView.setQueryHint(getString(R.string.buscar_cliente));
		mSearchView.setOnQueryTextListener(this);

		mMenu = menu;

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			adicionarNovo();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void adicionarNovo() {
		Intent intent = ClienteActivity_.intent(this).get();
		startActivityForResult(intent, 1);
	}

	@UiThread
	protected void carregarLista(RequestResult result) {
		if (!result.isError()) {

			@SuppressWarnings("unchecked")
			ArrayList<Cliente> list = (ArrayList<Cliente>) result.getData();
			if (!isFinishing()) {
				mAdapter = new AdapterList<Cliente>(this, R.layout.list_layout, Cliente.class, list);
				listView.setAdapter(mAdapter);
				listView.setOnItemClickListener(this);
				listView.setEmptyView(emptyView);
			}

			if (!StringHelper.isNullOrEmpty(mQuery)) {
				mSearchItem.expandActionView();
				mSearchView.setQuery(mQuery, true);
				mSearchView.clearFocus();
				onQueryTextSubmit(mQuery);
				mQuery = null;
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
			Cliente cliente = (Cliente) data.getSerializableExtra("data");
			buscarDados();

			mQuery = cliente.getNome();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object obj = parent.getItemAtPosition(position);
		if (obj != null) {
			Intent intent = new Intent();
			intent.putExtra("data", (Cliente) obj);
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(true);
			return true;
		}

		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(false);
			return true;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		search(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		search(query);
		return false;
	}

	private void search(String query) {
		if (mAdapter != null)
			mAdapter.getFilter().filter(query);
	}

}

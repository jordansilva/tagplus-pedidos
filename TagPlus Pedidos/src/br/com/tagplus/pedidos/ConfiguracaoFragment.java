package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Configuracao;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.helper.Constants;
import br.com.tagplus.pedidos.ui.SessionHelper;

import com.jordansilva.android.prettyui.ExtendedButton;

@EFragment(R.layout.fragment_configuracoes)
public class ConfiguracaoFragment extends BaseFragment implements BusinessListener {

	@Bean
	SessionHelper mSession;

	@Bean
	PerfilService service;

	@ViewById
	ExtendedButton btnVincularConta;

	@ViewById
	RadioGroup rgTipoPessoa;

	@ViewById
	RadioButton rbPessoaFisica;

	@ViewById
	RadioButton rbPessoaJuridica;

	@ViewById
	RadioGroup rgOrdenacao;

	@ViewById
	RadioButton rbPorCliente;

	@ViewById
	RadioButton rbPorData;

	Perfil mPerfil;
	Configuracao mConfiguracao;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();

		mPerfil = getUsuarioLogado();

		configurarUi();
		carregarDados();
	}

	protected void configurarUi() {

		int text = 0;
		if (mPerfil.isVinculado()) {
			text = R.string.vinculada_conta;
			btnVincularConta.setClickable(false);
		} else {
			text = R.string.vincular_conta;
			btnVincularConta.setClickable(true);
		}
		Spanned html = Html.fromHtml(getString(text));
		btnVincularConta.setText(html);

	}

	protected void carregarDados() {

		mConfiguracao = mSession.getConfiguracao();

		// Tipo Pessoa
		if (mConfiguracao.getTipoPessoa() == Constants.TipoPessoa.PESSOA_FISICA)
			rbPessoaFisica.setChecked(true);
		else
			rbPessoaJuridica.setChecked(true);

		// Tipo Ordenação
		if (mConfiguracao.getTipoOrdenacaoPedido() == Constants.OrdenacaoPedido.POR_CLIENTE)
			rbPorCliente.setChecked(true);
		else
			rbPorData.setChecked(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.configuracoes, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			salvarConfiguracoes();
		}
		return super.onOptionsItemSelected(item);
	}

	protected void salvarConfiguracoes() {

		if (rgTipoPessoa.getCheckedRadioButtonId() == R.id.rbPessoaFisica)
			mConfiguracao.setTipoPessoa(Constants.TipoPessoa.PESSOA_FISICA);
		else
			mConfiguracao.setTipoPessoa(Constants.TipoPessoa.PESSOA_JURIDICA);

		if (rgOrdenacao.getCheckedRadioButtonId() == R.id.rbPorCliente)
			mConfiguracao.setTipoOrdenacaoPedido(Constants.OrdenacaoPedido.POR_CLIENTE);
		else
			mConfiguracao.setTipoOrdenacaoPedido(Constants.OrdenacaoPedido.POR_DATA);

		mPerfil.setConfiguracao(mConfiguracao);
		service.salvar(mPerfil, this);
	}

	@Click(R.id.btnVincularConta)
	protected void onClickVincular(View v) {
		Intent intent = new Intent(getActivity(), ContaTagPlusActivity_.class);
		startActivityForResult(intent, 999);
	}

	@Override
	public void onPreInit() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPostExecute(RequestResult result) {
		if (!result.isError()) {
			Perfil p = (Perfil) result.getData();
			mSession.addUsuario(p);
			Toast.makeText(getActivity(), getString(R.string.configuracoes_salvo), Toast.LENGTH_SHORT).show();
			moverParaTelaPedidos();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 999 && resultCode == Activity.RESULT_OK) {
			load();
			reconfigureMenu();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}

package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.helper.Constants;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.SessionHelper;

@SuppressLint("SetJavaScriptEnabled")
@EActivity(R.layout.activity_vincular_conta_nova)
public class VincularContaNovaActivity extends BaseActivity {

	@ViewById
	WebView webView;

	@Bean
	SessionHelper mSession;

	@Bean
	PerfilService mPerfilService;

	Perfil mPerfil;

	@Override
	protected void load() {
		super.load();

		mPerfil = getUsuarioLogado();
		configureUi();
	}

	@Override
	public void configureActionBar() {

	}

	@UiThread
	protected void configureUi() {
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
		webView.getSettings().setSupportMultipleWindows(false);
		webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {

				if (url.contains(Constants.WebService.NEW_ACCOUNT_FINAL)) {
					view.loadUrl("javascript:(function() { " + jsInjectCode + "})()");
					view.loadUrl("javascript:HTMLOUT.processHTML(document.getElementsByTagName('html')[0].innerHTML);");
				}
				super.onPageFinished(view, url);
			}

		});
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		webView.loadUrl(Constants.WebService.NEW_ACCOUNT);

	}

	protected void salvar() {
		if (mPerfil.isVinculado() && !StringHelper.isNullOrEmpty(mPerfil.getUsuario()) && !StringHelper.isNullOrEmpty(mPerfil.getSistema())) {
			mPerfilService.salvar(mPerfil, new BusinessListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					if (!result.isError()) {
						mSession.addUsuario(mPerfil);
						setResult(RESULT_OK);
						finish();
					}
				}
			});
		}
	}

	private final String jsInjectCode = "function parseForm(event) {" + "    var form = this;"
			+ "    if (this.tagName.toLowerCase() != 'form')" + "        form = this.form;" + "    var data = '';"
			+ "    if (!form.method)  form.method = 'get';" + "    data += 'method=' + form.method;"
			+ "    data += '&action=' + form.action;" + "    var inputs = document.forms[0].getElementsByTagName('input');"
			+ "    for (var i = 0; i < inputs.length; i++) {" + "         var field = inputs[i];"
			+ "         if (field.type != 'submit' && field.type != 'reset' && field.type != 'button')"
			+ "             data += '&' + field.name + '=' + field.value;" + "    }" + "    HTMLOUT.processFormData(data);" + "}" + ""
			+ "for (var form_idx = 0; form_idx < document.forms.length; ++form_idx)"
			+ "    document.forms[form_idx].addEventListener('submit', parseForm, false);"
			+ "var inputs = document.getElementsByTagName('input');" + "for (var i = 0; i < inputs.length; i++) {"
			+ "    if (inputs[i].getAttribute('type') == 'button')" + "        inputs[i].addEventListener('click', parseForm, false);"
			+ "}" + "";

	class MyJavaScriptInterface {
		@JavascriptInterface
		public void processFormData(String text) {
			String[] form = text.split("&");

			for (String string : form) {
				if (string.contains("endereco_sistema"))
					mPerfil.setSistema(string.replace("endereco_sistema=", ""));
				if (string.contains("login"))
					mPerfil.setUsuario(string.replace("login=", ""));
				if (string.contains("senha"))
					mPerfil.setSenha(string.replace("senha=", ""));
			}
		}

		@JavascriptInterface
		public void processHTML(String html) {
			if (html.contains("<head></head><body>"))
				mPerfil.setKey(html.replace("<head></head><body>", "").replace("</body>", ""));

			salvar();
		}
	}

}

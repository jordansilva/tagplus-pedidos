package br.com.tagplus.pedidos;

import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Cidade;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.helper.Constants;
import br.com.tagplus.pedidos.helper.DocumentValidator;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.AutoCompleteFloatLabel;
import br.com.tagplus.pedidos.ui.Mask;
import br.com.tagplus.pedidos.ui.SessionHelper;
import br.com.tagplus.pedidos.ui.listener.ClienteListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

import com.iangclifton.android.floatlabel.FloatLabel;

@EFragment(R.layout.fragment_cliente_detalhe)
public class ClienteDetalheFragment extends BaseFragment implements UpdateFragmentListener {

	@Bean
	PerfilService perfilService;

	@Bean
	SessionHelper session;

	@ViewById
	Spinner spinnerTipoCliente;

	@ViewById
	FloatLabel txtNome;

	@ViewById
	FloatLabel txtDocumento;

	@ViewById
	FloatLabel txtEmail;

	@ViewById
	FloatLabel txtTelefone;

	@ViewById
	FloatLabel txtCep;

	@ViewById
	FloatLabel txtLogradouro;

	@ViewById
	FloatLabel txtNumero;

	@ViewById
	FloatLabel txtComplemento;

	@ViewById
	FloatLabel txtBairro;

	@ViewById
	AutoCompleteFloatLabel txtCidade;

	@ViewById
	FloatLabel txtEstado;

	@ViewById
	EditText txtNota;

	Cliente mCliente;
	ClienteListener listener;
	Cidade mCidadeSelected;
	TextWatcher cpfMask;
	TextWatcher cnpjMask;
	TextWatcher cepMask;
	String documento;
	boolean alreadyCalled;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = super.onCreateView(inflater, container, savedInstanceState);

		return v;
	}

	@Override
	protected void load() {
		super.load();

		if (mCliente == null)
			mCliente = listener.getCliente();

		listarCidades();

		if (StringHelper.isNullOrEmpty(mCliente.getNome())) {
			txtNome.getEditText().requestFocus();
			showSoftKeyboard(txtNome.getEditText());
		} else {
			txtNome.getEditText().clearFocus();
			hideSoftKeyboard(txtNome.getEditText());
		}

		cpfMask = Mask.insert("###.###.###-##", txtDocumento.getEditText());
		cnpjMask = Mask.insert("##.###.###/####-##", txtDocumento.getEditText());
		cepMask = Mask.insert("#####-###", txtCep.getEditText());
		txtCep.getEditText().addTextChangedListener(cepMask);

		spinnerTipoCliente.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

				if (pos == 0) {
					txtDocumento.getEditText().getEditableText().clear();
					txtDocumento.setLabel(R.string.cpf);
					txtDocumento.setText(mCliente.getCpf());
					txtDocumento.getEditText().removeTextChangedListener(cpfMask);
					txtDocumento.getEditText().removeTextChangedListener(cnpjMask);
					txtDocumento.getEditText().addTextChangedListener(cpfMask);
					// if (alreadyCalled)
					// documento = null;
				}
				if (pos == 1) {
					txtDocumento.getEditText().getEditableText().clear();
					txtDocumento.setLabel(R.string.cnpj);
					txtDocumento.setText(mCliente.getCnpj());
					txtDocumento.getEditText().removeTextChangedListener(cnpjMask);
					txtDocumento.getEditText().removeTextChangedListener(cpfMask);
					txtDocumento.getEditText().addTextChangedListener(cnpjMask);
					// if (alreadyCalled)
					// documento = null;
				}

				alreadyCalled = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		if (mCliente != null)
			carregarCampos();
		else {
			if (session.getConfiguracao().getTipoPessoa() == Constants.TipoPessoa.PESSOA_FISICA)
				spinnerTipoCliente.setSelection(0);
			else
				spinnerTipoCliente.setSelection(1);

			spinnerTipoCliente.invalidate();
		}
	}

	@Background
	protected void listarCidades() {
		perfilService.listarCidades(new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError() && result.getData() != null)
					carregarCidadesAutoComplete(result);

			}
		});
	}

	@SuppressWarnings("unchecked")
	@UiThread
	protected void carregarCidadesAutoComplete(RequestResult result) {
		if (!result.isError() && isAdded()) {
			List<Cidade> list = (List<Cidade>) result.getData();
			ArrayAdapter<Cidade> adapterFornecedores = new ArrayAdapter<Cidade>(getActivity(), R.layout.list_cidade, android.R.id.text1,
					list) {
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					View view = super.getView(position, convertView, parent);

					Cidade item = (Cidade) getItem(position);
					TextView text1 = (TextView) view.findViewById(android.R.id.text1);
					TextView text2 = (TextView) view.findViewById(android.R.id.text2);

					text1.setText(item.getNome());
					text2.setText(item.getEstadoNome());
					return view;
				}
			};
			txtCidade.getAutoCompleteTextView().setAdapter(adapterFornecedores);
			txtCidade.getAutoCompleteTextView().setThreshold(0);
			txtCidade.getAutoCompleteTextView().addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					if (StringHelper.isNullOrEmpty(s.toString())) {
						mCidadeSelected = null;
						txtEstado.setText("");
					}
				}
			});
			txtCidade.getAutoCompleteTextView().setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Object obj = parent.getItemAtPosition(position);
					if (obj != null) {
						mCidadeSelected = (Cidade) obj;
						txtEstado.setText(mCidadeSelected.getEstado().getNome());
					}
				}
			});

		}
	}

	@UiThread
	protected void carregarCampos() {
		// Dados
		txtNome.setText(mCliente.getNome());
		if (mCliente.isPessoaFisica()) {
			documento = mCliente.getCpf();
			spinnerTipoCliente.setSelection(0);
		} else if (mCliente.isPessoaJuridica()) {
			documento = mCliente.getCnpj();
			spinnerTipoCliente.setSelection(1);
		}

		txtEmail.setText(mCliente.getEmail());
		txtTelefone.setText(mCliente.getTelefone());
		// Endereço
		txtCep.setText(mCliente.getCep());
		txtLogradouro.setText(mCliente.getLogradouro());
		txtNumero.setText(mCliente.getNumero());
		txtComplemento.setText(mCliente.getComplemento());
		txtBairro.setText(mCliente.getBairro());

		if (mCliente.getCidade() != null) {
			txtCidade.setText(mCliente.getCidade().getNome());
			txtEstado.setText(mCliente.getCidade().getEstado().getNome());
		}
		// Nota
		txtNota.setText(mCliente.getObservacao());

		mCidadeSelected = mCliente.getCidade();
	}

	public boolean salvarClienteComValidacao() {

		if (validarCampos()) {
			salvarInstanciaCliente();
			return true;
		}

		return false;
	}

	public Cliente salvarInstanciaCliente() {
		mCliente = listener.getCliente();

		if (mCliente == null)
			mCliente = new Cliente();

		// Dados Pessoais
		mCliente.setNome(txtNome.getTextValue());

		if (spinnerTipoCliente.getSelectedItemPosition() == 0) {
			mCliente.setCpf(txtDocumento.getTextValue());
			mCliente.setCnpj("");
			mCliente.setPessoaFisica(true);
		} else {
			mCliente.setCnpj(txtDocumento.getTextValue());
			mCliente.setCpf("");
			mCliente.setPessoaJuridica(true);
		}
		mCliente.setEmail(txtEmail.getTextValue());
		mCliente.setTelefone(txtTelefone.getTextValue());

		// Endereço
		mCliente.setCep(txtCep.getTextValue());
		mCliente.setLogradouro(txtLogradouro.getTextValue());
		mCliente.setNumero(txtNumero.getTextValue());
		mCliente.setComplemento(txtComplemento.getTextValue());
		mCliente.setBairro(txtBairro.getTextValue());

		if (mCidadeSelected != null && mCidadeSelected.getNome() != null
				&& !mCidadeSelected.getNome().equalsIgnoreCase(txtCidade.getTextValue()))
			mCidadeSelected = new Cidade();

		mCliente.setCidade(mCidadeSelected);

		// Nota
		mCliente.setObservacao(txtNota.getEditableText().toString());

		return mCliente;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (ClienteListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement ClienteListener.");
		}
	}

	protected boolean validarCampos() {

		boolean isValid = true;

		txtEmail = (FloatLabel) getActivity().findViewById(R.id.txtEmail);
		txtNome = (FloatLabel) getActivity().findViewById(R.id.txtNome);

		if (txtEmail.getTextValue() != null && !txtEmail.getTextValue().trim().isEmpty()) {
			boolean emailValido = android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getTextValue()).matches();
			if (!emailValido) {
				txtEmail.getEditText().requestFocus();
				txtEmail.getEditText().setError(getString(R.string.email_invalido));
			}

			isValid = emailValido && isValid;
		}

		if (txtNome.getTextValue() == null || txtNome.getTextValue().trim().isEmpty()) {
			txtNome.getEditText().requestFocus();
			txtNome.getEditText().setError(getString(R.string.campo_obrigatorio));
			isValid = false;
		}

		String document = txtDocumento.getTextValue();
		if (!StringHelper.isNullOrEmpty(document)) {
			if (spinnerTipoCliente.getSelectedItemPosition() == 0) {
				if (!DocumentValidator.isCPFValid(document)) {
					txtDocumento.getEditText().requestFocus();
					txtDocumento.getEditText().setError(getString(R.string.cpf_invalido));
					isValid = false;
				}

			} else {
				if (!DocumentValidator.isCNPJValid(document)) {
					txtDocumento.getEditText().requestFocus();
					txtDocumento.getEditText().setError(getString(R.string.cnpj_invalido));
					isValid = false;
				}
			}
		}

		if (!validarEndereco())
			isValid = false;

		return isValid;
	}

	protected boolean validarEndereco() {
		boolean isValid = true;
		boolean isCepNull = StringHelper.isNullOrEmpty(txtCep.getTextValue());
		boolean isLogradouroNull = StringHelper.isNullOrEmpty(txtLogradouro.getTextValue());
		boolean isNumeroNull = StringHelper.isNullOrEmpty(txtNumero.getTextValue());

		boolean isBairroNull = StringHelper.isNullOrEmpty(txtBairro.getTextValue());
		boolean isCidadeNull = mCidadeSelected == null || mCidadeSelected.getCode() == null
				|| StringHelper.isNullOrEmpty(txtCidade.getTextValue());
		boolean isCidadeInvalida = !isCidadeNull
				&& (StringHelper.isNullOrEmpty(mCidadeSelected.getNome()) || !mCidadeSelected.getNome().equalsIgnoreCase(
						txtCidade.getTextValue()));
		boolean isEstadoNull = StringHelper.isNullOrEmpty(txtEstado.getTextValue());

		txtCidade = (AutoCompleteFloatLabel) getActivity().findViewById(R.id.txtCidade);
		txtBairro = (FloatLabel) getActivity().findViewById(R.id.txtBairro);
		txtNumero = (FloatLabel) getActivity().findViewById(R.id.txtNumero);
		txtLogradouro = (FloatLabel) getActivity().findViewById(R.id.txtLogradouro);

		// se todos os campos estiverem preenchidos, não entra.
		if (!(isCepNull && isLogradouroNull && isNumeroNull && isBairroNull && isCidadeNull && isEstadoNull)) {
			if (isCepNull | isLogradouroNull | isNumeroNull | isBairroNull | isCidadeNull | isCidadeInvalida | isEstadoNull) {
				isValid = false;

				if (isCidadeNull | isCidadeInvalida | isEstadoNull) {
					mCidadeSelected = new Cidade();
					txtEstado.setText("");
					txtCidade.getAutoCompleteTextView().requestFocus();

					int message = (isCidadeInvalida) ? R.string.cidade_invalida : R.string.campo_obrigatorio;
					txtCidade.getAutoCompleteTextView().setError(getString(message));
					txtEstado.getEditText().setError(getString(R.string.campo_obrigatorio));
				}

				if (isBairroNull) {
					txtBairro.getEditText().requestFocus();
					txtBairro.getEditText().setError(getString(R.string.campo_obrigatorio));
				}

				if (isNumeroNull) {
					txtNumero.getEditText().requestFocus();
					txtNumero.getEditText().setError(getString(R.string.campo_obrigatorio));
				}

				if (isLogradouroNull) {
					txtLogradouro.getEditText().requestFocus();
					txtLogradouro.getEditText().setError(getString(R.string.campo_obrigatorio));
				}

				if (isCepNull) {
					txtCep.getEditText().requestFocus();
					txtCep.getEditText().setError(getString(R.string.campo_obrigatorio));
				}
			}
		}

		if (!isCepNull) {
			if (txtCep.getTextValue().replace("-", "").length() < 8) {
				isValid = false;
				txtCep.getEditText().requestFocus();
				txtCep.getEditText().setError(getString(R.string.valor_invalido));
			}
		}

		return isValid;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;

		int id = item.getItemId();
		if (id == R.id.action_save) {
			try {
				if (salvarClienteComValidacao())
					listener.salvarCliente();
				result = false;
			} catch (Exception ex) {
				result = true;
			}
		}

		return result;
	}

	public void clearFocus() {
		View v = getView();
		if (v != null) {
			v.clearFocus();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		update();
	}

	@Override
	public void update() {
		salvarInstanciaCliente();
		listener.updateDetalhes(mCliente);
	}

	@Override
	public boolean validar() {
		return validarCampos();
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (!isVisibleToUser) {
			clearFocus();
		}
	}

}

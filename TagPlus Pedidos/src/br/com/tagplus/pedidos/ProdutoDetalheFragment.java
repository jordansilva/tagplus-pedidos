package br.com.tagplus.pedidos;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.helper.FileHelper;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.MoneyTextWatcher;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.listener.ProdutoListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

import com.crashlytics.android.Crashlytics;
import com.iangclifton.android.floatlabel.FloatLabel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

@EFragment(R.layout.fragment_produto_detalhe)
public class ProdutoDetalheFragment extends BaseFragment implements UpdateFragmentListener {

	@ViewById
	ImageView imgProduto;

	@ViewById
	FloatLabel txtDescricao;

	@ViewById
	FloatLabel txtCategoria;

	@ViewById
	FloatLabel txtCodigoInterno;

	@ViewById
	FloatLabel txtCodigoEAN;

	@ViewById
	FloatLabel txtQuantidadeEstoque;

	@ViewById
	FloatLabel txtUnidadeMedida;

	@ViewById
	FloatLabel txtValorProduto;

	@ViewById
	EditText txtNota;

	Produto mProduto;
	ProdutoListener listener;
	Bitmap mImageProduto;
	boolean isImagemNova = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	protected void load() {
		super.load();
		UIHelper.showLoading(getActivity());

		if (mProduto == null)
			mProduto = listener.getProduto();

		TextWatcher watcher = new MoneyTextWatcher(txtValorProduto.getEditText());
		txtValorProduto.getEditText().addTextChangedListener(watcher);

		if (mImageProduto != null) {
			imgProduto.setImageBitmap(mImageProduto);
		}

		if (mProduto != null)
			carregarCampos();

		UIHelper.hideLoading(getActivity());

	}

	@UiThread
	protected void carregarCampos() {
		// Dados
		txtDescricao.setText(mProduto.getDescricao());
		txtCategoria.setText(mProduto.getCategoria());

		txtCodigoInterno.setVisibility(StringHelper.isNullOrEmpty(mProduto.getCodigoInterno()) ? View.GONE : View.VISIBLE);
		txtCodigoInterno.setText(mProduto.getCodigoInterno());

		txtCodigoEAN.setVisibility(StringHelper.isNullOrEmpty(mProduto.getCodigoEAN()) ? View.GONE : View.VISIBLE);
		txtCodigoEAN.setText(mProduto.getCodigoEAN());
		txtQuantidadeEstoque.setText(String.valueOf(mProduto.getQuantidadeEstoque()));
		txtUnidadeMedida.setText(mProduto.getUnidadeMedida());
		txtValorProduto.setText(mProduto.getValorProduto());

		carregarImagem();

		// Nota
		txtNota.setText(mProduto.getObservacao());
		hideSoftKeyboard(txtDescricao);
	}

	@Background
	protected void carregarImagem() {
		try {
			if (!StringHelper.isNullOrEmpty(mProduto.getImage())) {
				File file = getActivity().getFileStreamPath(mProduto.getImage());
				if (file.exists()) {
					final Bitmap b = Picasso.with(getActivity()).load(file).get();
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							imgProduto.setImageBitmap(b);
						}
					});
				}
			} else if (!StringHelper.isNullOrEmpty(mProduto.getImageExterna())) {
				mImageProduto = Picasso.with(getActivity()).load(Uri.parse(mProduto.getImageExterna())).get();
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						imgProduto.setImageBitmap(mImageProduto);
						imgProduto.setScaleType(ScaleType.CENTER_CROP);
					}
				});
			}
		} catch (Exception e) {
			Crashlytics.logException(e);
		}
	}

	public boolean salvarProdutoComValidacao() {

		if (validarCampos()) {
			salvarInstanciaProduto();
			return true;
		}

		return false;
	}

	public void salvarInstanciaProduto() {

		mProduto = listener.getProduto();

		if (mProduto == null)
			mProduto = new Produto();

		// Dados
		mProduto.setDescricao(txtDescricao.getTextValue());
		mProduto.setCategoria(txtCategoria.getTextValue());

		if (!StringHelper.isNullOrEmpty(txtQuantidadeEstoque.getTextValue()))
			mProduto.setQuantidadeEstoque(Double.valueOf(txtQuantidadeEstoque.getTextValue()));

		mProduto.setUnidadeMedida(txtUnidadeMedida.getTextValue());

		if (mImageProduto != null && isImagemNova) {
			mProduto.setImageBitmap(mImageProduto);
			mProduto.setImageSincronizada(false);
		}
		try {
			if (!StringHelper.isNullOrEmpty(txtValorProduto.getTextValue())) {
				NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
				Number money = moneyFormat.parse(txtValorProduto.getTextValue());

				mProduto.setValorDoProduto(Money.of(CurrencyUnit.getInstance(Locale.getDefault()), money.doubleValue()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// Nota
		mProduto.setObservacao(txtNota.getEditableText().toString());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (ProdutoListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement ProdutoListener.");
		}
	}

	int REQUEST_CAMERA = 120;
	int SELECT_FILE = 121;

	File photo;
	
	@Click({ R.id.imgProduto, R.id.btnAlterarFoto })
	protected void onAlterarFoto(View v) {
		hideSoftKeyboard(txtDescricao);

		final CharSequence[] items = { getString(R.string.tirar_foto), getString(R.string.escolher_biblioteca),
				getString(R.string.cancelar) };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.alterar_foto);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					try {
						photo = FileHelper.createImageFile();
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
						startActivityForResult(intent, REQUEST_CAMERA);
					} catch (IOException e) {
						showToast(R.string.error_foto);
					}

				} else if (item == 1) {
					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(Intent.createChooser(intent, getString(R.string.selecionar_arquivo)), SELECT_FILE);
				} else if (item == 2) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == REQUEST_CAMERA || requestCode == SELECT_FILE) {
				requestImage(data, requestCode);
			}
		}
	}

	@Background
	protected void requestImage(Intent data, int requestCode) {

		try {
			if (requestCode == REQUEST_CAMERA) {

				File f = new File(photo.toURI());

				if (f.exists()) {
					mImageProduto = FileHelper.decodeBitmap(f);
					f.delete();
				}

			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				mImageProduto = FileHelper.getBitmapFromUri(getActivity(), selectedImageUri);
			}

			if (mImageProduto != null) {
				isImagemNova = true;
				setImage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@UiThread
	protected void setImage() {
		imgProduto.setImageBitmap(mImageProduto);
	}

	protected boolean validarCampos() {

		if (txtDescricao.getTextValue() == null || txtDescricao.getTextValue().trim().isEmpty()) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					txtDescricao.getEditText().requestFocus();
					txtDescricao.getEditText().setError(getString(R.string.campo_obrigatorio));
				}
			});
			return false;
		} else if (txtDescricao.getTextValue().length() < 3) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					txtDescricao.getEditText().requestFocus();
					txtDescricao.getEditText().setError(getString(R.string.error_nome_produto_pequeno));
				}
			});
			return false;
		}

		return true;
	}

	Transformation transformation = new Transformation() {

		@Override
		public Bitmap transform(Bitmap source) {
			int targetWidth = imgProduto.getWidth();

			double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
			int targetHeight = (int) (targetWidth * aspectRatio);
			Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
			if (result != source) {
				source.recycle();
				source = null;
			}
			return result;
		}

		@Override
		public String key() {
			return "transformation" + " desiredWidth";
		}
	};

	public void clearFocus() {
		View v = getView();
		if (v != null) {
			v.clearFocus();
		}
	}

	@Override
	public void update() {
		salvarInstanciaProduto();
		listener.updateDetalhes(mProduto);
	}

	@Override
	public boolean validar() {
		return validarCampos();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;

		int id = item.getItemId();
		if (id == R.id.action_save) {
			try {
				if (salvarProdutoComValidacao())
					listener.salvarProduto();
				result = false;
			} catch (Exception ex) {
				result = true;
			}
		}

		return result;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		update();
	}
}

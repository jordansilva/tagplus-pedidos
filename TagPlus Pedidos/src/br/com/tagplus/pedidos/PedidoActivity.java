package br.com.tagplus.pedidos;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ClienteService;
import br.com.tagplus.pedidos.business.PedidoService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.FormaPagamento;
import br.com.tagplus.pedidos.business.domain.Pedido;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;
import br.com.tagplus.pedidos.ui.ClearableEditText.ClearableEditTextListener;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment.YesNoListener;
import br.com.tagplus.pedidos.ui.PercentageTextWatcher;
import br.com.tagplus.pedidos.ui.StretchedListView;
import br.com.tagplus.pedidos.ui.UIHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterFormaPagamento;
import br.com.tagplus.pedidos.ui.adapter.AdapterPedidoProduto;
import br.com.tagplus.pedidos.ui.listener.AdapterPedidoProdutoListener;

import com.iangclifton.android.floatlabel.ClearableFloatLabel;
import com.jordansilva.android.prettyui.ExtendedButton;
import com.jordansilva.android.prettyui.ExtendedEditText;
import com.jordansilva.android.prettyui.filters.InputFilterDecimalDigits;
import com.jordansilva.android.prettyui.filters.InputFilterMinMax;

@EActivity(R.layout.activity_pedido)
public class PedidoActivity extends BaseActivity implements AdapterPedidoProdutoListener {

	private final static int REQUESTCODE_PRODUTO = 1;
	private final static int REQUESTCODE_FORMAPAGAMENTO = 2;
	private final static int REQUESTCODE_CLIENTE = 3;
	private Cliente mClienteSelected;
	private AdapterPedidoProduto mAdapterProduto;
	private AdapterFormaPagamento mAdapterFormaPagamento;
	private MenuItem mMenuSalvar;

	@Extra
	UUID codigoPedido;

	Pedido mPedido;

	@ViewById
	StretchedListView listProdutos;

	@ViewById
	RelativeLayout listProdutosFooter;

	@ViewById
	StretchedListView listFormasPagamento;

	@ViewById
	RelativeLayout listFormasPagamentoFooter;

	@ViewById
	ClearableFloatLabel txtCliente;

	@ViewById
	EditText txtNota;

	@ViewById
	RelativeLayout relResumo;

	@ViewById
	TextView txtDate;

	@ViewById
	TextView txtSubtotal;

	@ViewById
	ExtendedEditText txtDesconto;

	@ViewById
	TextView txtTotal;

	@ViewById
	Button btnFecharPedido;

	@Bean
	ClienteService clienteService;

	@Bean
	PedidoService pedidoService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	protected void load() {
		super.load();
		UIHelper.showLoading(PedidoActivity.this);
		if (codigoPedido == null) {
			mPedido = new Pedido();
			mPedido.setDataPedido(DateTime.now());
		} else {
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			pedidoService.getPedido(codigoPedido, new BusinessListener() {

				@Override
				@UiThread
				public void onPreInit() {

				}

				@Override
				public void onPostExecute(RequestResult result) {

					if (!result.isError()) {
						mPedido = (Pedido) result.getData();

						if (mPedido.getCliente() != null)
							mClienteSelected = mPedido.getCliente();
						setTitle(mPedido.getNomeCliente());
					}
				}
			});
		}

		configurarUi();
		carregarDados();
		UIHelper.hideLoading(PedidoActivity.this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.pedido, menu);

		mMenuSalvar = menu.findItem(R.id.action_save);
		if (mPedido != null && mPedido.getCodigoStatus() != Pedido.ABERTO)
			mMenuSalvar.setVisible(false);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			salvarPedido(Pedido.ABERTO);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void configurarUi() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		// DateFormat f = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
		// DateFormat.SHORT, Locale.getDefault());
		txtDate.setText(sdf.format(mPedido.getDataPedido().toDate()));

		ExtendedButton btnProdutos = (ExtendedButton) listProdutosFooter.findViewById(R.id.emptyview_add_new_item);
		btnProdutos.setText(R.string.adicionar_produto);
		btnProdutos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarProduto();
			}
		});

		ExtendedButton btnFormasPagamento = (ExtendedButton) listFormasPagamentoFooter.findViewById(R.id.emptyview_add_new_item);
		btnFormasPagamento.setText(R.string.adicionar_pagamento);
		btnFormasPagamento.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarFormaPagamento();
			}
		});

		TextWatcher watcher = new PercentageTextWatcher(txtDesconto);
		txtDesconto.addTextChangedListener(watcher);

		txtDesconto.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				Double desconto = 0.0;
				try {
					if (s != null && s.length() > 0)
						desconto = Double.valueOf(s.toString().replace("%", ""));
					mPedido.setDesconto(desconto);
					carregarResumoPedido();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});

		txtCliente.getEditText().setListener(new ClearableEditTextListener() {

			@Override
			public void onTextChanged(EditText view, String text) {
			}

			@Override
			public void didClearEditText() {
				txtCliente.getEditText().getEditableText().clear();
				mClienteSelected = null;
				carregarCliente();
			}
		});
	}

	protected void carregarDados() {

		ArrayList<InputFilter> filters = new ArrayList<InputFilter>(Arrays.asList(txtDesconto.getFilters()));
		InputFilterMinMax filterMinMax = new InputFilterMinMax("0.00", "100.00");
		filterMinMax.removeChars("%");
		filters.add(filterMinMax);

		filters.add(new InputFilterDecimalDigits(2));
		txtDesconto.setFilters(filters.toArray(new InputFilter[] {}));

		txtDesconto.setText(String.valueOf(mPedido.getDesconto()));
		txtNota.setText(mPedido.getObservacao());

		// listarClientes();
		mClienteSelected = mPedido.getCliente();
		carregarCliente();

		carregarProdutos();
		carregarFormaDePagamento();

		if (mPedido.getCodigoStatus() != Pedido.ABERTO) {
			if (mMenuSalvar != null)
				mMenuSalvar.setVisible(false);
			btnFecharPedido.setVisibility(View.GONE);
			mAdapterProduto.setEnabled(false);
			mAdapterFormaPagamento.setEnabled(false);
			txtCliente.setEnabled(false);
			txtCliente.getEditText().showClearButton(false);
			txtDesconto.setEnabled(false);
			txtNota.setEnabled(false);
			listFormasPagamentoFooter.setVisibility(View.GONE);
			listProdutosFooter.setVisibility(View.GONE);
		}
	}

	@Click(R.id.txtCliente)
	protected void clienteClick() {
		Intent intent = SelectClientesActivity_.intent(this).get();
		startActivityForResult(intent, REQUESTCODE_CLIENTE);
	}

	/**
	 * Cliente
	 **/
	@UiThread
	protected void carregarCliente() {
		if (mClienteSelected != null) {
			txtCliente.setText(mClienteSelected.getNome());
			txtCliente.setLabel(R.string.tab_cliente);
		} else
			txtCliente.setLabel(R.string.select_cliente);

		txtCliente.clearFocus();
		txtCliente.getEditText().clearFocus();
		txtCliente.getEditText().setLongClickable(false);
	}

	protected void carregarProdutos() {

		if (mPedido != null && mPedido.getProdutos() != null) {
			mAdapterProduto = new AdapterPedidoProduto(this, (List<PedidoProduto>) mPedido.getProdutos(), this);
			listProdutos.setAdapter(mAdapterProduto);
		} else
			listProdutos.setAdapter(null);

		carregarResumoPedido();
	}

	protected void carregarFormaDePagamento() {
		if (mPedido != null && mPedido.getFormaPagamento() != null) {
			mAdapterFormaPagamento = new AdapterFormaPagamento(this, (List<FormaPagamento>) mPedido.getFormaPagamento());
			listFormasPagamento.setAdapter(mAdapterFormaPagamento);
		} else
			listFormasPagamento.setAdapter(null);
	}

	protected void carregarResumoPedido() {
		if (mPedido != null && mPedido.getProdutos().size() > 0) {
			relResumo.setVisibility(View.VISIBLE);

			mPedido.recalcularValores();

			NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
			String subtotal = moneyFormat.format(mPedido.getSubtotal());
			txtSubtotal.setText(subtotal);

			txtTotal.setText(mPedido.getTotalPedido());
		} else
			relResumo.setVisibility(View.GONE);
	}

	@Override
	public void notifyPedidoDataSetChanged() {
		mPedido.setProdutos(mAdapterProduto.getItems());
		carregarResumoPedido();
	}

	protected void adicionarProduto() {
		Intent intent = ListaProdutoPedidoGradeActivity_.intent(this)
				.mPedidoProdutos((ArrayList<PedidoProduto>) mAdapterProduto.getItems()).get();
		startActivityForResult(intent, REQUESTCODE_PRODUTO);
	}

	protected void adicionarFormaPagamento() {

		Intent intent = FormaPagamentoActivity_.intent(this).valorInicial(mPedido.valorRestantePagamento()).get();
		startActivityForResult(intent, REQUESTCODE_FORMAPAGAMENTO);
	}

	@Click(R.id.btnFecharPedido)
	protected void onClickFecharPedido(View v) {

		String message = getString(R.string.fechar_pedido_mensagem);
		if (mPedido.valorRestantePagamento().compareTo(BigDecimal.ZERO) > 0) {
			NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
			String valor = moneyFormat.format(mPedido.valorRestantePagamento());
			message = String.format(getString(R.string.fechar_pedido_mensagem_debito), valor);
		}

		DialogDeleteFragment dialog = new DialogDeleteFragment(R.string.fechar_pedido, message, R.string.dialog_confirmar,
				R.string.dialog_cancelar, new YesNoListener() {

					@Override
					public void onYes() {
						salvarPedido(Pedido.CONFIRMADO);
					}

					@Override
					public void onNo() {
						// TODO Auto-generated method stub

					}
				});

		dialog.show(getFragmentManager(), DialogDeleteFragment.TAG);

	}

	private void salvarPedido(int statusPedido) {

		if (validarCampos()) {
			mPedido.setCliente(mClienteSelected);

			if (mAdapterProduto != null)
				mPedido.setProdutos(mAdapterProduto.getItems());

			if (mAdapterFormaPagamento != null)
				mPedido.setFormaPagamento(mAdapterFormaPagamento.getItems());

			mPedido.setCodigoStatus(statusPedido);
			mPedido.setObservacao(txtNota.getEditableText().toString());
			pedidoService.salvar(getUsuarioLogado().getCode(), mPedido, new BusinessListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					salvarPedidoResult(result);
				}
			});
		}
	}

	@UiThread
	protected void salvarPedidoResult(RequestResult result) {
		if (!result.isError()) {
			if (mPedido.getCode() == null)
				showToast(R.string.pedido_criado);
			else
				showToast(R.string.pedido_salvo);

			setResult(RESULT_OK);
			finish();
		}
	}

	protected boolean validarCampos() {
		boolean isValid = true;

		if (mAdapterProduto.getItems() == null || mAdapterProduto.getItems().isEmpty()) {
			isValid = false;
			showToast(R.string.error_pedido_sem_produtos);
		}

		if (isValid
				&& (mPedido == null || mPedido.getSubtotal().compareTo(BigDecimal.ZERO) == 0 || mPedido.getTotal().compareTo(
						BigDecimal.ZERO) == 0)) {
			isValid = false;
			showToast(R.string.error_pedido_subtotal_total);
		}
		// Validar Cliente
		// if (mClienteSelected == null)
		// isValid = false;
		// else if (StringHelper.isNullOrEmpty(txtCliente.getTextValue()))
		// isValid = false;
		// else if
		// (!txtCliente.getTextValue().equalsIgnoreCase(mClienteSelected.getNome()))
		// isValid = false;
		//
		// if (!isValid)
		// txtCliente.getAutoCompleteTextView().setError(getString(R.string.campo_obrigatorio));

		return isValid;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			if (requestCode == REQUESTCODE_PRODUTO) {
				mPedido.setProdutos((List<PedidoProduto>) data.getSerializableExtra("pedidoProdutos"));
				carregarProdutos();
			}

			if (requestCode == REQUESTCODE_FORMAPAGAMENTO) {
				ArrayList<FormaPagamento> formaPagamento = new ArrayList<FormaPagamento>();
				if (mPedido.getFormaPagamento() != null)
					formaPagamento = (ArrayList<FormaPagamento>) mPedido.getFormaPagamento();

				formaPagamento.add((FormaPagamento) data.getSerializableExtra("data"));
				mPedido.setFormaPagamento(formaPagamento);

				carregarFormaDePagamento();
			}

			if (requestCode == REQUESTCODE_CLIENTE) {
				mClienteSelected = (Cliente) data.getSerializableExtra("data");
				carregarCliente();
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void adicionarQuantidade(UUID id, int quantidade) {
		mPedido.setProdutos(mAdapterProduto.getItems());
		carregarResumoPedido();
	}
}

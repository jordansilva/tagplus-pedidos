package br.com.tagplus.pedidos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.ProdutoService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.ui.adapter.AdapterPedidoProdutoGrid;
import br.com.tagplus.pedidos.ui.listener.AdapterPedidoProdutoListener;

import com.iangclifton.android.floatlabel.FloatLabel;
import com.jordansilva.android.prettyui.RelativeLayoutAnimation;
import com.jordansilva.android.prettyui.empty.EmptyView;

@EActivity(R.layout.fragment_grid)
public class ListaProdutoPedidoGradeActivity extends BaseActivity implements BusinessListener, OnQueryTextListener, OnActionExpandListener,
		AdapterPedidoProdutoListener {

	private Menu mMenu;
	private AdapterPedidoProdutoGrid mAdapter;
	private HashMap<UUID, Integer> mProdutosQuantidade;
	private SearchView mSearchView;

	@Extra
	ArrayList<PedidoProduto> mPedidoProdutos;

	@ViewById(R.id.grid)
	GridView gridView;

	@ViewById
	EmptyView emptyView;

	@ViewById
	RelativeLayoutAnimation buscaAvancada;

	@ViewById
	FloatLabel txtFornecedor;

	@ViewById
	FloatLabel txtCategoria;

	@ViewById
	FloatLabel txtAtributo;

	@ViewById
	TextView txtQuantidadeProdutos;

	@ViewById
	LinearLayout headerResultado;

	@Bean
	ProdutoService service;

	boolean isFiltrado = false;

	@Override
	protected void load() {
		super.load();

		emptyView.setButtonText(R.string.action_adicionar_novo_produto);
		emptyView.setListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				adicionarNovoProduto();
			}
		});
		listarProdutos();
	}

	@Background
	protected void listarProdutos() {
		service.listarProdutos(getUsuarioLogado().getCode(), this);
	}

	@Override
	public void onPreInit() {
	}

	@Override
	public void onPostExecute(RequestResult result) {
		carregarLista(result);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lista_produto_pedido, menu);

		MenuItem searchItem = menu.findItem(R.id.action_search);
		searchItem.setOnActionExpandListener(this);

		mSearchView = (SearchView) searchItem.getActionView();
		mSearchView.setQueryHint(getString(R.string.buscar_produto));
		mSearchView.setOnQueryTextListener(this);

		mMenu = menu;

		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			adicionarNovoProduto();
			return true;
		case R.id.action_busca_avancada:
			exibirBuscaAvancada();
			return true;
		case R.id.action_save:
			processarProdutos();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void adicionarNovoProduto() {
		Intent intent = ProdutoActivity_.intent(this).get();
		startActivityForResult(intent, 1);
	}

	@UiThread
	protected void carregarLista(RequestResult result) {
		if (!result.isError()) {

			if (buscaAvancada.getVisibility() == View.VISIBLE)
				exibirBuscaAvancada();

			@SuppressWarnings("unchecked")
			ArrayList<Produto> list = (ArrayList<Produto>) result.getData();

			mAdapter = new AdapterPedidoProdutoGrid(this, list, mPedidoProdutos, this);
			if (mProdutosQuantidade != null && mProdutosQuantidade.size() > 0)
				mAdapter.setProdutosQuantidade(mProdutosQuantidade);
			gridView.setAdapter(mAdapter);
			// gridView.setOnItemClickListener(this);
			gridView.setEmptyView(emptyView);
			verificarHeader();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		HashMap<UUID, Integer> produtosQuantidade = mAdapter.getProdutosQuantidade();
		outState.putSerializable("produtosQuantidade", produtosQuantidade);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey("produtosQuantidade"))
			mProdutosQuantidade = (HashMap<UUID, Integer>) savedInstanceState.get("produtosQuantidade");
	}

	@Override
	public void onBackPressed() {

		if (buscaAvancada.getVisibility() == View.VISIBLE)
			exibirBuscaAvancada();
		else
			super.onBackPressed();
		// processarProdutos();

	}

	private void processarProdutos() {

		mProdutosQuantidade = mAdapter.getProdutosQuantidade();

		ArrayList<PedidoProduto> mPedidoProdutosNovo = new ArrayList<PedidoProduto>();

		for (UUID codigoProduto : mProdutosQuantidade.keySet()) {
			PedidoProduto pedidoProduto = buscarProduto(codigoProduto);
			if (pedidoProduto == null) {
				pedidoProduto = new PedidoProduto();
				Produto produto = getProduto(codigoProduto);
				if (produto != null) {
					pedidoProduto.setProduto(produto);
					pedidoProduto.setValorUnidade(produto.getValorDoProduto());
				}
			}

			pedidoProduto.setQuantidade(mProdutosQuantidade.get(codigoProduto));
			if (pedidoProduto.getQuantidade() > 0)
				mPedidoProdutosNovo.add(pedidoProduto);
		}

		Intent intent = new Intent();
		intent.putExtra("pedidoProdutos", mPedidoProdutosNovo);
		setResult(RESULT_OK, intent);
		finish();
	}

	private Produto getProduto(UUID codigoProduto) {
		List<Produto> produtos = mAdapter.getItems();
		for (Produto p : produtos) {
			if (p.getCode().equals(codigoProduto))
				return p;
		}

		return null;
	}

	private PedidoProduto buscarProduto(UUID codigoProduto) {
		if (mPedidoProdutos != null) {
			for (PedidoProduto pedidoProduto : mPedidoProdutos)
				if (pedidoProduto.getProduto().getCode().equals(codigoProduto))
					return pedidoProduto;
		}

		return null;
	}

	protected void exibirBuscaAvancada() {

		final int height = (buscaAvancada.getHeight() + buscaAvancada.getPaddingTop());

		if (buscaAvancada.getVisibility() == View.GONE)
			buscaAvancada.setVisibility(View.INVISIBLE);

		if (buscaAvancada.getVisibility() == View.INVISIBLE) {

			if (mAdapter == null || mAdapter.getCount() == 0)
				emptyView.setVisibility(View.INVISIBLE);

			buscaAvancada.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buscaAvancada.getLayoutParams();
					params.topMargin += height;
					buscaAvancada.setVisibility(View.VISIBLE);
					buscaAvancada.setLayoutParams(params);
				}
			});

			AnimationSet animationSet = new AnimationSet(true);

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, height);
			translate.setDuration(300);
			animationSet.addAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(0, 1);
			alpha.setDuration(300);
			animationSet.addAnimation(alpha);

			buscaAvancada.startAnimation(animationSet);
		}

		if (buscaAvancada.getVisibility() == View.VISIBLE) {

			if (mAdapter == null || mAdapter.getCount() == 0)
				emptyView.setVisibility(View.VISIBLE);

			buscaAvancada.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buscaAvancada.getLayoutParams();
					params.topMargin -= height;
					buscaAvancada.setVisibility(View.INVISIBLE);
					buscaAvancada.setLayoutParams(params);
				}
			});

			AnimationSet animationSet = new AnimationSet(true);

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, -height);
			translate.setDuration(300);
			animationSet.addAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(1, 0);
			alpha.setDuration(300);
			animationSet.addAnimation(alpha);

			buscaAvancada.startAnimation(animationSet);
		}
	}

	@Click(R.id.btnPesquisar)
	protected void onClickPesquisarAvancado() {
		if (mAdapter != null)
			mProdutosQuantidade = mAdapter.getProdutosQuantidade();

		isFiltrado = true;
		service.listarProdutos(getUsuarioLogado().getCode(), txtFornecedor.getTextValue(), txtAtributo.getTextValue(),
				txtCategoria.getTextValue(), this);

		hideSoftKeyboard(txtFornecedor);
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(true);
			mMenu.setGroupVisible(R.id.groupAvancado, false);
			if (buscaAvancada.getVisibility() == View.VISIBLE)
				exibirBuscaAvancada();
			return true;
		}

		return false;
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			mMenu.findItem(R.id.action_add).setVisible(false);
			mMenu.setGroupVisible(R.id.groupAvancado, true);

			return true;
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		search(newText);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		search(query);
		return false;
	}

	private void search(String query) {
		if (mAdapter != null) {
			mAdapter.getFilter().filter(query);
			isFiltrado = true;
			verificarHeader();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == 1)
			listarProdutos();

		super.onActivityResult(requestCode, resultCode, data);
	}

	protected void verificarHeader() {
		if (isFiltrado) {
			headerResultado.setVisibility(View.VISIBLE);
			txtQuantidadeProdutos.setText(String.valueOf(mAdapter.getCount()));
		} else
			headerResultado.setVisibility(View.GONE);
	}

	@Click({ R.id.btnLimpar, R.id.btnCancelar })
	protected void onClickLimparFiltro(View v) {

		mProdutosQuantidade = mAdapter.getProdutosQuantidade();

		mSearchView.setQuery("", false);
		txtFornecedor.getEditText().setText("");
		txtAtributo.getEditText().setText("");
		txtCategoria.getEditText().setText("");

		isFiltrado = false;
		listarProdutos();
	}

	@Override
	public void notifyPedidoDataSetChanged() {
		verificarHeader();
	}

	@Override
	public void adicionarQuantidade(UUID id, int quantidade) {

		mAdapter.alterarProduto(id, quantidade, null);

	}

}

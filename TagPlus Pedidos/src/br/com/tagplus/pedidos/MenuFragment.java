package br.com.tagplus.pedidos;

import java.util.ArrayList;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.ui.DialogListFragment;
import br.com.tagplus.pedidos.ui.DialogListFragment.ListClickListener;
import br.com.tagplus.pedidos.ui.NavMenuItem;
import br.com.tagplus.pedidos.ui.SessionHelper;
import br.com.tagplus.pedidos.ui.adapter.AdapterMenu;
import br.com.tagplus.pedidos.ui.adapter.SectionAdapterMenu;

@EFragment(R.layout.fragment_menu)
public class MenuFragment extends Fragment implements OnItemClickListener {

	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	private SectionAdapterMenu mAdapter;
	private MenuListener mListener;
	private ArrayList<NavMenuItem> mData;
	private Perfil mPerfil;

	@ViewById(R.id.drawer_list)
	ListView mDrawerList;

	@ViewById
	TextView txtNomePerfil;

	@ViewById
	TextView txtNomeEmpresa;

	@Bean
	SessionHelper helper;

	@ViewById
	protected ImageView imgExpand;

	private int mCurrentSelectedPosition = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
		}

	}

	@AfterViews
	protected void load() {
		criarMenu();
		configurarUsuario();
	}

	public void init() {
		selectItem(mCurrentSelectedPosition);
	}

	protected void criarMenu() {
		mData = new ArrayList<NavMenuItem>();

		mData.add(new NavMenuItem(1, getString(R.string.menu_pedidos), getActivity(), R.drawable.ic_cart_remove_blue));
		mData.add(new NavMenuItem(2, getString(R.string.menu_produtos), getActivity(), R.drawable.ic_product));
		mData.add(new NavMenuItem(3, getString(R.string.menu_clientes), getActivity(), R.drawable.ic_address_book));
		mData.add(new NavMenuItem(4, getString(R.string.menu_fornecedores), getActivity(), R.drawable.ic_provider));

		String ferramentas = getString(R.string.menu_ferramentas);
		NavMenuItem backup = new NavMenuItem(6, getString(R.string.menu_backup), getActivity(), R.drawable.ic_file_download);
		backup.setSection(ferramentas);
		mData.add(backup);

		NavMenuItem sincronizar = new NavMenuItem(6, getString(R.string.menu_sincronizar), getActivity(), R.drawable.ic_loop2);
		sincronizar.setSection(ferramentas);
		mData.add(sincronizar);

		String maisOpcoes = getString(R.string.menu_mais_opcoes);
		NavMenuItem configuracoes = new NavMenuItem(7, getString(R.string.menu_configuracoes), getActivity(), R.drawable.ic_cog2);
		configuracoes.setSection(maisOpcoes);
		mData.add(configuracoes);

		mAdapter = new SectionAdapterMenu(getActivity(), new AdapterMenu(getActivity(), mData));
		mDrawerList.setAdapter(mAdapter);
		mDrawerList.setOnItemClickListener(this);

	}

	protected void configurarUsuario() {
		mPerfil = helper.getUsuario();
		if (mPerfil != null) {
			txtNomePerfil.setText(mPerfil.getNome());
			if (mPerfil.isVinculado()) {
				txtNomeEmpresa.setText(mPerfil.getSistema());
				txtNomeEmpresa.setVisibility(View.VISIBLE);
			} else
				txtNomeEmpresa.setVisibility(View.GONE);
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		selectItem(position);

	}

	public void selectItem(int position) {
		mCurrentSelectedPosition = position;

		if (mAdapter != null) {
			NavMenuItem item = (NavMenuItem) mAdapter.getItem(position);

			if (item.getCode() != 6) {
				mAdapter.setSelectedItem(position);
				mAdapter.notifyDataSetChanged();
			}

			if (mListener != null) {
				mListener.onMenuSelected(item.getCode(), item.getName());
			}
		}

	}

	@Click(R.id.imgExpand)
	protected void onClickExpand(View v) {
		DialogListFragment fragment = new DialogListFragment();
		fragment.setTitle(mPerfil.getNome());

		final ArrayList<String> list = new ArrayList<String>();
		if (!mPerfil.isVinculado())
			list.add(getString(R.string.vincular_tagplus));
		list.add(getString(R.string.deslogar));

		fragment.setList(list.toArray(new String[] {}));
		fragment.setListener(new ListClickListener() {

			@Override
			public void onClick(int which) {
				String k = list.get(which);
				if (k.equalsIgnoreCase(getString(R.string.deslogar)))
					mListener.onMenuSelected(-1, k);
				if (k.equalsIgnoreCase(getString(R.string.vincular_tagplus))) {
					ContaTagPlusActivity_.intent(getActivity()).startForResult(999);
				}
			}
		});

		fragment.show(getFragmentManager(), DialogListFragment.TAG);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mListener = (MenuListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement MenuListener.");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	public interface MenuListener {
		public void onMenuSelected(int codigoMenu, String nomeMenu);
	}

}

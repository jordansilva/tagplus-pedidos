package br.com.tagplus.pedidos;

import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.PerfilService;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.SessionHelper;
import br.com.tagplus.pedidos.ui.adapter.TwoLineAdapter;

import com.crashlytics.android.Crashlytics;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements OnEditorActionListener, OnItemSelectedListener {

	@Bean
	SessionHelper session;

	@Bean
	PerfilService service;

	@ViewById
	Button btnCriarPrimeiroPerfil;

	@ViewById
	Button btnCriarPerfil;

	@ViewById
	Button btnEntrarPerfilExistente;

	@ViewById
	Button btnEntrarPerfil;

	@ViewById
	Button btnCriarNovoPerfil;

	@ViewById
	LinearLayout lnlEntrarPerfil;

	@ViewById
	LinearLayout lnlCriarPerfil;

	@ViewById
	EditText txtCriarPerfil;

	@ViewById
	Spinner spinnerNomePerfil;

	@ViewById
	EditText txtSenhaPerfil;

	@ViewById(R.id.logoTagPlus)
	ImageView imgLogoTagPlus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	protected void configurarUi() {
		if (!verificarUltimoLogin()) {
			listarPerfis();
			startScreen();
		}
	}

	protected void startScreen() {
		Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.translate);
		animTranslate.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationEnd(Animation arg0) {

				validarPrimeiraTela();
			}
		});
		imgLogoTagPlus.startAnimation(animTranslate);
	}

	protected boolean verificarUltimoLogin() {

		String codigoUltimoUsuario = session.getPrefs().codigoUltimoUsuario().get();
		if (codigoUltimoUsuario != null && !codigoUltimoUsuario.isEmpty()) {
			logar(codigoUltimoUsuario);
			return true;
		}

		return false;
	}

	protected void validarPrimeiraTela() {
		boolean isFirstTime = session.getPrefs().isFirstTime().get();

		txtCriarPerfil.setOnEditorActionListener(this);
		txtSenhaPerfil.setOnEditorActionListener(this);

		if (!isFirstTime) {
			btnCriarPrimeiroPerfil.setVisibility(View.GONE);
			btnCriarNovoPerfil.setVisibility(View.VISIBLE);
			btnEntrarPerfilExistente.setVisibility(View.GONE);
			lnlEntrarPerfil.setVisibility(View.VISIBLE);
		} else
			btnCriarPrimeiroPerfil.setVisibility(View.VISIBLE);
	}

	// Mostra a caixa para criar novo perfil
	@Click({ R.id.btnCriarPrimeiroPerfil, R.id.btnCriarNovoPerfil })
	protected void onClickNovoPerfil(View v) {
		btnCriarPrimeiroPerfil.setVisibility(View.GONE);
		lnlEntrarPerfil.setVisibility(View.GONE);
		lnlCriarPerfil.setVisibility(View.VISIBLE);
		btnCriarNovoPerfil.setVisibility(View.GONE);
		btnEntrarPerfilExistente.setVisibility(View.VISIBLE);
		txtCriarPerfil.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(txtCriarPerfil, InputMethodManager.SHOW_IMPLICIT);
	}

	@Click({ R.id.btnEntrarPerfilExistente })
	protected void onClickEntrarPerfil(View v) {
		btnEntrarPerfilExistente.setVisibility(View.GONE);
		validarPrimeiraTela();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(txtCriarPerfil, InputMethodManager.SHOW_IMPLICIT);
	}

	// Criar novo Perfil
	@Click(R.id.btnCriarPerfil)
	protected void onClickCriarPerfil(View v) {
		if (validarCampos()) {
			salvarPerfil();
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
		}
	}

	private boolean validarCampos() {
		if (lnlCriarPerfil.getVisibility() == View.VISIBLE) {
			String perfil = txtCriarPerfil.getText().toString();
			if (perfil == null || perfil.trim().isEmpty()) {
				txtCriarPerfil.setError(getString(R.string.campo_obrigatorio));
				txtCriarPerfil.requestFocus();
				return false;
			}
		}

		if (lnlEntrarPerfil.getVisibility() == View.VISIBLE) {
			Perfil perfil = (Perfil) spinnerNomePerfil.getSelectedItem();
			if (perfil.isVinculado()) {
				if (!perfil.getSenha().equals(txtSenhaPerfil.getEditableText().toString())) {
					txtSenhaPerfil.setError(getString(R.string.senha_invalida));
					txtSenhaPerfil.requestFocus();
					return false;
				}
			}

			if (!StringHelper.isNullOrEmpty(perfil.getSistema()) && perfil.getSistema().equalsIgnoreCase("-1")) {
				Toast.makeText(this, R.string.selecione_perfil, Toast.LENGTH_LONG).show();
				return false;
			}
		}

		return true;
	}

	@Background
	protected void salvarPerfil() {
		Perfil perfil = new Perfil();
		perfil.setNome(txtCriarPerfil.getText().toString().trim());
		service.salvar(perfil, new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(RequestResult result) {
				logarPerfil(result);
			}
		});
	}

	@UiThread
	protected void logarPerfil(RequestResult result) {
		if (!result.isError() && result.getData() != null) {
			Perfil perfil = (Perfil) result.getData();

			session.getPrefs().edit().isFirstTime().put(false).apply();
			session.getPrefs().edit().codigoUltimoUsuario().put(perfil.getNome()).apply();

			session.addUsuario(perfil);

			Crashlytics.setUserIdentifier(perfil.getCode().toString());
			Crashlytics.setUserName(perfil.getNome());

			DrawerActivity_.intent(this).start();
			finish();
		} else {
			int errorMessage = R.string.erro_generico;
			if (result.getCode() == 999) {
				errorMessage = R.string.error_perfil_ja_criado;
				txtCriarPerfil.setText("");
			} else
				validarPrimeiraTela();

			Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
		}
	}

	@Background
	protected void listarPerfis() {
		service.listarPerfis(new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(RequestResult result) {
				carregarPerfis(result);
			}
		});
	}

	@UiThread
	@SuppressWarnings("unchecked")
	protected void carregarPerfis(RequestResult result) {
		if (!result.isError() && result.getData() != null) {
			List<Perfil> items = (List<Perfil>) result.getData();
			Perfil p = new Perfil();
			p.setNome(getString(R.string.selecione_perfil));
			p.setSistema("-1");
			items.add(0, p);
			TwoLineAdapter adapter = new TwoLineAdapter(this, items);
			adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
			spinnerNomePerfil.setAdapter(adapter);

			if (items.size() == 2)
				spinnerNomePerfil.setSelection(1);

			spinnerNomePerfil.setOnItemSelectedListener(this);
		}
	}

	@Click(R.id.btnEntrarPerfil)
	protected void onClickLogar(View v) {
		if (validarCampos()) {
			Perfil perfil = (Perfil) spinnerNomePerfil.getSelectedItem();
			session.getPrefs().edit().isFirstTime().put(false).apply();
			session.getPrefs().edit().codigoUltimoUsuario().put(perfil.getNome()).apply();
			session.addUsuario(perfil);

			// service.gerarData(perfil);

			Crashlytics.setUserIdentifier(perfil.getCode().toString());
			Crashlytics.setUserName(perfil.getNome());

			DrawerActivity_.intent(this).start();
			finish();
		}
	}

	@Background
	protected void logar(String perfil) {
		service.logar(perfil, new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(RequestResult result) {
				logarPerfil(result);
			}
		});
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE) {
			if (v.getId() == R.id.txtCriarPerfil)
				onClickCriarPerfil(v);
			if (v.getId() == R.id.txtSenhaPerfil)
				onClickLogar(v);
			return true;
		}
		return false;
	}

	@Override
	public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
		Perfil p = (Perfil) adapter.getItemAtPosition(position);
		txtSenhaPerfil.setVisibility(p.isVinculado() ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapter) {
		txtSenhaPerfil.setVisibility(View.GONE);
	}

}

package br.com.tagplus.pedidos;

import java.util.ArrayList;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.tagplus.pedidos.business.domain.Atributo;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment;
import br.com.tagplus.pedidos.ui.DialogDeleteFragment.YesNoListener;
import br.com.tagplus.pedidos.ui.StretchedListView;
import br.com.tagplus.pedidos.ui.adapter.AdapterAtributo;
import br.com.tagplus.pedidos.ui.adapter.AdapterAtributo.AdapterAtributoListener;
import br.com.tagplus.pedidos.ui.listener.ProdutoListener;
import br.com.tagplus.pedidos.ui.listener.UpdateFragmentListener;

import com.iangclifton.android.floatlabel.FloatLabel;

@EFragment(R.layout.fragment_produto_atributo)
public class ProdutoAtributoFragment extends BaseFragment implements AdapterAtributoListener, UpdateFragmentListener {

	@ViewById
	FloatLabel txtDescricao;

	@ViewById
	FloatLabel txtConteudo;

	@ViewById
	Button btnAdicionar;

	@ViewById
	TextView txtAtributos;

	@ViewById
	StretchedListView listView;

	Produto mProduto;
	ProdutoListener listener;
	AdapterAtributo adapter;
	ArrayList<Atributo> atributos;
	private String txtDescricaoSaved;
	private String txtConteudoSaved;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("txtDescricao"))
				txtDescricaoSaved = savedInstanceState.getString("txtDescricao");

			if (savedInstanceState.containsKey("txtConteudo"))
				txtConteudoSaved = savedInstanceState.getString("txtConteudo");
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	protected void load() {
		super.load();
		atributos = new ArrayList<Atributo>();

		if (mProduto == null)
			mProduto = listener.getProduto();

		if (mProduto != null)
			carregarCampos();
	}

	@UiThread
	protected void carregarCampos() {
		atributos = (ArrayList<Atributo>) mProduto.getAtributos();
		adapter = new AdapterAtributo(getActivity(), atributos, this);
		listView.setAdapter(adapter);

		if (atributos.size() > 0)
			txtAtributos.setVisibility(View.VISIBLE);

		if (!StringHelper.isNullOrEmpty(txtDescricaoSaved))
			txtDescricao.setText(txtDescricaoSaved);

		if (!StringHelper.isNullOrEmpty(txtConteudoSaved))
			txtConteudo.setText(txtConteudoSaved);
	}

	@Click(R.id.btnAdicionar)
	protected void adicionarAtributo(View v) {
		if (validar()) {
			atributos.add(new Atributo(txtDescricao.getTextValue(), txtConteudo.getTextValue()));

			txtDescricao.getEditText().getText().clear();
			txtConteudo.getEditText().getText().clear();
			hideSoftKeyboard(v);

			if (adapter == null)
				carregarCampos();
			else {
				adapter.notifyDataSetChanged();
				txtAtributos.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public boolean validar() {
		boolean isValid = true;

		if (StringHelper.isNullOrEmpty(txtConteudo.getTextValue())) {
			isValid = false;
			txtConteudo.requestFocus();
			txtConteudo.getEditText().setError(getString(R.string.campo_obrigatorio));
		}

		if (StringHelper.isNullOrEmpty(txtDescricao.getTextValue())) {
			isValid = false;
			txtDescricao.requestFocus();
			txtDescricao.getEditText().setError(getString(R.string.campo_obrigatorio));
		}

		return isValid;
	}

	@Override
	public void onDelete(int position, final Atributo a) {

		DialogDeleteFragment dialog = new DialogDeleteFragment();
		dialog.setTitleId(R.string.dialog_excluir_atributo_title);
		dialog.setMessage(getString(R.string.dialog_excluir_atributo));
		dialog.setListener(new YesNoListener() {

			@Override
			public void onYes() {
				adapter.remove(a);
				atributos.remove(a);
			}

			@Override
			public void onNo() {
				// TODO Auto-generated method stub

			}
		});
		dialog.show(getFragmentManager(), DialogDeleteFragment.TAG);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listener = (ProdutoListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement ProdutoListener.");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		update();
		txtDescricaoSaved = txtDescricao.getTextValue();
		txtConteudoSaved = txtConteudo.getTextValue();
	}

	@Override
	public void update() {
		listener.updateAtributos(adapter.getItems());
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;

		int id = item.getItemId();
		if (id == R.id.action_save) {
			try {
				update();
				listener.salvarProduto();
				result = false;
			} catch (Exception ex) {
				result = true;
			}
		}

		return result;
	}

}

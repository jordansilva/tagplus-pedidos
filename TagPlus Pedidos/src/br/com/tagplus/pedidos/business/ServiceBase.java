package br.com.tagplus.pedidos.business;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.content.Context;

@EBean
public abstract class ServiceBase {

	@RootContext
	protected Context context;

	protected SimpleDateFormat sdf;

	public ServiceBase() {
		this.sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
		this.sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

}

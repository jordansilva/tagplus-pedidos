package br.com.tagplus.pedidos.business;

public interface BusinessListener {
	 void onPreInit();

	 void onPostExecute(RequestResult result);
}

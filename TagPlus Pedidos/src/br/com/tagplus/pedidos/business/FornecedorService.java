package br.com.tagplus.pedidos.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import br.com.tagplus.pedidos.business.domain.Cidade;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.business.domain.TagPlusFornecedor;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.repository.CidadeRepository;
import br.com.tagplus.pedidos.repository.FornecedorRepository;
import br.com.tagplus.pedidos.services.CustomErrorHandler;
import br.com.tagplus.pedidos.services.webservice.HttpBasicAuthenticatorInterceptor;
import br.com.tagplus.pedidos.services.webservice.WSSincronizacao;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@EBean
public class FornecedorService extends ServiceBase {

	@RestService
	WSSincronizacao wsSincronizacao;

	@Bean
	CustomErrorHandler errorHandler;

	@AfterViews
	protected void init() {
		wsSincronizacao.setRestErrorHandler(errorHandler);
	}

	protected void addKey(String key) {
		HttpBasicAuthenticatorInterceptor auth = new HttpBasicAuthenticatorInterceptor(key);
		ArrayList<ClientHttpRequestInterceptor> interceptors = (ArrayList<ClientHttpRequestInterceptor>) wsSincronizacao.getRestTemplate()
				.getInterceptors();
		if (interceptors == null)
			interceptors = new ArrayList<ClientHttpRequestInterceptor>();

		interceptors.add(auth);
		wsSincronizacao.getRestTemplate().setInterceptors(interceptors);
	}

	public void getFornecedor(UUID codigo, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			FornecedorRepository repository = new FornecedorRepository(context);
			try {
				result.setData(repository.get(codigo));
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarFornecedores(UUID codigoPerfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			FornecedorRepository repository = new FornecedorRepository(context);
			try {
				ArrayList<Fornecedor> lista = (ArrayList<Fornecedor>) repository.listar(codigoPerfil);
				result.setData(lista);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void salvar(UUID codigoPerfil, Fornecedor fornecedor, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();

			Perfil p = new Perfil();
			p.setCode(codigoPerfil);
			fornecedor.setPerfil(p);
			fornecedor.setUltimaAlteracao(DateTime.now());

			try {
				FornecedorRepository repository = new FornecedorRepository(context);
				repository.save(fornecedor);
				listener.onPostExecute(new RequestResult(0, null, false, fornecedor));
			} catch (SQLException e) {
				listener.onPostExecute(new RequestResult(-1, e.getMessage(), true));
			}
		}
	}

	/*
	 * FORNECEDORES
	 */
	public void sincronizar(Perfil perfil) throws Exception {

		SharedPreferences preferences = context.getSharedPreferences(perfil.getCode().toString(), Context.MODE_PRIVATE);
		Editor editor = preferences.edit();

		long dataRemover = remover(perfil, preferences.getLong("sincronizacaoFornecedorRemover", 0));
		editor.putLong("sincronizacaoFornecedorRemover", dataRemover).apply();
		editor.commit();

		long dataEnviarBuscar = preferences.getLong("sincronizacaoFornecedorEnviarBuscar", 0);
		enviar(perfil, dataEnviarBuscar);

		long dataBuscar = buscar(perfil, dataEnviarBuscar);
		editor.putLong("sincronizacaoFornecedorEnviarBuscar", dataBuscar).apply();
		editor.commit();
	}

	/**
	 * Removendo itens excluídos do servidor no cliente
	 * 
	 * @param perfil
	 *            Perfil do Usuário para excluir os itens
	 * @throws Exception
	 */
	private long remover(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		addKey(perfil.getKey());

		if (data > 0)
			map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

		JsonArray items = wsSincronizacao.getFornecedoresDeletados(perfil.getSistema(), map);
		dataFinal = DateTime.now().getMillis();

		FornecedorRepository repository = new FornecedorRepository(context);
		for (JsonElement jsonElement : items) {
			String id = jsonElement.getAsJsonObject().get("id").getAsString();
			repository.deletePorCodigoExterno(perfil.getCode(), id);
		}

		return dataFinal;
	}

	/**
	 * Enviando fornecedores atualizados/criados no cliente para o servidor
	 * 
	 * @param perfil
	 * @throws Exception
	 */
	private long enviar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {
			DateTime dataInicial = null;
			if (data > 0)
				dataInicial = new DateTime(data);

			FornecedorRepository repository = new FornecedorRepository(context);
			Collection<Fornecedor> fornecedores = repository.listarPorData(perfil.getCode(), dataInicial);

			ArrayList<JsonObject> array = new ArrayList<JsonObject>();

			for (Fornecedor f : fornecedores) {

				JsonObject jsonFornecedor = new JsonObject();

				jsonFornecedor.addProperty("nome", f.getNome());
				jsonFornecedor.addProperty("cod_controle_fornecedor", f.getCode().toString());
				jsonFornecedor.addProperty("observacao", f.getObservacao());
				jsonFornecedor.addProperty("telefone", f.getTelefone());
				jsonFornecedor.addProperty("email", f.getEmail());

				// Bug fix: 1
				if (f.getUltimaAlteracao() == null)
					f.setUltimaAlteracao(DateTime.now());

				jsonFornecedor.addProperty("data_alteracao", sdf.format(f.getUltimaAlteracao().toDate()));

				if (f.isPessoaFisica()) {
					jsonFornecedor.addProperty("tipo_fornecedor", "1");
					if (!StringHelper.isNullOrEmpty(f.getCpf()))
						jsonFornecedor.addProperty("documento", f.getCpf().replace("-", "").replace(".", ""));
				} else {
					jsonFornecedor.addProperty("tipo_fornecedor", "2");
					if (!StringHelper.isNullOrEmpty(f.getCnpj()))
						jsonFornecedor.addProperty("documento", f.getCnpj().replace("/", "").replace(".", "").replace("-", ""));
				}

				if (!StringHelper.isNullOrEmpty(f.getCodigoExterno()))
					jsonFornecedor.addProperty("id", f.getCodigoExterno());

				if (!StringHelper.isNullOrEmpty(f.getCep()))
					jsonFornecedor.addProperty("cep", f.getCep());

				if (!StringHelper.isNullOrEmpty(f.getLogradouro()))
					jsonFornecedor.addProperty("logradouro", f.getLogradouro());

				if (!StringHelper.isNullOrEmpty(f.getNumero()))
					jsonFornecedor.addProperty("numero", f.getNumero());

				if (!StringHelper.isNullOrEmpty(f.getComplemento()))
					jsonFornecedor.addProperty("complemento", f.getComplemento());

				if (!StringHelper.isNullOrEmpty(f.getBairro()))
					jsonFornecedor.addProperty("bairro", f.getBairro());

				if (f.getCidade() != null && f.getCidade().getCode() != null)
					jsonFornecedor.addProperty("cod_cidade", f.getCidade().getCodigoCidade());

				array.add(jsonFornecedor);
			}

			if (array != null && array.size() > 0) {
				String json = GsonHelper.getInstance().toJson(array);
				MultiValueMap<String, String> dados = new LinkedMultiValueMap<String, String>();
				dados.add("fornecedores", json);

				addKey(perfil.getKey());
				JsonArray retorno = wsSincronizacao.saveFornecedores(perfil.getSistema(), dados);
				dataFinal = DateTime.now().getMillis();

				for (JsonElement jsonElement : retorno) {
					JsonObject object = jsonElement.getAsJsonObject();
					UUID codigoItem = UUID.fromString(object.get("cod_controle_fornecedor").getAsString());

					Fornecedor f = repository.get(codigoItem);
					f.setCodigoExterno(object.get("id").getAsString());
					repository.save(f);
				}

			}
		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

	/**
	 * Buscando itens do servidor para o cliente
	 * 
	 * @param perfil
	 * @param dataInicial
	 * @throws Exception
	 */
	private long buscar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			addKey(perfil.getKey());

			if (data > 0)
				map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

			TagPlusFornecedor[] fornecedores = wsSincronizacao.getFornecedores(perfil.getSistema(), map);
			DateTime dateFinal = DateTime.now();
			dataFinal = dateFinal.getMillis();

			FornecedorRepository fornecedorRepository = new FornecedorRepository(context);
			CidadeRepository cidadeRepository = new CidadeRepository(context);
			for (TagPlusFornecedor item : fornecedores) {

				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setPerfil(perfil);
				fornecedor.setNome(item.razaoSocial);
				fornecedor.setCpf(item.cpf);
				fornecedor.setCnpj(item.cnpj);
				fornecedor.setCodigoExterno(String.valueOf(item.id));

				if (item.tipo == null)
					item.tipo = "J";

				fornecedor.setPessoaFisica(item.tipo.equalsIgnoreCase("F"));
				fornecedor.setObservacao(item.observacao);

				// Detalhes
				fornecedor.setDetalhes(item.detalhes);

				// Contato
				if (item.telefonePrincipal != null)
					fornecedor.setTelefone(item.telefonePrincipal.descricao);
				if (item.emailPrincipal != null)
					fornecedor.setEmail(item.emailPrincipal.descricao);

				// Endereço
				if (item.enderecoPrincipal != null) {
					fornecedor.setCep(item.enderecoPrincipal.cep);
					fornecedor.setLogradouro(item.enderecoPrincipal.logradouro);
					fornecedor.setNumero(item.enderecoPrincipal.numero);
					fornecedor.setComplemento(item.enderecoPrincipal.complemento);
					fornecedor.setBairro(item.enderecoPrincipal.bairro);
					
					Cidade cidade = cidadeRepository.getCidadePorCodigo(Integer.valueOf(item.enderecoPrincipal.codCidade));
					fornecedor.setCidade(cidade);
				}

				fornecedor.setUltimaAlteracao(dateFinal);
				fornecedorRepository.save(fornecedor);
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

}

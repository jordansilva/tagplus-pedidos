package br.com.tagplus.pedidos.business;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;
import org.springframework.web.client.HttpClientErrorException;

import retrofit.RetrofitError;
import android.content.Context;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.repository.PerfilRepository;
import br.com.tagplus.pedidos.services.CustomErrorHandler;
import br.com.tagplus.pedidos.services.RestToken;
import br.com.tagplus.pedidos.services.ServiceProvider;
import br.com.tagplus.pedidos.services.webservice.WSSincronizacao;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;

@EBean
public class SincronizarService extends ServiceBase {

	private SimpleDateFormat sdf;

	@Bean
	FornecedorService fornecedorService;

	@Bean
	ProdutoService produtoService;

	@Bean
	ClienteService clienteService;

	@Bean
	PedidoService pedidoService;

	@RestService
	WSSincronizacao service;

	@Bean
	CustomErrorHandler customErrorHandler;

	public SincronizarService(Context context) {
		super();

		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	//
	// @AfterInject
	// protected void init() {
	// service.setRestErrorHandler(customErrorHandler);
	//
	// ClientHttpRequestFactory requestFactory =
	// service.getRestTemplate().getRequestFactory();
	// if (requestFactory instanceof SimpleClientHttpRequestFactory) {
	// ((SimpleClientHttpRequestFactory)
	// requestFactory).setConnectTimeout(5000);
	// ((SimpleClientHttpRequestFactory) requestFactory).setReadTimeout(30000);
	// } else if (requestFactory instanceof
	// HttpComponentsClientHttpRequestFactory) {
	// ((HttpComponentsClientHttpRequestFactory)
	// requestFactory).setConnectTimeout(5000);
	// ((HttpComponentsClientHttpRequestFactory)
	// requestFactory).setReadTimeout(30000);
	// }
	// }
	//
	// protected void setKey(String key) {
	// RestTemplate restTemplate = service.getRestTemplate();
	// List<ClientHttpRequestInterceptor> interceptors =
	// restTemplate.getInterceptors();
	// if (interceptors == null)
	// interceptors = new ArrayList<ClientHttpRequestInterceptor>();
	// interceptors.add(new HttpBasicAuthenticatorInterceptor(key));
	// restTemplate.setInterceptors(interceptors);
	// }

	public void vincularConta(final String sistema, final String usuario, final String senha, final Perfil perfil,
			final BusinessListener listener) {

		RequestResult result = new RequestResult();
		if (listener != null) {
			listener.onPreInit();

			try {

				ServiceProvider<RestToken> provider = new ServiceProvider<RestToken>(context, RestToken.class);

				RestToken restToken = provider.get(sistema);

				JsonObject jsonAuth = new JsonObject();
				jsonAuth.addProperty("usuario", usuario);
				jsonAuth.addProperty("senha", senha);

				JsonObject jsonObject = restToken.vincularConta(jsonAuth);

				perfil.setSistema(sistema);
				perfil.setUsuario(usuario);
				perfil.setSenha(senha);
				perfil.setKey(jsonObject.get("token").getAsString());

				PerfilRepository repositorio = new PerfilRepository(context);
				repositorio.save(perfil);

				result.setData(perfil);

			} catch (RetrofitError e) {
				if (e.getResponse() != null) {
					result.setCode(e.getResponse().getStatus());
					result.setMessage(e.getResponse().getReason());
				} else {
					result.setMessage(e.getMessage());
				}
				result.setError(true);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);

		}
	}

	/**
	 * Sincronizar Dados
	 * 
	 * @param perfil
	 *            Perfil a ser sincronizado
	 * @param listener
	 */
	public void sincronizarDados(Perfil perfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			String etapa = "fornecedores";
			try {
				if (perfil.isVinculado()) {
					try {

						DateTime dataInicial = DateTime.now();

						// Sincronizar Fornecedores
						etapa = "fornecedores";
						fornecedorService.sincronizar(perfil);

						// Sincronizar Clientes
						etapa = "clientes";
						clienteService.sincronizar(perfil);

						// Sincronizar Produtos
						etapa = "produtos";
						produtoService.sincronizar(perfil);

						// Sincronizar Pedidos
						etapa = "pedidos";
						pedidoService.sincronizar(perfil);

						PerfilRepository repository = new PerfilRepository(context);
						perfil.setUltimaSincronizacao(dataInicial);
						repository.save(perfil);
						result.setData(perfil);

					} catch (SQLException e) {
						result.setError(true);
						String message = String.format(context.getString(R.string.sincronizacao_error), etapa);
						result.setMessage(message);
					}

				} else {
					result.setError(true);
					result.setMessage(context.getString(R.string.perfil_nao_vinculado));
				}

			} catch (Exception e) {
				if (e instanceof HttpClientErrorException) {
					int code = ((HttpClientErrorException) e).getStatusCode().value();
					result.setCode(code);
				}

				result.setError(true);
				String message = String.format(context.getString(R.string.sincronizacao_error), etapa);
				result.setMessage(message);
				Crashlytics.logException(e);
			}

			listener.onPostExecute(result);
		}
	}

}

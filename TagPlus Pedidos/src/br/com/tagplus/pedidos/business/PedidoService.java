package br.com.tagplus.pedidos.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.FormaPagamento;
import br.com.tagplus.pedidos.business.domain.Pedido;
import br.com.tagplus.pedidos.business.domain.PedidoProduto;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.business.domain.TagPlusPedido;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.repository.ClienteRepository;
import br.com.tagplus.pedidos.repository.PedidoRepository;
import br.com.tagplus.pedidos.repository.ProdutoRepository;
import br.com.tagplus.pedidos.services.CustomErrorHandler;
import br.com.tagplus.pedidos.services.webservice.HttpBasicAuthenticatorInterceptor;
import br.com.tagplus.pedidos.services.webservice.WSSincronizacao;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@EBean
public class PedidoService extends ServiceBase {

	@RestService
	WSSincronizacao wsSincronizacao;

	@Bean
	CustomErrorHandler errorHandler;

	@AfterViews
	protected void init() {
		wsSincronizacao.setRestErrorHandler(errorHandler);
	}

	protected void addKey(String key) {
		HttpBasicAuthenticatorInterceptor auth = new HttpBasicAuthenticatorInterceptor(key);
		ArrayList<ClientHttpRequestInterceptor> interceptors = (ArrayList<ClientHttpRequestInterceptor>) wsSincronizacao.getRestTemplate()
				.getInterceptors();
		if (interceptors == null)
			interceptors = new ArrayList<ClientHttpRequestInterceptor>();

		interceptors.add(auth);
		wsSincronizacao.getRestTemplate().setInterceptors(interceptors);
	}

	public void getPedido(UUID codigo, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			PedidoRepository repository = new PedidoRepository(context);
			try {
				result.setData(repository.get(codigo));
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarPedidos(UUID codigoPerfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			PedidoRepository repository = new PedidoRepository(context);
			try {
				ArrayList<Pedido> items = (ArrayList<Pedido>) repository.listar(codigoPerfil);
				result.setData(items);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void salvar(UUID codigoPerfil, Pedido pedido, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();

			Perfil p = new Perfil();
			p.setCode(codigoPerfil);
			pedido.setPerfil(p);
			pedido.setUltimaAlteracao(DateTime.now());

			try {
				PedidoRepository repository = new PedidoRepository(context);
				repository.save(pedido);
				listener.onPostExecute(new RequestResult(0, null, false, pedido));
			} catch (SQLException e) {
				listener.onPostExecute(new RequestResult(-1, e.getMessage(), true));
			}
		}
	}

	/*
	 * SINCRONIZAÇÃO
	 */
	public void sincronizar(Perfil perfil) throws Exception {

		SharedPreferences preferences = context.getSharedPreferences(perfil.getCode().toString(), Context.MODE_PRIVATE);
		Editor editor = preferences.edit();

		long dataRemover = remover(perfil, preferences.getLong("sincronizacaoPedidoRemover", 0));
		editor.putLong("sincronizacaoPedidoRemover", dataRemover).apply();
		editor.commit();

		long dataEnviarBuscar = preferences.getLong("sincronizacaoPedidoEnviarBuscar", 0);
		enviar(perfil, dataEnviarBuscar);	
		
		long dataBuscar = buscar(perfil, dataEnviarBuscar);
		editor.putLong("sincronizacaoPedidoEnviarBuscar", dataBuscar).apply();
		editor.commit();
	}

	private long remover(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		addKey(perfil.getKey());

		if (data > 0)
			map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

		JsonArray items = wsSincronizacao.getPedidosDeletados(perfil.getSistema(), map);
		dataFinal = DateTime.now().getMillis();

		PedidoRepository repository = new PedidoRepository(context);
		for (JsonElement jsonElement : items) {
			String id = jsonElement.getAsJsonObject().get("id").getAsString();
			repository.deletePorCodigoExterno(perfil.getCode(), id);
		}

		return dataFinal;
	}

	private long buscar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			addKey(perfil.getKey());

			if (data > 0)
				map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

			TagPlusPedido[] items = wsSincronizacao.getPedidos(perfil.getSistema(), map);
			DateTime dateFinal = DateTime.now();
			dataFinal = dateFinal.getMillis();

			PedidoRepository pedidoRepository = new PedidoRepository(context);
			ClienteRepository clienteRepository = new ClienteRepository(context);
			ProdutoRepository produtoRepository = new ProdutoRepository(context);

			DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");

			for (TagPlusPedido item : items) {

				Pedido pedido = new Pedido();
				pedido.setCodigoExterno(item.id);
				pedido.setPerfil(perfil);
				pedido.setDataPedido(format.parseDateTime(item.dataAbertura));

				// Cliente
				if (!StringHelper.isNullOrEmpty(item.idCliente)) {
					Cliente cliente = clienteRepository.getCodigoExterno(perfil.getCode(), item.idCliente);
					pedido.setCliente(cliente);
				}

				int codigoStatus = 0;
				if (item.status.equalsIgnoreCase("A"))
					codigoStatus = Pedido.ABERTO;
				else if (item.status.equalsIgnoreCase("B"))
					codigoStatus = Pedido.CONFIRMADO;
				else if (item.status.equalsIgnoreCase("C"))
					codigoStatus = Pedido.CANCELADO;

				pedido.setCodigoStatus(codigoStatus);

				if (item.produtos != null) {
					ArrayList<PedidoProduto> pedidoProdutos = new ArrayList<PedidoProduto>();
					for (TagPlusPedido.Produto itemProduto : item.produtos) {

						Produto produto = produtoRepository.getCodigoInterno(perfil.getCode(), itemProduto.codigoProduto);

						if (produto == null)
							new Exception("Erro ao criar Pedido! Produto não encontrado.");

						BigDecimal valor = BigDecimal.valueOf(itemProduto.valorUnitario
								- (itemProduto.valorDesconto / itemProduto.quantidade));
						valor = valor.setScale(2, RoundingMode.HALF_EVEN);
						PedidoProduto pedidoProduto = new PedidoProduto(pedido, produto, itemProduto.quantidade, valor);

						pedidoProdutos.add(pedidoProduto);
					}

					pedido.setProdutos(pedidoProdutos);
				}

				Double d = (item.desconto * 100) / pedido.getTotal().doubleValue();
				if (d.isNaN() || d.isInfinite())
					d = 0.0;

				BigDecimal desconto = BigDecimal.valueOf(d);
				desconto = desconto.setScale(2, RoundingMode.HALF_EVEN);
				pedido.setDesconto(desconto.doubleValue());
				pedido.setUltimaAlteracao(dateFinal);

				String observacao = item.observacao;
				try {
					// Observacao && Forma de Pagamento
					String startTerm = "##FORMA PAGAMENTO";
					String endTerm = "###";

					if (item.observacao.contains(startTerm)) {
						ArrayList<FormaPagamento> formasPagamento = new ArrayList<FormaPagamento>();

						int indexOf = item.observacao.indexOf(startTerm);
						int endOf = item.observacao.indexOf(endTerm, indexOf + startTerm.length()) + 3;
						String group = item.observacao.substring(indexOf, endOf);
						String[] split = group.split("\n");

						DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");

						if (split[0].equals(startTerm) && split[split.length - 1].equals(endTerm)) {
							for (int i = 1; i < split.length - 1; i++) {
								FormaPagamento pagamento = new FormaPagamento();
								String[] forma = split[i].split("\\|");
								if (forma[0].contains("TIPO:") && forma[1].contains("VALOR") && forma[2].contains("DATA")) {
									pagamento.setNome(forma[0].replace("TIPO:", "").trim());
									pagamento.setValor(new BigDecimal(forma[1].replace("VALOR:", "").trim()));
									pagamento.setData(formatter.parseDateTime(forma[2].replace("DATA:", "").trim()));
								}
								formasPagamento.add(pagamento);
							}
						}

						pedido.setFormaPagamento(formasPagamento);
						observacao = item.observacao.replace(group, "").trim();

					}
				} catch (Exception ex) {
					Crashlytics.logException(ex);

				}

				pedido.setObservacao(observacao);
				pedido.recalcularValores();
				pedidoRepository.save(pedido);
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

	private long enviar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {

			DateTime dataInicial = null;
			if (data > 0)
				dataInicial = new DateTime(data);

			PedidoRepository repository = new PedidoRepository(context);
			Collection<Pedido> items = repository.listarPorData(perfil.getCode(), dataInicial);

			ArrayList<TagPlusPedido> array = new ArrayList<TagPlusPedido>();

			for (Pedido pedido : items) {

				TagPlusPedido tagPlusPedido = new TagPlusPedido();

				if (!StringHelper.isNullOrEmpty(pedido.getCodigoExterno()))
					tagPlusPedido.id = pedido.getCodigoExterno();

				tagPlusPedido.codigoPedido = pedido.getCode().toString();
				if (pedido.getCliente() != null)
					tagPlusPedido.idCliente = pedido.getCliente().getCodigoExterno();

				tagPlusPedido.total = pedido.getTotal().doubleValue();
				tagPlusPedido.desconto = pedido.getValorDesconto().doubleValue();
				tagPlusPedido.dataAbertura = sdf.format(pedido.getDataPedido().toDate());
				tagPlusPedido.dataAlteracao = sdf.format(pedido.getUltimaAlteracao().toDate());

				String status = "A";
				if (pedido.getCodigoStatus() == Pedido.ABERTO)
					status = "A";
				else if (pedido.getCodigoStatus() == Pedido.CONFIRMADO)
					status = "B";
				else if (pedido.getCodigoStatus() == Pedido.CANCELADO)
					status = "C";

				tagPlusPedido.status = status;
				tagPlusPedido.produtos = new ArrayList<TagPlusPedido.Produto>();

				for (PedidoProduto p : pedido.getProdutos()) {
					TagPlusPedido.Produto tagPlusProduto = tagPlusPedido.new Produto();
					tagPlusProduto.codigoProduto = p.getProduto().getCodigoInterno();
					tagPlusProduto.valorUnitario = p.getValorUnidade().doubleValue();
					

//					BigDecimal desconto = p.getProduto().getValorDoProduto().subtract(p.getValorUnidade());
//					desconto = desconto.setScale(2, RoundingMode.HALF_EVEN);
//					desconto = desconto.multiply(BigDecimal.valueOf(p.getQuantidade()));
//					desconto = desconto.setScale(2, RoundingMode.HALF_EVEN);
					
					tagPlusProduto.valorDesconto = 0; //desconto.doubleValue();
					tagPlusProduto.quantidade = p.getQuantidade();

					BigDecimal total = BigDecimal.valueOf(tagPlusProduto.valorUnitario * tagPlusProduto.quantidade);
					total = total.setScale(2, RoundingMode.HALF_EVEN);
//					total = total.subtract(desconto);
					tagPlusProduto.total = total.doubleValue();
					
					tagPlusPedido.produtos.add(tagPlusProduto);
				}

				// Observação
				String observacao = pedido.getObservacao();

				ArrayList<String> observacaoPagamento = new ArrayList<String>();
				observacaoPagamento.add("##FORMA PAGAMENTO");
				for (FormaPagamento formaPagamento : pedido.getFormaPagamento()) {
					observacaoPagamento.add(String.format("TIPO: %s | VALOR: %s | DATA: %s", formaPagamento.getNome(), formaPagamento
							.getValor().toString(), formaPagamento.getDataFormaPagamento()));
				}
				observacaoPagamento.add("###");

				if (observacaoPagamento.size() > 2) {
					if (!StringHelper.isNullOrEmpty(observacao))
						observacao += "\n\n";
					observacao += StringUtils.arrayToDelimitedString(observacaoPagamento.toArray(new String[] {}), "\n");
				}

				tagPlusPedido.observacao = observacao;

				array.add(tagPlusPedido);
			}

			if (array != null && array.size() > 0) {

				String json = GsonHelper.getInstance().toJson(array);
				MultiValueMap<String, String> dados = new LinkedMultiValueMap<String, String>();
				dados.add("pedidos", json);

				addKey(perfil.getKey());
				JsonArray retorno = wsSincronizacao.savePedidos(perfil.getSistema(), dados);
				dataFinal = DateTime.now().getMillis();

				for (JsonElement jsonElement : retorno) {
					JsonObject object = jsonElement.getAsJsonObject();
					UUID codigoItem = UUID.fromString(object.get("codigo_controle_pedido").getAsString());

					Pedido p = repository.get(codigoItem);
					p.setCodigoExterno(object.get("id").getAsString());
					repository.save(p);
				}
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}
}

package br.com.tagplus.pedidos.business;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.UUID;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.time.DateTime;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.business.domain.TagPlusProduto;
import br.com.tagplus.pedidos.helper.FileHelper;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.repository.FornecedorRepository;
import br.com.tagplus.pedidos.repository.ProdutoRepository;
import br.com.tagplus.pedidos.services.CustomErrorHandler;
import br.com.tagplus.pedidos.services.webservice.HttpBasicAuthenticatorInterceptor;
import br.com.tagplus.pedidos.services.webservice.WSSincronizacao;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

@EBean
public class ProdutoService extends ServiceBase {

	@RestService
	WSSincronizacao wsSincronizacao;

	@Bean
	CustomErrorHandler errorHandler;

	@AfterViews
	protected void init() {
		wsSincronizacao.setRestErrorHandler(errorHandler);
	}

	protected void addKey(String key) {
		HttpBasicAuthenticatorInterceptor auth = new HttpBasicAuthenticatorInterceptor(key);
		ArrayList<ClientHttpRequestInterceptor> interceptors = (ArrayList<ClientHttpRequestInterceptor>) wsSincronizacao.getRestTemplate()
				.getInterceptors();
		if (interceptors == null)
			interceptors = new ArrayList<ClientHttpRequestInterceptor>();

		interceptors.add(auth);
		wsSincronizacao.getRestTemplate().setInterceptors(interceptors);
	}

	public void getProduto(UUID codigoProduto, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			ProdutoRepository repository = new ProdutoRepository(context);
			try {
				result.setData(repository.get(codigoProduto));
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarProdutos(UUID codigoPerfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			ProdutoRepository repository = new ProdutoRepository(context);
			try {
				ArrayList<Produto> produtos = (ArrayList<Produto>) repository.listar(codigoPerfil);
				result.setData(produtos);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarProdutos(UUID codigoPerfil, String fornecedor, String atributos, String categoria, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			ProdutoRepository repository = new ProdutoRepository(context);
			try {
				ArrayList<Produto> produtos = (ArrayList<Produto>) repository.listar(codigoPerfil, fornecedor, atributos, categoria);
				result.setData(produtos);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void salvar(UUID codigoPerfil, Produto produto, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();

			Perfil p = new Perfil();
			p.setCode(codigoPerfil);
			produto.setPerfil(p);
			produto.setUltimaAlteracao(DateTime.now());
			if (produto.getCode() == null)
				produto.setAtivo(true);

			try {
				if (!produto.isImageSincronizada() && produto.getImageBitmap() != null) {
					String imageName = produto.getImage();
					imageName = FileHelper.saveImage(context, produto.getImageBitmap(), imageName);
					produto.setImage(imageName);
				}

				ProdutoRepository repository = new ProdutoRepository(context);
				repository.save(produto);
				listener.onPostExecute(new RequestResult(0, null, false, produto));
			} catch (SQLException e) {
				listener.onPostExecute(new RequestResult(-1, e.getMessage(), true));
			}
		}
	}

	/*
	 * PRODUTOS
	 */
	public void sincronizar(Perfil perfil) throws Exception {

		SharedPreferences preferences = context.getSharedPreferences(perfil.getCode().toString(), Context.MODE_PRIVATE);
		Editor editor = preferences.edit();

		long dataRemover = remover(perfil, preferences.getLong("sincronizacaoProdutoRemover", 0));
		editor.putLong("sincronizacaoProdutoRemover", dataRemover).apply();
		editor.commit();

		long dataEnviarBuscar = preferences.getLong("sincronizacaoProdutoEnviarBuscar", 0);
		enviar(perfil, dataEnviarBuscar);

		uploadFotos(perfil);

		long dataBuscar = buscar(perfil, dataEnviarBuscar);
		editor.putLong("sincronizacaoProdutoEnviarBuscar", dataBuscar).apply();
		editor.commit();
	}

	/**
	 * Sincronizar produtos removidos no servidor
	 * 
	 * @param perfil
	 * @throws Exception
	 */
	private long remover(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		addKey(perfil.getKey());

		if (data > 0)
			map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

		JsonArray items = wsSincronizacao.getProdutosDeletados(perfil.getSistema(), map);
		dataFinal = DateTime.now().getMillis();

		ProdutoRepository repository = new ProdutoRepository(context);
		for (JsonElement jsonElement : items) {
			String id = jsonElement.getAsJsonObject().get("id").getAsString();
			repository.deletePorCodigoExterno(perfil.getCode(), id);
		}

		return dataFinal;
	}

	/**
	 * Sincronizar produtos do servidor
	 * 
	 * @param perfil
	 * @param dataInicial
	 * @throws Exception
	 */
	private long buscar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			addKey(perfil.getKey());

			if (data > 0)
				map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

			TagPlusProduto[] items = wsSincronizacao.getProdutos(perfil.getSistema(), map);
			DateTime dateFinal = DateTime.now();
			dataFinal = dateFinal.getMillis();

			ProdutoRepository repository = new ProdutoRepository(context);
			FornecedorRepository fornecedorRepository = new FornecedorRepository(context);
			for (TagPlusProduto item : items) {

				Produto produto = new Produto();
				produto.setPerfil(perfil);
				produto.setDescricao(item.descricaoProduto);
				produto.setCategoria(item.categoria);
				produto.setQuantidadeEstoque(item.quantidade);
				produto.setUnidadeMedida(item.unidadeSaida);
				produto.setAtributos(item.atributos);
				produto.setCodigoEAN(item.codigoEAN);
				produto.setCodigoInterno(item.codigoSecundario);
				produto.setCodigoExterno(item.id);
				produto.setImageExterna(item.image);
				produto.setValorDoProduto(Money.of(CurrencyUnit.getInstance(Locale.getDefault()), item.valor));
				produto.setObservacao(item.informacaoAdicional);
				produto.setAtivo(true);

				ArrayList<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
				if (item.fornecedores != null) {
					for (br.com.tagplus.pedidos.business.domain.TagPlusProduto.TagPlusFornecedor f : item.fornecedores) {
						Fornecedor fornecedor = fornecedorRepository.getCodigoExterno(perfil.getCode(), f.fornecedorId);
						if (fornecedor != null)
							fornecedores.add(fornecedor);
					}
					produto.setFornecedores(fornecedores);
				}

				produto.setImageSincronizada(true);
				produto.setCodigoExterno(String.valueOf(item.id));
				produto.setUltimaAlteracao(dateFinal);

				repository.save(produto);
				produto = null;
				fornecedores = null;
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

	/**
	 * Enviar produtos para o servidor
	 * 
	 * @param perfil
	 * @throws Exception
	 */
	private long enviar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {

			DateTime dataInicial = null;
			if (data > 0)
				dataInicial = new DateTime(data);

			ProdutoRepository repository = new ProdutoRepository(context);
			Collection<Produto> items = repository.listarPorData(perfil.getCode(), dataInicial);

			ArrayList<TagPlusProduto> array = new ArrayList<TagPlusProduto>();

			for (Produto p : items) {

				TagPlusProduto tagPlusProduto = new TagPlusProduto();
				if (!StringHelper.isNullOrEmpty(p.getCodigoExterno()))
					tagPlusProduto.id = p.getCodigoExterno();

				tagPlusProduto.codigoControleProduto = p.getCode().toString();
				tagPlusProduto.descricaoProduto = p.getDescricao();
				tagPlusProduto.categoria = p.getCategoria();

				tagPlusProduto.unidadeSaida = p.getUnidadeMedida();
				tagPlusProduto.quantidade = p.getQuantidadeEstoque();
				tagPlusProduto.valor = p.getValorDoProduto().doubleValue();
				tagPlusProduto.isAtivo = (p.isAtivo()) ? 1 : 0;
				tagPlusProduto.atributos = p.getAtributos();
				tagPlusProduto.data = p.getUltimaAlteracao();
				tagPlusProduto.informacaoAdicional = p.getObservacao();

				tagPlusProduto.fornecedores = new ArrayList<TagPlusProduto.TagPlusFornecedor>();

				for (Fornecedor f : p.getFornecedores()) {
					if (!StringHelper.isNullOrEmpty(f.getCodigoExterno())) {
						TagPlusProduto.TagPlusFornecedor tagPlusFornecedor = tagPlusProduto.new TagPlusFornecedor();
						tagPlusFornecedor.fornecedorId = f.getCodigoExterno();
						tagPlusProduto.fornecedores.add(tagPlusFornecedor);
					}
				}

				array.add(tagPlusProduto);
			}

			if (array != null && array.size() > 0) {

				String json = GsonHelper.getInstance().toJson(array);
				MultiValueMap<String, String> dados = new LinkedMultiValueMap<String, String>();
				dados.add("produtos", json);

				addKey(perfil.getKey());
				JsonArray retorno = wsSincronizacao.saveProdutos(perfil.getSistema(), dados);
				dataFinal = DateTime.now().getMillis();

				for (JsonElement jsonElement : retorno) {
					JsonObject object = jsonElement.getAsJsonObject();
					UUID codigoItem = UUID.fromString(object.get("cod_controle_produto").getAsString());

					Produto p = repository.get(codigoItem);
					p.setCodigoExterno(object.get("id").getAsString());
					repository.save(p);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

	/**
	 * Sincronizar fotos dos produtos
	 * 
	 * @param perfil
	 */
	public void uploadFotos(Perfil perfil) {
		try {
			addKey(perfil.getKey());

			ProdutoRepository repository = new ProdutoRepository(context);
			Collection<Produto> items = repository.listarFotosUpload(perfil.getCode());

			for (Produto produto : items) {
				try {
					File file = context.getFileStreamPath(produto.getImage());
					if (file.exists()) {
						Bitmap bitmap = Picasso.with(context).load(file).get();
						String bitmapSerialized = FileHelper.bitmapToString(bitmap);

						JsonObject json = new JsonObject();
						json.addProperty("cod_produto", produto.getCodigoInterno());
						json.addProperty("base64_foto", "data:image/png;base64," + bitmapSerialized);

						String result = wsSincronizacao.uploadFotos(perfil.getSistema(), json);
						if (Boolean.valueOf(result)) {
							produto.setImageSincronizada(true);
							repository.save(produto);
						}

					}
				} catch (Exception e) {
					Crashlytics.logException(e);
				} catch (OutOfMemoryError e) {
					Crashlytics.logException(e);
				}
			}
		} catch (SQLException e) {
			Crashlytics.logException(e);
		}
	}
}

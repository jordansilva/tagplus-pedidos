package br.com.tagplus.pedidos.business.domain;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.time.DateTime;
import org.springframework.util.StringUtils;

import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.ui.adapter.IFilterable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "pedido")
public class Pedido extends DomainBase implements IFilterable {

	public static final int ABERTO = 0;
	public static final int CONFIRMADO = 1;
	public static final int CANCELADO = 2;

	@DatabaseField
	private String codigoExterno;

	@DatabaseField(foreign = true, canBeNull = false)
	private Perfil perfil;

	@DatabaseField(foreign = true, canBeNull = true, foreignAutoRefresh = true)
	private Cliente cliente;

	@DatabaseField
	private int codigoStatus;

	@DatabaseField
	private DateTime dataPedido;

	@ForeignCollectionField(eager = true, maxEagerLevel = 3)
	private ForeignCollection<PedidoProduto> pedidoProdutos;

	@DatabaseField
	private BigDecimal subTotal;

	@DatabaseField
	private Double desconto;

	@DatabaseField
	private Double comissaoPercentual;

	@DatabaseField
	private BigDecimal comissaoValor;

	@DatabaseField
	private BigDecimal total;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<FormaPagamento> formaPagamento;

	private Collection<PedidoProduto> cPedidoProdutos;

	private Collection<FormaPagamento> cFormaPagamento;
	
	private int quantidadeItems = 0;

	@DatabaseField
	private String observacao;

	public Pedido() {
		cPedidoProdutos = new ArrayList<PedidoProduto>();
		cFormaPagamento = new ArrayList<FormaPagamento>();
		desconto = Double.valueOf(0);
		comissaoPercentual = Double.valueOf(0);
	}

	public int getCodigoStatus() {
		return codigoStatus;
	}

	public void setCodigoStatus(int codigoStatus) {
		this.codigoStatus = codigoStatus;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Collection<PedidoProduto> getProdutos() {
		if (pedidoProdutos != null && cPedidoProdutos.size() == 0)
			cPedidoProdutos.addAll(pedidoProdutos);

		return cPedidoProdutos;
	}

	public void setProdutos(Collection<PedidoProduto> produtos) {
		this.cPedidoProdutos = produtos;

		if (this.cPedidoProdutos == null || this.cPedidoProdutos.size() == 0) {
			this.pedidoProdutos = null;
			this.cPedidoProdutos = new ArrayList<PedidoProduto>();
		}
	}

	public Collection<FormaPagamento> getFormaPagamento() {
		if (formaPagamento != null && cFormaPagamento.size() == 0)
			cFormaPagamento.addAll(formaPagamento);

		return cFormaPagamento;
	}

	public void setFormaPagamento(Collection<FormaPagamento> formaPagamento) {
		this.cFormaPagamento = formaPagamento;

		if (this.cFormaPagamento == null || this.cFormaPagamento.size() == 0) {
			this.formaPagamento = null;
			this.cFormaPagamento = new ArrayList<FormaPagamento>();
		}
	}

	public BigDecimal valorRestantePagamento() {
		Collection<FormaPagamento> pagamentos = getFormaPagamento();
		BigDecimal valorRestante = getTotal();

		if (pagamentos != null) {
			BigDecimal valorTotal = BigDecimal.ZERO;
			for (FormaPagamento formaPagamento : pagamentos) {
				valorTotal = valorTotal.add(formaPagamento.getValor());
			}

			if (valorRestante.compareTo(valorTotal) < 0)
				valorRestante = BigDecimal.ZERO;
			else
				valorRestante = valorRestante.subtract(valorTotal);
		}

		return valorRestante;
	}

	public DateTime getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(DateTime dataPedido) {
		this.dataPedido = dataPedido;
	}

	public BigDecimal getSubtotal() {
		if (subTotal == null)
			recalcularValores();

		return this.subTotal;
	}

	public CharSequence getTotalPedido() {
		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
		return moneyFormat.format(getTotal());
	}

	public void setSubTotal(BigDecimal valor) {
		this.subTotal = valor;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public BigDecimal getValorDesconto() {
		if (this.desconto == null)
			desconto = Double.valueOf(0);
		
		return this.subTotal.multiply(BigDecimal.valueOf(this.desconto / 100.0));
	}

	public Double getComissaoPercentual() {
		return comissaoPercentual;
	}

	public void setComissaoPercentual(Double comissaoPercentual) {
		this.comissaoPercentual = comissaoPercentual;
	}

	public BigDecimal getComissaoValor() {
		return this.subTotal.multiply(BigDecimal.valueOf(this.desconto / 100.0));
	}

	public void setComissaoValor(Money comissaoValor) {
		this.comissaoValor = comissaoValor.getAmount();
	}

	public BigDecimal getTotal() {
		if (total == null)
			recalcularValores();

		return total;
	}

	public void setTotal(BigDecimal valor) {
		this.total = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@InstantText(viewId = R.id.txtCliente)
	public CharSequence getNomeCliente() {
		if (getCliente() == null)
			return "";
		else
			return getCliente().getNome();
	}

	@InstantText(viewId = R.id.txtValor)
	public CharSequence getValor() {
		return getTotalPedido();
	}

	private String formaPagamentoTexto = null;

	@InstantText(viewId = R.id.txtFormaPagamento)
	public String getFormaPagamentoTexto() {

		if (formaPagamentoTexto == null) {
			ArrayList<String> textos = new ArrayList<String>();
			for (FormaPagamento f : getFormaPagamento()) {
				String nome = f.getNome().toUpperCase(Locale.getDefault()).trim();
				if (!textos.contains(nome))
					textos.add(nome);
			}
			formaPagamentoTexto = StringUtils.arrayToDelimitedString(textos.toArray(new String[] {}), ", ");
		}

		return formaPagamentoTexto;
	}

	@InstantText(viewId = R.id.txtQuantidadeItems, formatStringResId = R.string.produtos_s)
	public String getQuantidadeProdutos() {
		return String.valueOf(quantidadeItems);
		//return String.valueOf(getProdutos().size());
	}
	
	public void setQuantidadeProdutos(int quantidade)
	{
		quantidadeItems = quantidade;
	}

	@InstantText(viewId = R.id.txtDataDia)
	public String getDataDia() {
		return getDataPedido().toString("dd");
	}

	@InstantText(viewId = R.id.txtDataMes)
	public String getDataMes() {
		return getDataPedido().toString("MMM");
	}

	@InstantText(viewId = R.id.txtDataAno)
	public String getDataAno() {
		return getDataPedido().toString("yyyy");
	}

	public String getCodigoExterno() {
		return codigoExterno;
	}

	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}

	public void recalcularValores() {
		setSubTotal(recalcularSubTotal());
		setTotal(recalcularTotal());
	}

	private BigDecimal recalcularSubTotal() {
		Money subTotal = Money.zero(CurrencyUnit.getInstance(Locale.getDefault()));
		for (PedidoProduto p : getProdutos()) {
			BigDecimal valor = p.getValorUnidade().multiply(new BigDecimal(p.getQuantidade()));
			subTotal = subTotal.plus(valor);
		}

		return subTotal.getAmount();
	}

	private BigDecimal recalcularTotal() {
		BigDecimal desconto = this.subTotal.multiply(BigDecimal.valueOf(this.desconto / 100.0));
		BigDecimal comissao = this.subTotal.multiply(BigDecimal.valueOf(this.comissaoPercentual / 100.0));
		return subTotal.subtract(desconto).subtract(comissao);
	}

	@Override
	public boolean filter(CharSequence text) {
		text = text.toString().toUpperCase(Locale.getDefault());
		if (getCliente() != null && getCliente().getNome() != null
				&& getCliente().getNome().toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (getDataPedido() != null && getDataPedido().toString("dd/MM/yyyy").toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (getDataPedido() != null && getDataPedido().toString("MMMM").toUpperCase(Locale.getDefault()).contains(text))
			return true;

		return false;
	}

}

package br.com.tagplus.pedidos.business.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "cidade")
public class Cidade extends DomainBase {

	@DatabaseField
	private int codigoCidade;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 2)
	private Estado estado;

	@DatabaseField
	private String nome;

	public Cidade() {
		// TODO Auto-generated constructor stub
	}

	public int getCodigoCidade() {
		return codigoCidade;
	}

	public void setCodigoCidade(int codigoCidade) {
		this.codigoCidade = codigoCidade;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@InstantText(viewId = android.R.id.text1)
	public String getNome() {
		return nome;
	}

	@InstantText(viewId = android.R.id.text2)
	public String getEstadoNome() {
		return estado.getNome();
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return nome;
	}

}

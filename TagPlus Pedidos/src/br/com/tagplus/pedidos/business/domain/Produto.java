package br.com.tagplus.pedidos.business.domain;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.joda.money.Money;

import android.graphics.Bitmap;
import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.adapter.IFilterable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "produto")
public class Produto extends DomainBase implements IFilterable {

	@DatabaseField(foreign = true, canBeNull = false)
	private Perfil perfil;

	@DatabaseField
	private String descricao;

	@DatabaseField
	private String categoria;

	@DatabaseField
	private String codigoInterno;

	@DatabaseField
	private String codigoEAN;

	@DatabaseField
	private double quantidadeEstoque;

	@DatabaseField
	private String unidadeMedida;

	@DatabaseField
	private BigDecimal valorDoProduto;

	@DatabaseField
	private String observacao;

	@DatabaseField
	private String image;

	@DatabaseField
	private String imageExterna;

	private Bitmap imageBitmap;

	@DatabaseField
	private String codigoExterno;

	@DatabaseField(defaultValue = "true")
	private boolean isAtivo = true;

	@DatabaseField
	private boolean isImageSincronizada;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<Atributo> atributos;

	private Collection<Atributo> cAtributos;

	@ForeignCollectionField(eager = true, maxEagerLevel = 3)
	private ForeignCollection<ProdutoFornecedor> produtoFornecedor;

	private Collection<Fornecedor> cFornecedores;

	public Produto() {
		cAtributos = new ArrayList<Atributo>();
		cFornecedores = new ArrayList<Fornecedor>();
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@InstantText(viewId = R.id.txtNome)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@InstantText(viewId = R.id.txtCodigo)
	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getCodigoEAN() {
		return codigoEAN;
	}

	public void setCodigoEAN(String codigoEAN) {
		this.codigoEAN = codigoEAN;
	}

	public double getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(double quantidade) {
		this.quantidadeEstoque = quantidade;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public BigDecimal getValorDoProduto() {

		if (valorDoProduto != null)
			return valorDoProduto;
		else
			return BigDecimal.ZERO;
	}

	@InstantText(viewId = R.id.txtValor)
	public String getValorProduto() {

		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();

		BigDecimal valor = BigDecimal.ZERO;
		if (valorDoProduto != null)
			valor = valorDoProduto;

		return moneyFormat.format(valor);
	}

	public void setValorDoProduto(Money valorDoProduto) {
		this.valorDoProduto = valorDoProduto.getAmount();
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Collection<Atributo> getAtributos() {

		if (atributos != null && cAtributos.size() == 0)
			cAtributos = new ArrayList<Atributo>(atributos);

		return cAtributos;
	}

	public void setAtributos(Collection<Atributo> atributos) {
		this.cAtributos = atributos;

		if (this.cAtributos == null || this.cAtributos.size() == 0)
			this.atributos = null;
	}

	public Collection<Fornecedor> getFornecedores() {
		if (produtoFornecedor != null && cFornecedores.size() == 0)
			for (ProdutoFornecedor f : produtoFornecedor)
				cFornecedores.add(f.getFornecedor());

		return cFornecedores;
	}

	public void setFornecedores(Collection<Fornecedor> fornecedores) {
		this.cFornecedores = fornecedores;

		if (this.cFornecedores == null || this.cFornecedores.size() == 0)
			this.produtoFornecedor = null;
	}

	public boolean validar() {
		return !StringHelper.isNullOrEmpty(descricao);
	}

	public String getCodigoExterno() {
		return codigoExterno;
	}

	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Bitmap getImageBitmap() {
		return imageBitmap;
	}

	public void setImageBitmap(Bitmap imageBitmap) {
		this.imageBitmap = imageBitmap;
	}

	public String getImageExterna() {
		return imageExterna;
	}

	public void setImageExterna(String imageExterna) {
		this.imageExterna = imageExterna;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public boolean isImageSincronizada() {
		return isImageSincronizada;
	}

	public void setImageSincronizada(boolean isImageSincronizada) {
		this.isImageSincronizada = isImageSincronizada;
	}

	@Override
	public boolean filter(CharSequence text) {
		text = text.toString().toLowerCase(Locale.getDefault());
		text = Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		String descricaoMath = "";
		if (descricao != null) {
			descricaoMath = descricao.toLowerCase(Locale.getDefault());
			descricaoMath = Normalizer.normalize(descricaoMath, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		}

		if (descricaoMath.contains(text))
			return true;
		else if (codigoInterno != null && codigoInterno.toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (codigoEAN != null && codigoEAN.toUpperCase(Locale.getDefault()).contains(text))
			return true;

		return false;
	}

}

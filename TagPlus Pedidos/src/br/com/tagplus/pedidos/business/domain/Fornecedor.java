package br.com.tagplus.pedidos.business.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import br.com.tagplus.pedidos.R;
import br.com.tagplus.pedidos.helper.DocumentValidator;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.ui.adapter.IFilterable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "fornecedor")
public class Fornecedor extends DomainBase implements IFilterable {

	@DatabaseField(foreign = true, canBeNull = false)
	private Perfil perfil;

	@DatabaseField
	private String nome;

	@DatabaseField
	private String email;

	@DatabaseField
	private String telefone;

	@DatabaseField
	private String cpf;

	@DatabaseField
	private String cnpj;

	@DatabaseField
	private boolean isPessoaFisica;

	@DatabaseField
	private boolean isPessoaJuridica;

	@DatabaseField
	private String cep;

	@DatabaseField
	private String logradouro;

	@DatabaseField
	private String numero;

	@DatabaseField
	private String bairro;

	@DatabaseField
	private String complemento;

	@DatabaseField(foreign = true, canBeNull = true, columnName = "cidade_id", foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 3)
	private Cidade cidade;

	@DatabaseField
	private String codigoExterno;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<Detalhe> detalhes;

	@DatabaseField(defaultValue = "true")
	private boolean isAtivo = true;

	// Nota
	@DatabaseField
	private String observacao;

	public Fornecedor() {
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@InstantText(viewId = R.id.txtNome)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@InstantText(viewId = R.id.txtEmail)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@InstantText(viewId = R.id.txtTelefone)
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@InstantText(viewId = R.id.txtLocalizacao)
	public String getEnderecoLista() {
		if (getCidade() != null)
			return String.format("%s (%s)", getCidade().getNome(), getCidade().getEstado().getNome());
		else
			return "";
	}

	@Override
	public String toString() {
		return nome;
	}

	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}

	public String getCodigoExterno() {
		return codigoExterno;
	}

	public void setPessoaFisica(boolean isPessoaFisica) {
		this.isPessoaFisica = isPessoaFisica;
		this.isPessoaJuridica = !isPessoaFisica;
	}

	public void setPessoaJuridica(boolean isPessoaJuridica) {
		this.isPessoaFisica = !isPessoaJuridica;
		this.isPessoaJuridica = isPessoaJuridica;
	}

	public boolean isPessoaFisica() {
		return isPessoaFisica;
	}

	public boolean isPessoaJuridica() {
		return isPessoaJuridica;
	}

	public Collection<Detalhe> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(ArrayList<Detalhe> value) {
		this.detalhes = value;
	}

	@Override
	public boolean filter(CharSequence text) {
		text = text.toString().toUpperCase(Locale.getDefault());
		if (nome != null && nome.toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (cpf != null && cpf.contains(text))
			return true;
		else if (cnpj != null && cnpj.toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (telefone != null && telefone.toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (email != null && email.toUpperCase(Locale.getDefault()).contains(text))
			return true;
		else if (cidade != null && cidade.getNome().toUpperCase(Locale.getDefault()).contains(text))
			return true;

		return false;
	}

	public boolean validar() {
		boolean isValid = true;

		if (!StringHelper.isNullOrEmpty(email)) {
			boolean emailValido = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
			if (!emailValido)
				isValid = false;
		}

		if (StringHelper.isNullOrEmpty(nome))
			isValid = false;

		if (!StringHelper.isNullOrEmpty(cpf) && !DocumentValidator.isCPFValid(cpf))
			isValid = false;
		if (!StringHelper.isNullOrEmpty(cnpj) && !DocumentValidator.isCNPJValid(cnpj))
			isValid = false;

		if (!validarEndereco())
			isValid = false;

		return isValid;
	}

	public boolean validarEndereco() {
		boolean isValid = true;
		boolean isCepNull = StringHelper.isNullOrEmpty(cep);
		boolean isLogradouroNull = StringHelper.isNullOrEmpty(logradouro);
		boolean isNumeroNull = StringHelper.isNullOrEmpty(numero);
		boolean isBairroNull = StringHelper.isNullOrEmpty(bairro);
		boolean isCidadeNull = cidade == null || cidade.getCode() == null;

		// se todos os campos estiverem preenchidos, não entra.
		if (!(isCepNull && isLogradouroNull && isNumeroNull && isBairroNull && isCidadeNull)) {
			if (isCepNull | isLogradouroNull | isNumeroNull | isBairroNull | isCidadeNull) {
				isValid = false;
			}
		}

		if (!isCepNull && cep.replace("-", "").length() < 8)
			isValid = false;

		return isValid;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
}

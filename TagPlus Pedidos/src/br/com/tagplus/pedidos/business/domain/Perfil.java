package br.com.tagplus.pedidos.business.domain;

import org.joda.time.DateTime;

import br.com.tagplus.pedidos.helper.StringHelper;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "perfil")
public class Perfil extends DomainBase {

	@DatabaseField
	private String nome;

	@DatabaseField
	private String key;

	@DatabaseField
	private String sistema;

	@DatabaseField
	private String usuario;

	@DatabaseField
	private String senha;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, foreignAutoCreate = true, maxForeignAutoRefreshLevel = 2, persisted = true)
	private Configuracao configuracao;

	@DatabaseField
	private DateTime ultimaSincronizacao;

	public Perfil() {
		configuracao = new Configuracao();
	}

	@InstantText(viewId = android.R.id.text1)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isVinculado() {
		return !StringHelper.isNullOrEmpty(key);
	}

	public Configuracao getConfiguracao() {
		return configuracao;
	}

	public void setConfiguracao(Configuracao configuracao) {
		this.configuracao = configuracao;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public DateTime getUltimaSincronizacao() {
		return ultimaSincronizacao;
	}

	public void setUltimaSincronizacao(DateTime ultimaSincronizacao) {
		this.ultimaSincronizacao = ultimaSincronizacao;
	}

	@InstantText(viewId = android.R.id.text2)
	public String getUsuarioSistema() {
		if (isVinculado())
			return String.format("%s (%s)", usuario, sistema);
		else
			return "";
	}

	@Override
	public String toString() {
		return getNome();
	}

}

package br.com.tagplus.pedidos.business.domain;

import java.io.Serializable;
import java.util.UUID;

import org.joda.time.DateTime;

import com.j256.ormlite.field.DatabaseField;

@SuppressWarnings("serial")
public class DomainBase implements Serializable {

	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true, canBeNull = false)
	private UUID code;

	@DatabaseField
	private DateTime ultimaAlteracao;

	public DomainBase() {
	}

	public UUID getCode() {
		return code;
	}

	public void setCode(UUID code) {
		this.code = code;
	}

	public DateTime getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(DateTime ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}
}

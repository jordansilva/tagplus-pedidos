package br.com.tagplus.pedidos.business.domain;

import java.math.BigDecimal;
import java.text.NumberFormat;

import br.com.tagplus.pedidos.R;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "pedido_produto")
public class PedidoProduto extends DomainBase {

	@DatabaseField(foreign = true, canBeNull = false)
	private Pedido pedido;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 2)
	private Produto produto;

	@DatabaseField
	private int quantidade;

	@DatabaseField
	private BigDecimal valorUnidade;

	public PedidoProduto() {
	}

	public PedidoProduto(Pedido pedido, Produto produto, int quantidade, BigDecimal valorUnidade) {
		this.pedido = pedido;
		this.produto = produto;
		this.quantidade = quantidade;
		this.valorUnidade = valorUnidade;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	@InstantText(viewId = R.id.txtNome)
	public String getNomeProduto() {
		if (this.produto != null)
			return this.produto.getDescricao();
		else
			return null;
	}

	@InstantText(viewId = R.id.txtQuantidade)
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValorUnidade() {
		return valorUnidade;
	}

	public void setValorUnidade(BigDecimal valorUnidade) {
		this.valorUnidade = valorUnidade;
	}

	@InstantText(viewId = R.id.txtValor)
	public String getValorUnidadeProduto() {

		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();

		BigDecimal valor = BigDecimal.ZERO;
		if (valorUnidade != null)
			valor = valorUnidade;

		return moneyFormat.format(valor);
	}

}

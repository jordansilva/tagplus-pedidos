package br.com.tagplus.pedidos.business.domain;

import java.io.Serializable;
import java.util.Collection;

import org.joda.time.DateTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("serial")
public class TagPlusProduto implements Serializable {

	@Expose
	public String id;

	@Expose
	@SerializedName("ativo")
	public int isAtivo;

	@SerializedName("descricao_produto")
	@Expose
	public String descricaoProduto;

	@SerializedName("cod_controle_produto")
	@Expose
	public String codigoControleProduto;

	@SerializedName("descricao_categoria")
	@Expose
	public String categoria;

	@SerializedName("unidade_saida")
	@Expose
	public String unidadeSaida;

	@SerializedName("qtd")
	@Expose
	public Double quantidade;

	@SerializedName("cod_secundario")
	@Expose
	public String codigoSecundario;

	@SerializedName("cod_barra")
	@Expose
	public String codigoEAN;

	@SerializedName("vr_venda")
	@Expose
	public Double valor;

	@SerializedName("url_foto_principal")
	@Expose
	public String image;
	
	@SerializedName("informacao_adicional")
	@Expose
	public String informacaoAdicional;

	@Expose
	public Double peso;

	@Expose
	public Collection<TagPlusFornecedor> fornecedores;

	@Expose	
	public Collection<Atributo> atributos;

	@Expose
	@SerializedName("data_alteracao")
	public DateTime data;

	public TagPlusProduto() {
		// TODO Auto-generated constructor stub
	}

	public class TagPlusFornecedor implements Serializable {

		@SerializedName("id_fornecedor")
		@Expose
		public String fornecedorId;

	}

}

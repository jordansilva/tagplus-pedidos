package br.com.tagplus.pedidos.business.domain;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("serial")
public class TagPlusFornecedor implements Serializable {

	@Expose
	public int id;

	@SerializedName("razao_social")
	@Expose
	public String razaoSocial;

	@Expose
	public String cnpj;

	@Expose
	public String cpf;

	@Expose
	public String rg;

	@SerializedName("email_principal")
	@Expose
	public Email emailPrincipal;

	@SerializedName("telefone_principal")
	@Expose
	public Telefone telefonePrincipal;

	@SerializedName("endereco_principal")
	@Expose
	public Endereco enderecoPrincipal;

	@SerializedName("tipo")
	@Expose
	public String tipo;

	@SerializedName("informacao_adicional")
	@Expose
	public String observacao;

	@SerializedName("detalhes")
	@Expose
	public ArrayList<Detalhe> detalhes;

	public TagPlusFornecedor() {
		// TODO Auto-generated constructor stub
	}

	public class Telefone implements Serializable {
		@SerializedName("descricao_contato")
		@Expose
		public String descricao;

		public Telefone() {
		}

	}

	public class Email implements Serializable {
		@SerializedName("descricao_contato")
		@Expose
		public String descricao;
	}

	public class Endereco implements Serializable {
		@Expose
		public String logradouro;
		@Expose
		public String numero;
		@Expose
		public String complemento;
		@Expose
		public String bairro;
		@Expose
		public String cidade;
		@Expose
		public String uf;
		@Expose
		public String cep;
		@Expose
		public String pais;
		@Expose
		public String nome;
		@Expose
		public String codPais;
		@SerializedName("cod_cidade")
		@Expose
		public String codCidade;
	}
}

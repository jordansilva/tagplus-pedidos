package br.com.tagplus.pedidos.business.domain;

import br.com.tagplus.pedidos.R;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "atributo")
public class Atributo extends DomainBase {

	@DatabaseField(foreign = true, canBeNull = false)
	private Produto produto;

	@Expose
	@SerializedName("nome_atributo")
	@DatabaseField
	private String descricao;

	@Expose
	@SerializedName("valor_atributo")
	@DatabaseField
	private String conteudo;

	public Atributo() {
	}

	public Atributo(String descricao, String conteudo) {
		this.descricao = descricao;
		this.conteudo = conteudo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	@InstantText(viewId = R.id.txtDescricao)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@InstantText(viewId = R.id.txtConteudo)
	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

}

package br.com.tagplus.pedidos.business.domain;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("serial")
public class TagPlusPedido implements Serializable {

	@Expose
	@SerializedName("id")
	public String id;

	@Expose
	@SerializedName("codigo_controle_pedido")
	public String codigoPedido;

	@Expose
	@SerializedName("id_cliente")
	public String idCliente;

	@Expose
	public String status;

	@Expose
	@SerializedName("data_abertura")
	public String dataAbertura;

	@Expose
	@SerializedName("data_alteracao")
	public String dataAlteracao;

	// @DatabaseField
	// private Double comissaoPercentual;
	//
	// @DatabaseField
	// private BigDecimal comissaoValor;

	@Expose
	@SerializedName("vr_total")
	public double total;

	@Expose
	@SerializedName("vr_desconto")
	public double desconto;

	// @ForeignCollectionField(eager = true)
	// private ForeignCollection<FormaPagamento> formaPagamento;

	@Expose
	@SerializedName("observacoes")
	public String observacao;

	@Expose
	@SerializedName("lancamentos")
	public ArrayList<Produto> produtos;

	public TagPlusPedido() {
		// TODO Auto-generated constructor stub
	}

	public class Produto implements Serializable {

		@Expose
		@SerializedName("codigo_produto")
		public String codigoProduto;

		@Expose
		@SerializedName("qtd")
		public int quantidade;

		@Expose
		@SerializedName("vr_unitario")
		public double valorUnitario;

		@Expose
		@SerializedName("vr_produto")
		public double total;

		@Expose
		@SerializedName("vr_desconto")
		public double valorDesconto;
	}
}

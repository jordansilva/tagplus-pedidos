package br.com.tagplus.pedidos.business.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "configuracao")
public class Configuracao extends DomainBase {

	@DatabaseField
	private int tipoPessoa;

	@DatabaseField
	private int tipoOrdenacaoPedido;

	public Configuracao() {
	}

	public int getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(int tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public int getTipoOrdenacaoPedido() {
		return tipoOrdenacaoPedido;
	}

	public void setTipoOrdenacaoPedido(int tipoOrdenacaoPedido) {
		this.tipoOrdenacaoPedido = tipoOrdenacaoPedido;
	}

}

package br.com.tagplus.pedidos.business.domain;

import java.io.Serializable;

import br.com.tagplus.pedidos.R;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
public class Detalhe implements Serializable {

	@Expose
	@SerializedName("key")
	@DatabaseField
	private String descricao;

	@Expose
	@SerializedName("value")
	@DatabaseField
	private String conteudo;

	public Detalhe() {
	}

	public Detalhe(String descricao, String conteudo) {
		this.descricao = descricao;
		this.conteudo = conteudo;
	}

	@InstantText(viewId = R.id.txtDescricao)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@InstantText(viewId = R.id.txtConteudo)
	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

}

package br.com.tagplus.pedidos.business.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "produto_fornecedor")
public class ProdutoFornecedor extends DomainBase {

	@DatabaseField(foreign = true, canBeNull = false)
	private Produto produto;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 2)
	private Fornecedor fornecedor;

	public ProdutoFornecedor() {
	}

	public ProdutoFornecedor(Produto produto, Fornecedor fornecedor) {
		this.produto = produto;
		this.fornecedor = fornecedor;
	}

	public Produto getProduto() {
		return produto;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

}

package br.com.tagplus.pedidos.business.domain;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.joda.time.DateTime;

import br.com.tagplus.pedidos.R;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mobsandgeeks.adapters.InstantText;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "formaPagamento")
public class FormaPagamento extends DomainBase {

	@DatabaseField
	private String nome;

	@DatabaseField
	private DateTime data;

	@DatabaseField
	private BigDecimal valor;

	@DatabaseField(foreign = true, canBeNull = false)
	private Pedido pedido;

	public FormaPagamento() {
	}

	@InstantText(viewId = R.id.txtNome)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public DateTime getData() {
		return data;
	}

	public void setData(DateTime data) {
		this.data = data;
	}

	@InstantText(viewId = R.id.txtData)
	public String getDataFormaPagamento() {
		return data.toString("dd/MM/yyyy");
	}

	public BigDecimal getValor() {
		if (valor != null)
			return valor;
		else
			return BigDecimal.ZERO;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@InstantText(viewId = R.id.txtValor)
	public String getValorFormaPagamento() {

		NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();

		BigDecimal valorFormaPagamento = BigDecimal.ZERO;
		if (valor != null)
			valorFormaPagamento = valor;

		return moneyFormat.format(valorFormaPagamento);
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

}

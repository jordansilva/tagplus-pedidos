package br.com.tagplus.pedidos.business.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "estado")
public class Estado extends DomainBase {

	@DatabaseField
	private int codigoEstado;

	@DatabaseField
	private String nome;

	@DatabaseField
	private String sigla;

	public Estado() {
		// TODO Auto-generated constructor stub
	}

	public int getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

}

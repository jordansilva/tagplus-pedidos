package br.com.tagplus.pedidos.business;

import java.sql.SQLException;
import java.util.Locale;

import org.androidannotations.annotations.EBean;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import android.content.Context;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.Fornecedor;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.business.domain.Produto;
import br.com.tagplus.pedidos.repository.CidadeRepository;
import br.com.tagplus.pedidos.repository.ClienteRepository;
import br.com.tagplus.pedidos.repository.FornecedorRepository;
import br.com.tagplus.pedidos.repository.PerfilRepository;
import br.com.tagplus.pedidos.repository.ProdutoRepository;

@EBean
public class PerfilService extends ServiceBase {

	public PerfilService(Context context) {
		super();
		this.context = context;
	}

	public void logar(String nome, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			PerfilRepository repository = new PerfilRepository(context);
			try {
				Perfil perfil = repository.logarPerfil(nome);
				result.setData(perfil);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

//	public void gerarData(Perfil perfil) {
//		try {
//			ProdutoRepository produtoRepo = new ProdutoRepository(context);
//			ClienteRepository clienteRepo = new ClienteRepository(context);
//			FornecedorRepository fornecedorRepo = new FornecedorRepository(context);
//
//			for (int i = 0; i < 200; i++) {
//				// Produto
//				Produto produto = new Produto();
//				produto.setPerfil(perfil);
//				produto.setDescricao("Produto " + String.valueOf(i));
//				produto.setValorDoProduto(Money.of(CurrencyUnit.getInstance(Locale.getDefault()), i));
//				produtoRepo.save(produto);
//
//				// Cliente
//				Cliente cliente = new Cliente();
//				cliente.setPerfil(perfil);
//				cliente.setNome("Cliente " + String.valueOf(i));
//				clienteRepo.save(cliente);
//
//				// Fornecedor
//				Fornecedor fornecedor = new Fornecedor();
//				fornecedor.setPerfil(perfil);
//				fornecedor.setNome("Fornecedor " + String.valueOf(i));
//				fornecedorRepo.save(fornecedor);
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//	}

	public void salvar(Perfil perfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			PerfilRepository repository = new PerfilRepository(context);
			try {
				repository.save(perfil);
				result.setData(perfil);
			} catch (SQLException e) {
				result.setError(true);
				result.setCode(e.getErrorCode());
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarPerfis(BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			PerfilRepository repository = new PerfilRepository(context);
			try {
				result.setData(repository.listAll());
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarCidades(BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			CidadeRepository repository = new CidadeRepository(context);

			try {
				result.setData(repository.listAll());
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}
}

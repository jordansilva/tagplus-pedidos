package br.com.tagplus.pedidos.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import br.com.tagplus.pedidos.business.domain.Cidade;
import br.com.tagplus.pedidos.business.domain.Cliente;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.business.domain.TagPlusCliente;
import br.com.tagplus.pedidos.helper.GsonHelper;
import br.com.tagplus.pedidos.helper.StringHelper;
import br.com.tagplus.pedidos.repository.CidadeRepository;
import br.com.tagplus.pedidos.repository.ClienteRepository;
import br.com.tagplus.pedidos.services.CustomErrorHandler;
import br.com.tagplus.pedidos.services.webservice.HttpBasicAuthenticatorInterceptor;
import br.com.tagplus.pedidos.services.webservice.WSSincronizacao;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@EBean
public class ClienteService extends ServiceBase {

	@RestService
	WSSincronizacao wsSincronizacao;

	@Bean
	CustomErrorHandler errorHandler;

	@AfterViews
	protected void init() {
		wsSincronizacao.setRestErrorHandler(errorHandler);
	}

	protected void addKey(String key) {
		HttpBasicAuthenticatorInterceptor auth = new HttpBasicAuthenticatorInterceptor(key);
		ArrayList<ClientHttpRequestInterceptor> interceptors = (ArrayList<ClientHttpRequestInterceptor>) wsSincronizacao.getRestTemplate()
				.getInterceptors();
		if (interceptors == null)
			interceptors = new ArrayList<ClientHttpRequestInterceptor>();

		interceptors.add(auth);
		wsSincronizacao.getRestTemplate().setInterceptors(interceptors);
	}

	public void getCliente(UUID codigo, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			ClienteRepository repository = new ClienteRepository(context);
			try {
				result.setData(repository.get(codigo));
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarClientes(UUID codigoPerfil, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			ClienteRepository repository = new ClienteRepository(context);
			try {
				ArrayList<Cliente> clientes = (ArrayList<Cliente>) repository.listar(codigoPerfil);
				result.setData(clientes);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void salvar(UUID codigoPerfil, Cliente cliente, BusinessListener listener) {
		if (listener != null) {
			listener.onPreInit();

			Perfil p = new Perfil();
			p.setCode(codigoPerfil);
			cliente.setPerfil(p);
			cliente.setUltimaAlteracao(DateTime.now());

			try {
				ClienteRepository repository = new ClienteRepository(context);
				repository.save(cliente);
				listener.onPostExecute(new RequestResult(0, null, false, cliente));
			} catch (SQLException e) {
				listener.onPostExecute(new RequestResult(-1, e.getMessage(), true));
			}
		}
	}

	/*
	 * SINCRONIZAÇÃO
	 */
	public void sincronizar(Perfil perfil) throws Exception {

		SharedPreferences preferences = context.getSharedPreferences(perfil.getCode().toString(), Context.MODE_PRIVATE);
		Editor editor = preferences.edit();

		long dataRemover = remover(perfil, preferences.getLong("sincronizacaoClienteRemover", 0));
		editor.putLong("sincronizacaoClienteRemover", dataRemover).apply();
		editor.commit();

		long dataEnviarBuscar = preferences.getLong("sincronizacaoClienteEnviarBuscar", 0);
		enviar(perfil, dataEnviarBuscar);

		long dataBuscar = buscar(perfil, dataEnviarBuscar);
		editor.putLong("sincronizacaoClienteEnviarBuscar", dataBuscar).apply();
		editor.commit();
	}

	/**
	 * Sincronizar clientes removidos no servidor
	 * 
	 * @param perfil
	 * @throws Exception
	 */
	private long remover(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		addKey(perfil.getKey());

		if (data > 0)
			map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

		JsonArray items = wsSincronizacao.getClientesDeletados(perfil.getSistema(), map);
		dataFinal = DateTime.now().getMillis();

		ClienteRepository repository = new ClienteRepository(context);
		for (JsonElement jsonElement : items) {
			String id = jsonElement.getAsJsonObject().get("id").getAsString();
			repository.deletePorCodigoExterno(perfil.getCode(), id);
		}

		return dataFinal;
	}

	/**
	 * Sincronizar clientes do celular para o servidor
	 * 
	 * @param perfil
	 * @throws Exception
	 */
	private long enviar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {

			DateTime dataInicial = null;
			if (data > 0)
				dataInicial = new DateTime(data);

			ClienteRepository repository = new ClienteRepository(context);
			Collection<Cliente> clientes = repository.listarPorData(perfil.getCode(), dataInicial);

			ArrayList<JsonObject> array = new ArrayList<JsonObject>();

			for (Cliente c : clientes) {

				JsonObject jsonCliente = new JsonObject();

				jsonCliente.addProperty("nome", c.getNome());
				jsonCliente.addProperty("cod_controle_cliente", c.getCode().toString());
				jsonCliente.addProperty("observacao", c.getObservacao());
				jsonCliente.addProperty("telefone", c.getTelefone());
				jsonCliente.addProperty("email", c.getEmail());
				jsonCliente.addProperty("data_alteracao", sdf.format(c.getUltimaAlteracao().toDate()));

				if (c.isPessoaFisica()) {
					jsonCliente.addProperty("tipo_cliente", "1");
					if (!StringHelper.isNullOrEmpty(c.getCpf()))
						jsonCliente.addProperty("documento", c.getCpf().replace("-", "").replace(".", ""));
				} else {
					jsonCliente.addProperty("tipo_cliente", "2");
					if (!StringHelper.isNullOrEmpty(c.getCnpj()))
						jsonCliente.addProperty("documento", c.getCnpj().replace("/", "").replace(".", "").replace("-", ""));
				}

				if (!StringHelper.isNullOrEmpty(c.getCodigoExterno()))
					jsonCliente.addProperty("id", c.getCodigoExterno());

				if (!StringHelper.isNullOrEmpty(c.getCep()))
					jsonCliente.addProperty("cep", c.getCep());

				if (!StringHelper.isNullOrEmpty(c.getLogradouro()))
					jsonCliente.addProperty("logradouro", c.getLogradouro());

				if (!StringHelper.isNullOrEmpty(c.getNumero()))
					jsonCliente.addProperty("numero", c.getNumero());

				if (!StringHelper.isNullOrEmpty(c.getComplemento()))
					jsonCliente.addProperty("complemento", c.getComplemento());

				if (!StringHelper.isNullOrEmpty(c.getBairro()))
					jsonCliente.addProperty("bairro", c.getBairro());

				if (c.getCidade() != null && c.getCidade().getCode() != null)
					jsonCliente.addProperty("cod_cidade", c.getCidade().getCodigoCidade());

				array.add(jsonCliente);
			}

			if (array != null && array.size() > 0) {
				String json = GsonHelper.getInstance().toJson(array);
				MultiValueMap<String, String> dados = new LinkedMultiValueMap<String, String>();
				dados.add("clientes", json);

				addKey(perfil.getKey());
				JsonArray retorno = wsSincronizacao.saveClientes(perfil.getSistema(), dados);
				dataFinal = DateTime.now().getMillis();

				for (JsonElement jsonElement : retorno) {
					JsonObject object = jsonElement.getAsJsonObject();
					UUID codigoItem = UUID.fromString(object.get("cod_controle_cliente").getAsString());

					Cliente c = repository.get(codigoItem);
					c.setCodigoExterno(object.get("id").getAsString());
					repository.save(c);
				}
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}

	/**
	 * Sincronizar clientes do servidor
	 * 
	 * @param perfil
	 * @param dataInicial
	 * @throws Exception
	 */
	private long buscar(Perfil perfil, long data) throws Exception {

		long dataFinal = 0;

		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			addKey(perfil.getKey());

			if (data > 0)
				map.add("data_alteracao", sdf.format(new DateTime(data).toDate()));

			TagPlusCliente[] clientes = wsSincronizacao.getClientes(perfil.getSistema(), map);
			DateTime dateFinal = DateTime.now();
			dataFinal = dateFinal.getMillis();

			ClienteRepository repository = new ClienteRepository(context);
			CidadeRepository cidadeRepository = new CidadeRepository(context);
			for (TagPlusCliente item : clientes) {

				Cliente cliente = new Cliente();
				cliente.setPerfil(perfil);
				cliente.setNome(item.razaoSocial);
				cliente.setCpf(item.cpf);
				cliente.setCnpj(item.cnpj);
				cliente.setCodigoExterno(String.valueOf(item.id));
				cliente.setUltimaAlteracao(dateFinal);
				cliente.setAtivo(true);

				if (item.tipo == null)
					item.tipo = "F";

				cliente.setPessoaFisica(item.tipo.equalsIgnoreCase("F"));
				cliente.setObservacao(item.observacao);

				// Detalhes
				cliente.setDetalhes(item.detalhes);

				// Contato
				if (item.telefonePrincipal != null)
					cliente.setTelefone(item.telefonePrincipal.descricao);
				if (item.emailPrincipal != null)
					cliente.setEmail(item.emailPrincipal.descricao);

				// Endereço
				if (item.enderecoPrincipal != null) {
					cliente.setCep(item.enderecoPrincipal.cep);
					cliente.setLogradouro(item.enderecoPrincipal.logradouro);
					cliente.setNumero(item.enderecoPrincipal.numero);
					cliente.setComplemento(item.enderecoPrincipal.complemento);
					cliente.setBairro(item.enderecoPrincipal.bairro);
					
					Cidade cidade = cidadeRepository.getCidadePorCodigo(Integer.valueOf(item.enderecoPrincipal.codCidade));
					cliente.setCidade(cidade);
				}

				repository.save(cliente);
			}

		} catch (Exception ex) {
			throw ex;
		}

		return dataFinal;
	}
}

package br.com.tagplus.pedidos;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.ui.SessionHelper;

@EActivity
public abstract class BaseActivity extends Activity {

	@Bean
	protected SessionHelper helper;

	// @Bean
	// PerfilService service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	protected void load() {
		configureActionBar();

	}

	public void configureActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayUseLogoEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);

		int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
	}

	public Perfil getUsuarioLogado() {
		return helper.getUsuario();
	}

	@UiThread
	protected void showToast(int textoId) {
		Toast.makeText(this, textoId, Toast.LENGTH_SHORT).show();
	}

	@UiThread
	protected void showToast(String texto) {
		Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void hideSoftKeyboard(View v) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	protected void deslogar() {
		helper.limparSessao();
		finish();
		Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
		startActivity(intent);
	}
}

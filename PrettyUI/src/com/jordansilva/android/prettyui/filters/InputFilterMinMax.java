package com.jordansilva.android.prettyui.filters;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterMinMax implements InputFilter {

	private double min, max;
	private String replaceChars;

	public InputFilterMinMax(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public InputFilterMinMax(String min, String max) {
		this.min = Double.valueOf(min);
		this.max = Double.valueOf(max);
	}

	public void removeChars(String chars) {
		replaceChars = chars;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		try {

			String dest2 = dest.toString();
			String source2 = source.toString();
			if (replaceChars != null && replaceChars.length() > 0) {
				dest2 = dest2.replaceAll("[" + replaceChars + "]", "");
				source2 = source2.replaceAll("[" + replaceChars + "]", "");
			}

			double input = Double.valueOf(dest2 + source2);
			if (isInRange(min, max, input))
				return null;
		} catch (NumberFormatException nfe) {
		}
		return "";
	}

	private boolean isInRange(double a, double b, double c) {
		return b > a ? c >= a && c <= b : c >= b && c <= a;
	}
}
package com.jordansilva.android.prettyui;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.AutoCompleteTextView;

public class ExtendedAutoCompleteTextView extends AutoCompleteTextView {

	private static Map<String, Typeface> mTypefaces;
	private boolean mAllCaps;

	public ExtendedAutoCompleteTextView(final Context context) {
		super(context);
	}

	public ExtendedAutoCompleteTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ExtedendView);
		loadFontType(context, array);
		loadAllCaps(context, array);
	}

	public ExtendedAutoCompleteTextView(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);

		final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ExtedendView);

		loadFontType(context, array);
		loadAllCaps(context, array);
	}

	@SuppressLint("NewApi")
	private void loadAllCaps(final Context context, final TypedArray typedArray) {
		try {

			mAllCaps = typedArray.getBoolean(R.styleable.ExtedendView_allCaps, false);
			if (mAllCaps) {
				if (android.os.Build.VERSION.SDK_INT >= 14)
					setAllCaps(mAllCaps);
				else {
					if (getText() != null && getText().length() > 0) {
						String text = getText().toString().toUpperCase(Locale.getDefault());
						setText(text);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void loadFontType(final Context context, final TypedArray typedArray) {
		try {
			if (mTypefaces == null) {
				mTypefaces = new HashMap<String, Typeface>();
			}

			if (typedArray != null) {
				final String typefaceAssetPath = typedArray.getString(R.styleable.ExtedendView_android_fontFamily);
				final int textStyle = typedArray.getInt(R.styleable.ExtedendView_android_textStyle, Typeface.NORMAL);

				if (typefaceAssetPath != null) {
					Typeface typeface = null;

					if (mTypefaces.containsKey(typefaceAssetPath + String.valueOf(textStyle))) {
						typeface = mTypefaces.get(typefaceAssetPath);
					} else {
						AssetManager assets = context.getAssets();

						typeface = Typeface.createFromAsset(assets,
								"fonts/" + FontHelper.getFontName(typefaceAssetPath, Typeface.defaultFromStyle(textStyle)));
						mTypefaces.put(typefaceAssetPath + String.valueOf(textStyle), typeface);
					}

					setTypeface(typeface);
				}
				typedArray.recycle();
			}
		} catch (Exception e) {
			Log.e("ExtendedButton", "Could not get typeface");
		}
	}

	@Override
	public void setText(CharSequence text, BufferType type) {

		if (mAllCaps && text != null && text.length() > 0)
			text = text.toString().toUpperCase(Locale.getDefault());

		super.setText(text, type);
	}

	public void setTextHtml(Spanned text) {
		super.setText(text);
	}

	@SuppressLint("NewApi")
	public void setAllCaps(boolean allCaps) {

		if (android.os.Build.VERSION.SDK_INT >= 14)
			super.setAllCaps(allCaps);
		else
			mAllCaps = allCaps;

		invalidate();
	}

}

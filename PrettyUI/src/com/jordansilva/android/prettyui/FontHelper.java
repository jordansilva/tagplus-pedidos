package com.jordansilva.android.prettyui;

import android.graphics.Typeface;

public class FontHelper {

	public class ROBOTO {
		public static final String CONDENSED_LIGHT = "Roboto Condensed 300.ttf";
		public static final String CONDENSED_LIGHT_ITALIC = "Roboto Condensed 300italic.ttf";
		public static final String CONDENSED_BOLD = "Roboto Condensed 700.ttf";
		public static final String CONDENSED_BOLD_ITALIC = "Roboto Condensed 700italic.ttf";
		public static final String CONDENSED_ITALIC = "Roboto Condensed italic.ttf";
		public static final String CONDENSED_REGULAR = "Roboto Condensed regular.ttf";

		public static final String THIN = "Roboto 100.ttf";
		public static final String THIN_ITALIC = "Roboto 100italic.ttf";
		public static final String LIGHT = "Roboto 300.ttf";
		public static final String LIGHT_ITALIC = "Roboto 300italic.ttf";
		public static final String REGULAR = "Roboto 500.ttf";
		public static final String REGULAR_ITALIC = "Roboto 500italic.ttf";
		public static final String BOLD = "Roboto 700.ttf";
		public static final String BOLD_ITALIC = "Roboto 700italic.ttf";
		public static final String BLACK = "Roboto 900.ttf";
		public static final String BLACK_ITALIC = "Roboto 900italic.ttf";
	}

	public static String getFontName(String name, Typeface style) {
		// Font Roboto Tipo Estilo
		String font = "";
		String tipo = "";
		String estilo = "";

		boolean isRoboto = name.contains("sans-serif");
		boolean isCondensed = name.contains("condensed");
		boolean isThin = name.contains("thin");
		boolean isLight = name.contains("light");
		boolean isBlack = name.contains("black");
		boolean isBold = style.isBold();
		boolean isItalic = style.isItalic();

		// Fonte
		if (isRoboto)
			font = "Roboto";
		if (isCondensed)
			font += " Condensed";

		if (isThin)
			tipo = "100";
		else if (isLight)
			tipo = "300";
		else if (isBold)
			tipo = "700";
		else if (isBlack)
			tipo = "900";
		else if (!isCondensed)
			tipo = "500";
		else if (isCondensed)
			tipo = "regular";

		if (isItalic)
			estilo = "italic";

		if (isRoboto)
			return String.format("%s %s%s.ttf", font, tipo, estilo);
		else
			return String.format("%s.ttf", name);
	}

}

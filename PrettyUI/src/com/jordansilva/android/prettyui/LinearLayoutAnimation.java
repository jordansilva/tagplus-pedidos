package com.jordansilva.android.prettyui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class LinearLayoutAnimation extends LinearLayout {

	public CustomAnimationListener mListener;

	public void setAnimationListener(CustomAnimationListener listener) {
		mListener = listener;
	}

	public LinearLayoutAnimation(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LinearLayoutAnimation(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onAnimationEnd() {
		super.onAnimationEnd();
		if (mListener != null)
			mListener.onAnimationEnd();
	}

	public interface CustomAnimationListener {
		void onAnimationEnd();
	}

}

package com.jordansilva.android.prettyui.empty;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.jordansilva.android.prettyui.ExtendedButton;
import com.jordansilva.android.prettyui.ExtendedTextView;
import com.jordansilva.android.prettyui.R;

public class EmptyView extends RelativeLayout {

	private View mView;
	private ExtendedTextView mTextView;
	private ExtendedButton mButton;
	private int mLayout = R.layout.empty_layout;

	public EmptyView(Context context) {
		this(context, null);
	}

	public EmptyView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
		mView = LayoutInflater.from(context).inflate(mLayout, this);
		mTextView = (ExtendedTextView) mView.findViewById(R.id.emptyview_no_items);
		mButton = (ExtendedButton) mView.findViewById(R.id.emptyview_add_new_item);

		final CharSequence message;
		final CharSequence buttonText;
		final int visibility;

		if (attrs == null) {
			message = getResources().getString(R.string.emptyview_no_items);
			buttonText = getResources().getString(R.string.emptyview_add_new_item);
			visibility = View.VISIBLE;
		} else {
			final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EmptyView, defStyleAttr, 0);

			message = a.getText(R.styleable.EmptyView_message);
			buttonText = a.getText(R.styleable.EmptyView_buttonText);
			visibility = a.getInt(R.styleable.EmptyView_buttonVisible, View.VISIBLE);
			a.recycle();
		}

		if (message != null && message.length() > 0)
			mTextView.setText(message);

		if (buttonText != null && buttonText.length() > 0)
			mButton.setText(buttonText);

		mButton.setVisibility(visibility);
	}

	public void setCustomView(int layout) {
		mLayout = layout;

		removeAllViews();
		init(getContext(), null, 0);
		invalidate();
		requestLayout();
	}

	public void setMessageText(CharSequence text) {
		mTextView.setText(text);
	}

	public void setMessageText(int resid) {
		mTextView.setText(resid);
	}

	public void setButtonText(CharSequence text) {
		mButton.setText(text);
	}

	public void setButtonText(int resid) {
		mButton.setText(resid);
	}

	public void setButtonVisible(int visibility) {
		mButton.setVisibility(visibility);
	}

	public void setListener(OnClickListener listener) {
		mButton.setOnClickListener(listener);
	}

}

package br.com.tagplus.pedidos.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

public class FileHelper {

	public static String saveImage(Context context, Bitmap bitmap, String imageName) {
		if (bitmap != null) {
			try {

				String filename = imageName;

				if (StringHelper.isNullOrEmpty(imageName))
					filename = String.valueOf(System.currentTimeMillis()) + ".jpg";

				OutputStream fOut = context.openFileOutput(filename, Context.MODE_PRIVATE);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
				bitmap.recycle();
				fOut.flush();
				fOut.close();
				fOut = null;

				return filename;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public final static String bitmapToString(Bitmap in) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		in.compress(Bitmap.CompressFormat.JPEG, 65, bytes);
		return Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
	}

	public final static Bitmap stringToBitmap(String in) {
		byte[] bytes = Base64.decode(in, Base64.DEFAULT);
		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	}

	public static Bitmap getBitmapFromUri(Context context, Uri contentUri) {
		String path = null;
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
		if (cursor.moveToFirst()) {
			int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			path = cursor.getString(columnIndex);
		}
		cursor.close();
		File file = new File(path);
		if (file.exists())
			return decodeBitmap(file);

		return null;
	}

	public final static Bitmap decodeBitmap(File f) {
		try {

			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;

			FileInputStream stream = new FileInputStream(f);
			BitmapFactory.decodeStream(stream, null, o);
			stream.close();

			// The new size we want to scale to
			final int REQUIRED_SIZE = 640;

			// Orientation
			ExifInterface exif = new ExifInterface(f.getPath());
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

			int angle = 0;

			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
				angle = 90;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
				angle = 180;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
				angle = 270;
			}

			Matrix mat = new Matrix();
			mat.postRotate(angle);

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;

			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bmp = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();

			Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
			bmp = null;
			System.gc();

			return correctBmp;

		} catch (Exception e) {
		}
		return null;
	}

}

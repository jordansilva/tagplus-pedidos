package br.com.tagplus.pedidos;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import br.com.tagplus.pedidos.MenuFragment.MenuListener;
import br.com.tagplus.pedidos.business.BusinessListener;
import br.com.tagplus.pedidos.business.RequestResult;
import br.com.tagplus.pedidos.business.SincronizarService;
import br.com.tagplus.pedidos.business.domain.Perfil;
import br.com.tagplus.pedidos.ui.ProgressDialogFragment;

@EActivity(R.layout.activity_drawer)
public class DrawerActivity extends BaseActivity implements MenuListener {

	@ViewById(R.id.drawer_layout)
	DrawerLayout mDrawerLayout;

	@ViewById(R.id.drawer)
	FrameLayout mDrawerFrame;

	@ViewById(R.id.container)
	FrameLayout mContentFrame;

	@FragmentById(R.id.navigation_drawer)
	MenuFragment_ menuFragment;

	@Bean
	SincronizarService service;

	private Fragment currentFragment;
	private ActionBarDrawerToggle mDrawerToggle;
	private boolean mIsDrawerLocked;
	private String mTitle = "";
	private ProgressDialogFragment progressDialogFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		progressDialogFragment = ProgressDialogFragment.newInstance();
		progressDialogFragment.setRetainInstance(true);
	}

	@Override
	protected void load() {
		super.load();
		carregarActionBar();

		Fragment fragment = getFragmentManager().findFragmentById(R.id.navigation_drawer);
		if (fragment != null) {
			menuFragment = (MenuFragment_) fragment;
			if (currentFragment == null)
				menuFragment.init();
		}
	}

	private void carregarActionBar() {

		if (!mIsDrawerLocked) {
			// Getting reference to the ActionBarDrawerToggle
			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer,
					R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

				/** Called when drawer is closed */
				public void onDrawerClosed(View view) {
					setTitle(mTitle);
					invalidateOptionsMenu();
				}

				/** Called when a drawer is opened */
				public void onDrawerOpened(View drawerView) {
					setTitle(R.string.app_name);
					invalidateOptionsMenu();
				}
			};

			// Setting DrawerToggle on DrawerLayout
			mDrawerLayout.setDrawerListener(mDrawerToggle);
			mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mDrawerToggle != null)
			mDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMenuSelected(int codigoMenu, String nomeMenu) {

		Fragment fragment = null;

		switch (codigoMenu) {
		case -1:
			deslogar();
			return;
		case 1:
			fragment = ListaPedidoFragment_.builder().build();
			break;
		case 2:
			fragment = ListaProdutoGradeFragment_.builder().build();
			break;
		case 3:
			fragment = ListaClientesFragment_.builder().build();
			break;
		case 4:
			fragment = ListaFornecedoresFragment_.builder().build();
			break;
		case 6:
			if (getUsuarioLogado().isVinculado())
				sincronizarDados();
			else {
				fragment = SincronizarFragment_.builder().build();
				// Intent intent = new Intent(this,
				// ContaTagPlusActivity_.class);
				// startActivityForResult(intent, 100);
			}

			break;
		case 7:
			fragment = ConfiguracaoFragment_.builder().build();
		default:
			break;
		}

		if (fragment != null) {
			mTitle = nomeMenu;
			setTitle(nomeMenu);
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
			currentFragment = fragment;
		}

		// Closing the drawer
		if (!mIsDrawerLocked)
			mDrawerLayout.closeDrawer(mDrawerFrame);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 999 && resultCode == RESULT_OK) {
			reloadMenu();
		}

		super.onActivityResult(requestCode, resultCode, data);

	}

	@Background
	protected void sincronizarDados() {

		runOnUiThread(new Runnable() {
			public void run() {
				// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				progressDialogFragment.setCancelable(false);
				progressDialogFragment.show(getFragmentManager(), "Loading");
			}
		});

		service.sincronizarDados(getUsuarioLogado(), new BusinessListener() {

			@Override
			public void onPreInit() {
			}

			@Override
			public void onPostExecute(final RequestResult result) {
				runOnUiThread(new Runnable() {
					public void run() {

						// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
						if (progressDialogFragment != null && progressDialogFragment.isAdded())
							progressDialogFragment.dismissAllowingStateLoss();
						if (result.isError()) {
							String message = result.getMessage();
							if (result.getCode() == 401 || result.getCode() == 404) {
								message = getString(R.string.token_invalido);
								Perfil p = getUsuarioLogado();
								p.setKey("");
								helper.addUsuario(p);
							}
							Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

						} else {
							Toast.makeText(getApplicationContext(), R.string.sincronizacao_concluida, Toast.LENGTH_LONG).show();
							helper.addUsuario((Perfil) result.getData());
						}

						Fragment f = getFragmentManager().findFragmentById(R.id.container);
						if (f != null) {
							if (f instanceof SincronizarFragment)
								moverParaTelaPedidos();
							else if (f instanceof BaseFragment)
								((BaseFragment) f).load();
						}
					}
				});
			}
		});
	}

	public void moverParaTelaPedidos() {
		menuFragment.selectItem(0);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	public void reloadMenu() {
		menuFragment.load();
		sincronizarDados();
	}

}
